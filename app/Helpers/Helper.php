<?php
namespace App\Helpers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserDetail, Videolist, Category, Subcategory, VideoSubcategory, Genre, VideoGenre, Contest, ContestData, ContestResult, UserContest, ContestWinner, LeaderBoard, UserContestWinloss, Notification, Wallet, UserWallet, UserGame};
use Carbon\Carbon;
use Redirect;
use DB;
use Session;
use Auth;
class Helper {

    //method for return Game Winning Number
    public function getWinningNumber($game_id=NULL){
        $query = ContestResult::where('contest_id',$game_id)->whereDate('contest_date', '=', date(DB_DATE_FORMAT,strtotime(now())))->first();
        $winning_number = $query ? $query->result : 'comming soon';
        return $winning_number;
    }

    function formatGameData($jsonString) {
        // Decode the JSON data
        $data = json_decode($jsonString, true);
        // dd($data);

        // Initialize variables
        $formattedData = [];
        $totalAmount = 0;

        // Format each game entry and calculate total amount
        foreach ($data as $item) {
            dd($data);

            $gameNumber = $item['game_number'];
            $gameNumberAmount = $item['game_number_amount'];
            // Format the entry and add to the result array
            $formattedEntry = "$gameNumber($gameNumberAmount)";
            $formattedData[] = $formattedEntry;
            // Update total amount
            $totalAmount += $gameNumberAmount;
        }
        // Format the result with commas and spaces
        $resultString = implode(', ', $formattedData);
        // Add total amount in a new line
        $resultString .= "<br><b>Total amount</b>:  ₹ <b>$totalAmount</b>";
        return html_entity_decode($resultString);
    }


}

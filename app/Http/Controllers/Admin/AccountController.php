<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\AnalystAccount;
use DB;
use Auth;
use Exception;
use Response;
use Illuminate\Support\Str;
use Carbon\Carbon;


class AccountController extends Controller
{
    public function index(Request $request){
        
        $list = AnalystAccount::select('analyst_accounts.*', 'analyst_accounts.created_at as call_date', 'users.name', 'ud.*', 'u.id as uid')
                ->leftjoin('users', 'users.id', '=', 'analyst_accounts.analyst_id')
                ->leftjoin('users as u', 'u.id', '=', 'analyst_accounts.user_id')
                ->leftjoin('user_details as ud', 'ud.user_id', '=', 'analyst_accounts.analyst_id')
                ->orderBy('analyst_accounts.id', 'DESC')
                ->get();
        //echo"<pre>";print_r($list);die;
        return view('admin.account.analyst_account', compact('list'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\{User, UserDetail, Videolist, Category, Subcategory, VideoSubcategory, Genre, VideoGenre, Contest, ContestData, ContestResult, UserContest, ContestWinner, LeaderBoard, UserContestWinloss, Notification, Wallet, UserWallet, UserGame};
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class AgentController extends Controller
{
    private $today;
    public function __construct()
    {
        $this->today = Carbon::today();
    }
    /**
     * Display Agent Dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $user = Auth::user();
        $game = Contest::get();
        // dd($game);
        $details = UserGame::get();
        // dd($details);


        // $users = User::where('created_by',$user->id)->orderBy('id','desc')->get();
        return view('admin.agent-dashboard.dashboard',compact('details','game'));
    }

    /**
     * Display the file of Create User Create Form View.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserCreateView(Request $request){

        return view('admin.agent-dashboard.user_create');
    }

     /**
     * Method for store user data
     *
     * @return \Illuminate\Http\Response
     */
    public function UserCreateStore(Request $request){
        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required',
            //'email' => 'required|email|unique:users,email',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
        ],[
            //'email.unique' => 'This Email Already Exists !',
            'mobile.unique' => 'This Mobile Already Exists !',
        ]);
        //dd($request->all());
        DB::beginTransaction();
        try {
            $input = $request->all();
            $input['password'] = Hash::make('123456789');
            $input['created_by'] = $user->id;
            $user_createtd = User::create($input);
            $user_createtd->assignRole('User');
            //insert data in userdetail table
            UserDetail::create([
                'user_id' => $user_createtd->id
            ]);
            DB::commit();
            return redirect()->route('agent.UserHistory')->with('success','User created successfully');
        } catch (\Throwable $th) {
             DB::rollback();
            //echo $th ;die;
            return back()->with('danger','Something went wrong');
        }
    }

     /**
     * Display the file of Edit User Form View.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserEditView(Request $request,$user_id=NULL){
        $user = User::find($user_id);
        if (!$user) {
           return redirect()->route('agent.dashboard');
        }
        return view('admin.agent-dashboard.user_edit',compact('user'));
    }

     /**
     * Method for Edit user data
     *
     * @return \Illuminate\Http\Response
     */
    public function UserEditStore(Request $request, $user_id = NULL)
    {
        $user = Auth::user();
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required',
            //'email' => 'required|email|unique:users,email,' . $user_id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,' . $user_id,
        ]);

        try {
            DB::beginTransaction();
            // Find the user by ID
            $user = User::find($user_id);
            // Check if the user exists
            if (!$user) {
                return back()->with('danger', 'User not found');
            }
            // Update user data
            $user->update($input);
            // Insert data in userdetail table (assuming you have a UserDetail model and relationship)
            // $user->userDetail()->update([...]);
            DB::commit();
            return redirect()->route('agent.UserHistory')->with('success', 'User updated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            // Log the error for debugging purposes
            // logger()->error($e);
            return back()->with('danger', 'Something went wrong');
        }
    }

    /**
     * Method for Delete Users.
     */
    public function UserDestroy(Request $request, $user_id=NULL){
        try {
            // Find the user by ID
            $user = User::find($user_id);
            // Check if the user exists
            if (!$user) {
                return back()->with('danger', 'User not found');
            }
            User::find($user_id)->delete();
            return back()->with('danger','User deleted successfully !');
        } catch (\Throwable $th) {
            //return $th;
            return back()->with('danger', 'Something went wrong !');
        }

    }

     /**
     * Display the file of User history.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserHistory(Request $request){
        $user = Auth::user();
        $user_history = User::where('created_by',$user->id)->orderBy('id','desc')->get();
        return view('admin.agent-dashboard.user_history',compact('user_history'));
    }


}

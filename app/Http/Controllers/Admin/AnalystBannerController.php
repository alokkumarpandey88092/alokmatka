<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\QuestionAnswer;
use App\Models\UserAnswer;
use App\Models\banner;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AnalystBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $data = banner::orderBy('id','desc')->get();
        return view('admin.analyst.banners.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.analyst.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $input = $request->all();
        $data = [
            'status' => $input['status'],
        ];
        if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $image = uniqid() . $file->getClientOriginalName();
            $path = 'banners_image';
            $file->move($path, $image);
            $data['banner'] = $image;
        }
        try {
            $query = banner::create($data);
            return back()->with('success','Banner created successfully');
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        //return view('admin.analyst.banners.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){ 
        $banners = banner::find($id);
        return view('admin.analyst.banners.edit',compact('banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
            
            $input = $request->all();
            $data = [
                'status' => $input['status'],
            ];
            if ($request->hasFile('banner_image')) {
                $file = $request->file('banner_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = 'banners_image';
                $file->move($path, $image);
                $data['banner'] = $image;
                $exist_img = banner::select('banner')->where('id', $id)->first();
                File::delete(public_path('banners_image/' . $exist_img->banner));
            }
            try {
                $query = banner::where('id',$id)->update($data);
                return redirect()->route('banners.index')
                            ->with('success','Banner updated successfully');
            } catch (\Throwable $th) {
                return redirect()->route('banners.index')
                ->with('danger','Something went wrong');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $query = banner::find($id)->delete();
        if($query){
            return back()->with('danger','Banner deleted successfully !');
        }
    }
}

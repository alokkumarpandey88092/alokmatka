<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AnalystController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = User::role('Analyst')->with('userdetails')->orderBy('users.id','DESC')->get();
        return view('admin.analyst.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::select('name')->get();
        return view('admin.analyst.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|unique:users',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        $channal_name = (string) Str::uuid();
            UserDetail::create([
                'user_id' => $user->id,
                'channal_name' => $channal_name,
            ]);
        $user->assignRole($request->input('roles'));
    
        return back()->with('success','Analyst created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        //return view('admin.analyst.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$user = User::find($id);
        $user = User::with('userdetails')->find($id);
        $roles = Role::select('name')->get();
        $userRole = $user->roles->pluck('name','name')->all();
        $languages = DB::table('languages')->select('id','name','code')->get();
        return view('admin.analyst.edit',compact('user','roles','userRole','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|unique:users,mobile,'.$id,
            //'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
    
        $user = User::find($id);
        $user->update($input);
        $user_details = [
            'experience'                => $input['experience'] ? $input['experience'] : NULL,
            'consultant'                => $input['consultant'] ? $input['consultant'] : NULL,
            'language'                  => isset($input['language']) ? implode(',',$input['language']) : NULL,
            'location'                  => $input['location'] ? $input['location'] : NULL,
            'sebi'                      => $input['sebi'] ? $input['sebi'] : NULL,
            'bio'                       => $input['bio'] ? $input['bio'] : NULL,
            'description'               => $input['description'] ? $input['description'] : NULL,
            'voice_call_charges'        => $input['voice_call_charges'] ? $input['voice_call_charges'] : NULL,
            'video_call_charges'        => $input['video_call_charges'] ? $input['video_call_charges'] : NULL,
            'chat_call_charges'         => $input['chat_call_charges'] ? $input['chat_call_charges'] : NULL,
            'analyst_charges'           => $input['analyst_charges'] ? $input['analyst_charges'] : NULL,
            'live_stream_charges'           => $input['live_charges'] ? $input['live_charges'] : 0,
            'voice_call_enable'         => isset($input['voice_call_enable']) ? 1 : 0,
            'video_call_enable'         => isset($input['video_call_enable']) ? 1 : 0,
            'chat_call_enable'          => isset($input['chat_call_enable'])? 1 : 0,
            'updated_at'                => now(),
        ];
        //dd($user_details);
        if ($request->hasFile('profile')) {
            $file = $request->file('profile');
            $image = uniqid() . $file->getClientOriginalName();
            $path = 'members_image';
            $file->move($path, $image);
            $user_details['profile'] = $image;
            $exist_img = UserDetail::select('profile')->where('user_id', $id)->first();
            File::delete(public_path('members_image/' . $exist_img->profile));
        }
        UserDetail::where('user_id',$id)->update($user_details);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
    
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('analysts.index')
                        ->with('success','Analyst updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $query = User::find($id)->delete();
        if($query){
            return back()->with('danger','Analyst deleted successfully !');
        }
    }

    /**
     *Method for Approve Analyst.
     *
     * @return \Illuminate\Http\Response
     */   
    public function approveAnalyst(Request $request,$id){
        $user = User::with('userdetails')->find($id);
            if($user){
                $approved_status = ($user->userdetails['approved'] == 1) ? 0 : 1; 
                $approved_msg = ($user->userdetails['approved'] == 1) ? 'Pending' : 'Approved'; 
                UserDetail::where('user_id',$id)->update(['approved'=>$approved_status]);  
            }
        return back()->with('success',$approved_msg.' updated successfully');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AnalystProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $user = User::with('userdetails')->where('id',Auth::user()->id)->first();
        $roles = Role::select('name')->get();
        $userRole = $user->roles->pluck('name','name')->all();
        $languages = DB::table('languages')->select('id','name','code')->get();
        return view('admin.analyst.profile.index',compact('user','roles','userRole','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
         //dd($request->all());
         $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|unique:users,mobile,'.$id,
        ]);
    
        $input = $request->all();
    
        $user = User::find($id);
        $user->update($input);
        $user_details = [
            'experience'                => $input['experience'] ? $input['experience'] : NULL,
            'consultant'                => $input['consultant'] ? $input['consultant'] : NULL,
            'language'                  => isset($input['language']) ? implode(',',$input['language']) : NULL,
            'location'                  => $input['location'] ? $input['location'] : NULL,
            'sebi'                      => $input['sebi'] ? $input['sebi'] : NULL,
            'bio'                       => $input['bio'] ? $input['bio'] : NULL,
            'description'               => $input['description'] ? $input['description'] : NULL,
            'voice_call_charges'        => $input['voice_call_charges'] ? $input['voice_call_charges'] : NULL,
            'video_call_charges'        => $input['video_call_charges'] ? $input['video_call_charges'] : NULL,
            'chat_call_charges'         => $input['chat_call_charges'] ? $input['chat_call_charges'] : NULL,
            'analyst_charges'           => $input['analyst_charges'] ? $input['analyst_charges'] : NULL,
            'voice_call_enable'         => isset($input['voice_call_enable']) ? 1 : 0,
            'video_call_enable'         => isset($input['video_call_enable']) ? 1 : 0,
            'chat_call_enable'          => isset($input['chat_call_enable'])? 1 : 0,
            'updated_at'                => now(),
        ];
        //dd($user_details);
        if ($request->hasFile('profile')) {
            $file = $request->file('profile');
            $image = uniqid() . $file->getClientOriginalName();
            $path = 'members_image';
            $file->move($path, $image);
            $user_details['profile'] = $image;
            $exist_img = UserDetail::select('profile')->where('user_id', $id)->first();
            File::delete(public_path('members_image/' . $exist_img->profile));
        }
        UserDetail::where('user_id',$id)->update($user_details);
    
        return back()->with('success','Profile updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

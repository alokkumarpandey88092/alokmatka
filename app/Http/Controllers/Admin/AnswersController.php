<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\QuestionAnswer;
use App\Models\UserAnswer;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $data = UserAnswer::select('name','answer','is_skip')
                    ->leftJoin('users','users.id','=','user_answers.user_id')
                    ->where('user_answers.analyst_id',Auth::user()->id)
                    ->where('user_answers.answer','!=',NULL)
                    ->orderBy('user_answers.id','desc')->get();
        return view('admin.analyst.answers.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.analyst.questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'question' => 'required',
        ]);
        $input = $request->all();
        $data = [
            'question' => $input['question'] ? $input['question'] : NULL,
        ];
        $query = QuestionAnswer::create($data);
    
        return back()->with('success','Question created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        //return view('admin.analyst.questions.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $questions = QuestionAnswer::find($id);
        return view('admin.analyst.questions.edit',compact('questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
            $this->validate($request, [
                'question' => 'required|unique:question_answers,question,'.$id,
            ]);
            $input = $request->all();
            $data = [
                'question' => $input['question'] ? $input['question'] : NULL,
            ];
            $query = QuestionAnswer::where('id',$id)->update($data);
    
        return redirect()->route('questions.index')
                        ->with('success','Question updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $query = QuestionAnswer::find($id)->delete();
        if($query){
            return back()->with('danger','Questions deleted successfully !');
        }
    }
}

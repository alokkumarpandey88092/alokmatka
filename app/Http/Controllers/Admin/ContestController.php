<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Videolist;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\VideoSubcategory;
use App\Models\Genre;
use App\Models\VideoGenre;
use App\Models\Contest;
use App\Models\ContestData;
use App\Models\ContestResult;
use App\Models\UserContest;
use App\Models\ContestWinner;
use App\Models\LeaderBoard;
use App\Models\UserContestWinloss;
use App\Models\Notification;
use App\Models\Wallet;
use App\Models\UserWallet;
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();

        $data = Contest::select('contests.*','u.name as creater_name', 'u.email as creater_email', 'u.mobile as creater_mobile', 'u.id as creater_id')
        ->leftJoin('users as u','u.id','=','contests.created_by')
        ->orderBy('id','DESC')->get();
        return view('admin.contest.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $videos_category = Category::orderBy('id','desc')->get();
        $videos_subcategory = Subcategory::orderBy('id','desc')->get();
        $videos_genre = Genre::orderBy('id','desc')->get();
        $roles = Role::select('name')->get();
        return view('admin.contest.create',compact('roles','videos_category','videos_subcategory','videos_genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $user = Auth::user();
            $this->validate($request, [
                'contest_name' => 'required|unique:contests,contest_name'
            ], [
                'contest_name.unique' => 'This Game Already Exists !',
            ]);
        //dd($request->all());
        DB::beginTransaction();
        try {
            $contest = [
                'contest_name' => $request->contest_name ? $request->contest_name : NULL,
                // 'start_date' => $request->start_date ? date(DB_DATE_FORMAT,strtotime($request->start_date)) : NULL,
                // 'end_date' => $request->end_date ? date(DB_DATE_FORMAT,strtotime($request->end_date)): NULL,
                'start_end_time' => $request->start_time ? $request->start_time : NULL,
                'end_time' => $request->end_time ? $request->end_time : NULL,
                'description' => $request->description ? $request->description : NULL,
                'created_by' => $user->id,
            ];
            //dd($contest);
            if ($request->hasFile('member_logo')) {
                $file = $request->file('member_logo');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/member_logo');
                $file->move($path, $image);
                $contest['member_logo'] = $image;
            }

            if ($request->hasFile('contest_image')) {
                $file = $request->file('contest_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/contest_image');
                $file->move($path, $image);
                $contest['contest_image'] = $image;
            }

            $query = Contest::create($contest);
        //     $contest_data = [
        //         ['contest_id' => $query->id, 'share_name' =>  $request->option_a, 'updated_at' => now(),
        //             'created_at' => now() ],
        //         ['contest_id' => $query->id, 'share_name' =>  $request->option_b , 'updated_at' => now(),
        //             'created_at' => now()],
        //         ['contest_id' => $query->id, 'share_name' =>  $request->option_c , 'updated_at' => now(),
        //             'created_at' => now()],
        //         ['contest_id' => $query->id, 'share_name' =>  $request->option_d , 'updated_at' => now(),
        //             'created_at' => now()],
        //     ];
        //    // print_r($contest_data);die;
        //     ContestData::insert($contest_data);
        //     ContestResult::create(['contest_id' => $query->id, 'contest_date'=> now()->toDateString(), 'result'=>null]);
             DB::commit();
            return redirect()->route('contest.index')->with('success','Game created successfully');
        } catch (\Throwable $th) {
             DB::rollback();
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $contest = Contest::with('getContestData')->find($id);
        return view('admin.contest.edit',compact('contest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'contest_name' => 'required'
        ]);
        DB::beginTransaction();
        try {
            $contest = [
                'contest_name' => $request->contest_name ? $request->contest_name : NULL,
                'start_date' => $request->start_date ? date(DB_DATE_FORMAT,strtotime($request->start_date)) : NULL,
                'end_date' => $request->end_date ? date(DB_DATE_FORMAT,strtotime($request->end_date)): NULL,
                'start_end_time' => $request->start_time ? $request->start_time : NULL,
                'end_time' => $request->end_time ? $request->end_time : NULL,
                'description' => $request->description ? $request->description : NULL,
            ];
            //dd($contest);
            if ($request->hasFile('member_logo')) {
                $file = $request->file('member_logo');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/member_logo');
                $file->move($path, $image);
                $exist_img = Contest::select('member_logo')->where('id', $id)->first();
                File::delete(public_path('assets/member_logo/' . $exist_img->member_logo));
                $contest['member_logo'] = $image;
            }

            if ($request->hasFile('contest_image')) {
                $file = $request->file('contest_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/contest_image');
                $file->move($path, $image);
                $exist_img = Contest::select('contest_image')->where('id', $id)->first();
                File::delete(public_path('assets/contest_image/' . $exist_img->contest_image));
                $contest['contest_image'] = $image;
            }
            
            $query = Contest::where('id',$id)->update($contest);
            
            // ContestData::upsert(
            //     // Condition based on email
            //    [ ['id' => $request->id_option_a, 'contest_id'=> $id, 'share_name' =>  $request->option_a, 'updated_at' => now(),
            //         'created_at' => now() ],
            //     ['id' => $request->id_option_b, 'contest_id'=> $id, 'share_name' =>  $request->option_b , 'updated_at' => now(),
            //         'created_at' => now()],
            //     ['id' => $request->id_option_c, 'contest_id'=> $id, 'share_name' =>  $request->option_c , 'updated_at' => now(),
            //         'created_at' => now()],
            //     ['id' => $request->id_option_d, 'contest_id'=> $id, 'share_name' =>  $request->option_d , 'updated_at' => now(),
            //         'created_at' => now()]],['id'],['share_name']
            // );
            DB::commit();
            return redirect()->route('contest.index')->with('success','Game updated successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try {
            $query = Videolist::find($id)->delete();
            $query2 = VideoSubcategory::where('video_id', $id)->delete();
            $query3 = VideoGenre::where('video_id', $id)->delete();
            if ($query) {
            return back()->with('danger','Videos deleted successfully !');
            }
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }

     /**
     *Method for Approve Videos.
     *
     * @return \Illuminate\Http\Response
     */   
    public function approveContest(Request $request,$id){
        $contest_list = Contest::find($id);
        try {
            if($contest_list){
                $approved_status = ($contest_list->status == 1) ? 0 : 1; 
                //dd($approved_status);
                $approved_msg = ($contest_list->status == 1) ? 'Pending' : 'Approved'; 
                Contest::where('id',$id)->update(['status'=>$approved_status]);  
            }
        return back()->with('success','Contest '.$approved_msg.' updated successfully');
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }

  
    public function contestAnswer(Request $request){
        
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();
            $data = ContestResult::select('contest_results.id as caid','contest_results.contest_id','contest_results.contest_date', 'contest_results.result', 'contests.*')->leftJoin('contests', 'contests.id', '=', 'contest_results.contest_id')
            ->whereBetween('contest_results.created_at', [$from, $to])
            ->orderBy('contests.id', 'DESC')->get();
        return view('admin.contest.contest_answer', compact('data'));
    }
    
    public function editAnswer($id){
        
        $contest = ContestResult::select('contest_results.id as crid','contest_results.contest_id','contest_results.contest_date', 'contest_results.result', 'contests.*')->leftJoin('contests', 'contests.id', '=', 'contest_results.contest_id')->where('contest_results.id', $id)->first();
        
        $contestData = ContestData::where('contest_id', $contest->contest_id)->get();
        $reservedSeats = UserContest::where('contest_id', $contest->contest_id)->count();
        $totalCollection = ((int)$reservedSeats * (int)$contest->joining_fee);
       // dd($contest);
        
        return view('admin.contest.contest_result_edit',compact('contest', 'contestData', 'reservedSeats', 'totalCollection'));
    }
    
    public function saveContestAns(Request $request, $id){
         
        if($request->contest_type == 1){
            $result = [(int)$request->single_ans];
            $result_data = $request->single_ans;
        }elseif($request->contest_type == 2){
            $result = [(int)$request->arr_ans_a,(int)$request->arr_ans_b,(int)$request->arr_ans_c,(int)$request->arr_ans_d];    
            $result_data = "$request->arr_ans_a,$request->arr_ans_b,$request->arr_ans_c,$request->arr_ans_d";    
        }   
             try {
                $response = ContestResult::where('id', $id)->update(['result' => $result_data]);
                $join_user_data = UserContest::where('contest_id', $request->contest_id)->get();
                $contestWinner =[];
                $data =[];
                foreach($join_user_data as $value){
                    $array1     = explode(',', $value->answer);
                    $array1     = array_map('intval', $array1);
                    
                    if ($array1 === $result) {
                        // $leader = LeaderBoard::find($value->contest_id);
                        
                        // if($leader){
                        //     LeaderBoard::where('id', $value->contest_id)->delete();
                        // }
                        $data = ['user_id' => $value->user_id, 'contest_id' => $request->contest_id,  'result'=>1];
                        
                        $contestWinner = ['user_id' => $value->user_id, 'contest_id' => $request->contest_id];
                         ContestWinner::updateOrCreate(['user_id' => $value->user_id, 'contest_id' => $value->contest_id],$contestWinner);
                    } else{
                    
                        $data = ['user_id' => $value->user_id, 'contest_id' => $request->contest_id,  'result'=>0];
                        $check = ContestWinner::where(['user_id' => $value->user_id, 'contest_id' => $value->contest_id])->count();
                        if($check > 0){
                            ContestWinner::where(['user_id' => $value->user_id, 'contest_id' => $value->contest_id])->delete();
                        }
                    }
                }

                UserContestWinloss::updateOrCreate(['user_id' => $value->user_id,'contest_id' => $value->contest_id], $data);
                $users = ContestWinner::where(['contest_id' => $value->contest_id])->get();

                // Randomly shuffle the users and assign a random order
                $shuffledUsers = $users->shuffle();
                $randomOrder = 1;

                foreach ($shuffledUsers as $user) {
                    $user->update(['random_order' => $randomOrder]);
                    $randomOrder++;
                }
                
                return back()->with('success','Answer Set Successfully !');
            } catch (\Throwable $th) {
                throw $th;
                return back()->with('danger','Something went wrong');
            }

          
    }

    public function prizeDistributionView($id){
            $contest = ContestResult::select('contest_results.id as crid','contest_results.contest_id','contest_results.contest_date', 'contest_results.result', 'contests.*')->leftJoin('contests', 'contests.id', '=', 'contest_results.contest_id')->where('contest_results.contest_id', $id)->first();
            
            $contestData = ContestData::where('contest_id', $contest->contest_id)->get();
            $reservedSeats = UserContest::where('contest_id', $contest->contest_id)->count();
            $totalCollection = ((int)$reservedSeats * (int)$contest->joining_fee);
        return view('admin.contest.prize_distribution', ['id' => $id], compact('contest', 'contestData', 'reservedSeats', 'totalCollection'));
    }
    public function prizeDistribution(Request $request){
                $id = $request->id;
                $collection = (int)$request->collection;
                $govt_gst = ($collection*28)/100;
                $remaining_collection = $collection - $govt_gst;
                $admin_revenue = ($remaining_collection*15)/100;
                $winner_revenue = $remaining_collection - $admin_revenue;

                $array1 = $request->winner_id;
                $array2 = $request->winner_percent;
                $a = array_combine($array1,$array2);
                $transaction_id = unique(); 
                $winner_amount = $winner_revenue;
                foreach($a as $key => $v){
                    $wr = $winner_amount;
                    if($wr < $winner_revenue){
                        $winner_revenue = $winner_amount;
                    }
                    
                    $c = explode('-', $key);
                    if(count($c) > 1){  
                        $start = reset($c); 
                        $last = end($c); 
                        for($start; $start <= $last; $start++){
                                    $wid = $start;
                                    $wp = $v;
                                $userPrize = ($winner_revenue*$wp)/100;
                                $winner_amount =$winner_revenue - ($winner_revenue*$wp)/100;
                                $query = ContestWinner::where(['contest_id' => $id, 'random_order' => $wid]);
                                //print_r($query);
                                $checkclone = clone $query;
                                $updateQuery = clone $query;
                                $check = $checkclone->count();
                                if($check > 0){
                                    
                                    $updateQuery->update(['win_amount' => $userPrize]);
                                    $userClone = clone $query;
                                    $userData = $userClone->first();
                                    $user_id = $userData->user_id;
                                    //$userWinAmount = $userData->win_amount;
                                    $fl = LeaderBoard::where('user_id', $user_id);  
                                    $cloneFL = clone $fl;
                                    $cloneFLUpdate = clone $fl;
                                    $findLeader = $cloneFL->first();
                                    $leaderPreviousAmount = (int)$findLeader->win_prize;
                                    $current_amount = $leaderPreviousAmount + $userPrize;
                                    $cloneFLUpdate->update(['win_prize' => $current_amount]);
                                    Wallet::create(['user_id' => $user_id, 'transaction_id' =>transaction_id, 'wallet_amount' => $userPrize, 'type' => 'win', 'pay_status'=>'successful']);
                                    
                                    $prevWalletAmount = UserWallet::where('user_id', $user_id)->first();
                                    $preWalletAmount = $prevWalletAmount->total_amount;
                                    $currentWallet = (int)$preWalletAmount + $userPrize;    
                                    UserWallet::where('user_id', $user_id)->update(['total_amount' => $currentWallet]);
                                }
                        }
                    }else{
                            $wid = reset($c);
                            $wp = $v;
                                $userPrize = ($winner_revenue*$wp)/100;
                                $winner_amount =$winner_revenue - ($winner_revenue*$wp)/100;
                                $query = ContestWinner::where(['contest_id' => $id, 'random_order' => $wid]);
                                //print_r($query);
                                $checkclone = clone $query;
                                $updateQuery = clone $query;
                                $check = $checkclone->count();
                                if($check > 0){
                                    
                                    $updateQuery->update(['win_amount' => $userPrize]);
                                    $userClone = clone $query;
                                    $userData = $userClone->first();
                                    $user_id = $userData->user_id;
                                    
                                    $fl = LeaderBoard::where('user_id', $user_id);  
                                    $cloneFL = clone $fl;
                                    $cloneFLUpdate = clone $fl;
                                    $findLeader = $cloneFL->first();
                                    $leaderPreviousAmount = (int)$findLeader->win_prize;
                                    $current_amount = $leaderPreviousAmount + $userPrize;
                                    $cloneFLUpdate->update(['win_prize' => $current_amount]); 
                                    Wallet::create(['user_id' => $user_id, 'transaction_id' =>transaction_id, 'wallet_amount' => $userPrize, 'type' => 'win', 'pay_status'=>'successful']);
                                    $prevWalletAmount = UserWallet::where('user_id', $user_id)->first();
                                    $preWalletAmount = $prevWalletAmount->total_amount;
                                    $currentWallet = (int)$preWalletAmount + $userPrize;    
                                    UserWallet::where('user_id', $user_id)->update(['total_amount' => $currentWallet]);
                                }
                    } 
                }
        return back()->with('success', 'Prize distributed successfully');
       

    }
    
    public function notificationView(){
        return view('admin.notification.create');
    }
    public function notificationList(){
        $data = Notification::all();    
        return view('admin.notification.index', compact('data'));
    }
    public function leaderBoardList(){
        $data = LeaderBoard::leftJoin('users', 'users.id', '=', 'leader_boards.user_id')->leftJoin('user_details', 'user_details.user_id', '=', 'leader_boards.user_id')->orderBy('win_prize', 'DESC')->get();    
        return view('admin.contest.leader_board', compact('data'));
    }
    public function winnerList(){
        $data = ContestWinner::leftJoin('users', 'users.id', '=', 'contest_winners.user_id')->leftJoin('user_details', 'user_details.user_id', '=', 'contest_winners.user_id')->leftJoin('contests', 'contests.id', '=', 'contest_winners.contest_id')->orderBy('contest_id', 'DESC')->get();    
        return view('admin.contest.winner_list', compact('data'));
    }

     /**
     *Method for upload result.
     *
     * @return \Illuminate\Http\Response
     */   
    public function ContestUploadResult(Request $request,$id=NULL){
        $data = Contest::find($id);
        return view('admin.contest.upload_game_result', compact('data','id'));
    }

    /**
     *Method for upload result Store.
     *
     * @return \Illuminate\Http\Response
     */   
    public function ContestUploadResultStore(Request $request,$id=NULL){
        $input = $request->all();
        try {
            ContestResult::updateOrcreate(
                [
                    'contest_date' => date(DB_DATE_FORMAT,strtotime(now())),
                    'contest_id' => $id
                ],
                [
                    'contest_id' => $id,
                    'result' =>  $input['winning_number']
                ]
            );
            
            return back()->with('success','Result Uploaded successfully !');
            
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }
    
   
}

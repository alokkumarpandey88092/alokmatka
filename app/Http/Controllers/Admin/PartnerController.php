<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserDetail, Videolist, Category, Subcategory, VideoSubcategory, Genre, VideoGenre, Contest, ContestData, ContestResult, UserContest, ContestWinner, LeaderBoard, UserContestWinloss, Notification, Wallet, UserWallet, UserGame};
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class PartnerController extends Controller
{
    private $today;
    public function __construct()
    {
        $this->today = Carbon::today();
    }
    /**
     * Display Partner Dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $user = Auth::user();
        $game = Contest::where('created_by',$user->id)->orderBy('id','desc')->get();
        // dd($game);
        $details = UserGame::get();
        return view('admin.partner-dashboard.dashboard',compact('game','details'));
    }

    /**
     * Display the file of Create Game View.
     *
     * @return \Illuminate\Http\Response
     */
    public function GameCreateView(Request $request){

        return view('admin.partner-dashboard.game_create');
    }

     /**
     * Method for store game data
     *
     * @return \Illuminate\Http\Response
     */
    public function GameCreateStore(Request $request){
        $user = Auth::user();
        $this->validate($request, [
            'contest_name' => 'required|unique:contests,contest_name',
            'jantari_winning_amount' => 'required|numeric|between:0,100',
            'bahar_winning_amount' => 'required|numeric|between:0,100',
            'ander_winning_amount' => 'required|numeric|between:0,100',
            'company_commission' => 'required|numeric|between:0,100',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i|after:start_time',
            'result_time' => 'required|date_format:H:i|after:end_time',

        ], [
            'contest_name.unique' => 'This Game Already Exists !',
        ]);
        //dd($request->all());
        DB::beginTransaction();
        try {
            $contest = [
                'contest_name' => $request->contest_name ? $request->contest_name : NULL,
                'start_date' => $request->start_date ? date(DB_DATE_FORMAT,strtotime($request->start_date)) : NULL,
                'end_date' => $request->end_date ? date(DB_DATE_FORMAT,strtotime($request->end_date)): NULL,
                'start_end_time' => $request->start_time ? $request->start_time : NULL,
                'end_time' => $request->end_time ? $request->end_time : NULL,
                'jantari_winning_amount' => $request->jantari_winning_amount ? $request->jantari_winning_amount : NULL,
                'bahar_winning_amount' => $request->bahar_winning_amount ? $request->bahar_winning_amount : NULL,
                'ander_winning_amount' => $request->ander_winning_amount ? $request->ander_winning_amount : NULL,
                'company_commission' => $request->company_commission ? $request->company_commission : NULL,
                'result_time' => $request->result_time ? $request->result_time : NULL,
                'description' => $request->description ? $request->description : NULL,
                'created_by' => $user->id,
            ];
            //dd($contest);
            if ($request->hasFile('member_logo')) {
                $file = $request->file('member_logo');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/member_logo');
                $file->move($path, $image);
                $contest['member_logo'] = $image;
            }

            if ($request->hasFile('contest_image')) {
                $file = $request->file('contest_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/contest_image');
                $file->move($path, $image);
                $contest['contest_image'] = $image;
            }

            $query = Contest::create($contest);
             DB::commit();
            return redirect()->route('partner.GameHistory')->with('success','Game created successfully');
        } catch (\Throwable $th) {
             DB::rollback();
            //echo $th ;die;
            return back()->with('danger','Something went wrong');
        }
    }

     /**
     * Display the file of game history.
     */
    public function GameHistory(Request $request){
        $user = Auth::user();
        $game_history = Contest::where('created_by',$user->id)->orderBy('id','desc')->get();
        return view('admin.partner-dashboard.game_history',compact('game_history'));
    }

     /**
     * Display the file of Partner Game Settings.
     */
    public function GameSettings(Request $request,$game_id=NULL){
        $user = Auth::user();
        $game = Contest::where(['id'=>$game_id,'created_by'=>$user->id])->first();
        // dd($game);
        $data = User::role('User')
            ->join('user_games', 'users.id', '=', 'user_games.user_id')
            ->get();
        return view('admin.partner-dashboard.game_settings',compact('game','data'));
    }

    public function DataFetch(Request $request)
{
    $user = Auth::user();
    $search = $request->input('winning_number_1');

    $details = UserGame::leftJoin('users as u1', 'user_games.user_id', '=', 'u1.id')
        // ->where('user_games.user_id', $user->id)
        ->whereJsonContains('user_games.user_choise', ['game_number' => $search])
        ->select('user_games.*', 'u1.mobile', 'u1.name')
        ->get();

    return view('admin.partner-dashboard.game_settings', compact('details'));
}



}

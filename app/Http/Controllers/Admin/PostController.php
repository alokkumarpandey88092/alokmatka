<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserDetail,  VideoGenre, Genre, VideoSubcategory, Subcategory, Category, Videolist, Post};
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PostController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and isset($request->toDate)){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();
        $data = Post::whereBetween('created_at', [$from, $to])
                     ->orderBy('id','DESC')->get();
        return view('admin.post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [
            'category' => 'required',
            'title' => 'required',
            'link' => 'required'
        ]);

        try {
            $post_list = [
                'category' => $request->category ? $request->category : NULL,
                'title' => $request->title ? $request->title : NULL,
                'link' => $request->link ? $request->link : NULL,
                'description' => $request->description ? $request->description : NULL
            ];
            if ($request->hasFile('thumb_image')) {
                $file = $request->file('thumb_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/post_thumb');
                $file->move($path, $image);
                $post_list['thumnail'] = $image;
            }
            $query = Post::create($post_list);
            
            return back()->with('success','Post created successfully');
        } catch (\Throwable $th) {
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $data = Post::find($id);
        return view('admin.post.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'category' => 'required',
            'title' => 'required',
            'link' => 'required'
        ]);
        try {
            $post_list = [
                'category' => $request->category ? $request->category : NULL,
                'title' => $request->title ? $request->title : NULL,
                'link' => $request->link ? $request->link : NULL,
                'description' => $request->description ? $request->description : NULL,
            ];
            if ($request->hasFile('thumb_image')) {
                $file = $request->file('thumb_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('assets/post_thumb');
                $file->move($path, $image);
                $exist_img = Post::select('thumnail')->where('id', $id)->first();
                File::delete(public_path('assets/post_thumb/' . $exist_img->thumnail));
                $post_list['thumnail'] = $image;
            }
            $query = Post::where('id',$id)->update($post_list);
            return back()->with('success','Post updated successfully');
        } catch (\Throwable $th) {
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try {
            $query = Post::find($id)->delete();
            if ($query) {
            return back()->with('danger','Post deleted successfully !');
            }
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }
}

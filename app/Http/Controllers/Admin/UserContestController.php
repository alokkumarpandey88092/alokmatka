<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Contest;
use App\Models\UserContest;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        //dd($request->all());
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();
        $user_contest = UserContest::select('user_contests.id', 'user_contests.answer', 'u.name as user_name', 'c.contest_type', 'c.contest_name', 'c.start_date', 'c.end_date','c.start_end_time', 'c.end_time')
        ->leftJoin('contests as c', 'c.id', '=','user_contests.contest_id')
        ->leftJoin('users as u', 'u.id', '=','user_contests.user_id')
        ->whereBetween('user_contests.created_at', [$from, $to])
        ->orderBy('user_contests.id', 'desc')->get();
        return view('admin.user_contest.index',compact('user_contest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

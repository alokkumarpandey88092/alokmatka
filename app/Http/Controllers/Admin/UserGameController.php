<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserDetail, PartnerAccount, Videolist, Category, Subcategory, VideoSubcategory, Genre, VideoGenre, Contest, ContestData, ContestResult, UserContest, ContestWinner, LeaderBoard, UserContestWinloss, Notification, Wallet, UserWallet, UserGame};
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class UserGameController extends Controller
{
    private $today;
    private $game;
    public function __construct()
    {
        $this->game = Contest::where('status',1)->get();
        $this->today = Carbon::today();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $user = Auth::user();
        $user_wallet = UserWallet::where('user_id',$user->id)->first();
        $data = Contest::where('status',1)->orderBy('id','DESC')->get();
        $winning_number = Contest::select('contests.*','cr.result')
                            ->leftjoin('contest_results as cr','cr.contest_id','=','contests.id')
                            ->where('contests.status',1)
                            ->whereDate('contest_date', '=', date(DB_DATE_FORMAT,strtotime(now())))
                            ->orderBy('id','DESC')->get();
        $leader_board = UserGame::select('user_games.*','u.name as user_name', 'cr.result')
                            ->leftjoin('users as u','u.id','=','user_games.user_id')
                            ->leftjoin('contest_results as cr','cr.contest_id','=','user_games.game_id')
                            ->whereDate('contest_date', '=', date(DB_DATE_FORMAT,strtotime(now())))
                            ->orderBy('id','DESC')->get();
        
                if (count($leader_board) > 0) {
                    $leader_board = $leader_board->map(function ($val) {
                        $dataArray = json_decode($val->user_choise, true);
                        $numberExists = false;
                        foreach ($dataArray as $item) {
                            if (isset($item['game_number']) && $item['game_number'] == $val->result) {
                                $numberExists = true; // Set the flag to true if the number is found
                                break; // Exit the loop once the number is found
                            }
                        }
                        $val->is_winner = $numberExists;
                        return $val;
                    })->reject(function ($val) {
                        return !$val->is_winner;
                    });
                }
        //dd($leader_board);               
        //return view('admin.game.dashboard',compact('data','user_wallet','leader_board'));
        //return view('admin.game.dashboard-old',compact('data','user_wallet','leader_board'));
        return view('admin.game-dashboard.dashboard',compact('data','user_wallet','leader_board'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the file of user's game play.
     *
     * @return \Illuminate\Http\Response
     */
    public function playGameView(Request $request,$id=NULL,$game_type=NULL){
        $today = $this->today;
        $user = Auth::user();
        $data = Contest::where('status',1)->find($id);
        $user_wallet = UserWallet::where('user_id',$user->id)->first();
        //dd($data);
        // switch ($game_type) {
        //     case 'jodi_play':
        //         $type_game = 'Jodi Play';
        //         return view('admin.user-game.game_type.play_game_jodi',compact('data', 'user_wallet', 'id', 'today', 'game_type', 'type_game'));
        //         break;
        //     case 'crossing':
        //         $type_game = 'Crossing';
        //             return view('admin.user-game.game_type.play_game_crossing',compact('data', 'user_wallet', 'id', 'today', 'game_type', 'type_game'));
        //             break;
        //     case 'from_to':
        //         $type_game = 'From To';
        //             return view('admin.user-game.game_type.play_game_from_to',compact('data', 'user_wallet', 'id', 'today', 'game_type', 'type_game'));
        //             break;
        //     case 'ander_bahar_haruf':
        //         $type_game = 'Ander-Bahar Haruf';
        //             return view('admin.user-game.game_type.play_game_ander_bahar',compact('data', 'user_wallet', 'id', 'today', 'game_type', 'type_game'));
        //             break;
        //     default:
        //     abort(404, 'The page you are looking for could not be found.');
        //         break;
        // }
        //return view('admin.user-game.play_game',compact('data', 'user_wallet', 'id'));
        //return view('admin.game.play_game',compact('data', 'user_wallet', 'id', 'game_type'));
        return view('admin.game-dashboard.play_game',compact('data', 'user_wallet', 'id', 'game_type'));
    }

    /**
     * Display the file of game type.
     *
     * @return \Illuminate\Http\Response
     */
    public function GameTypeView(Request $request){

        return view('admin.game-dashboard.game_type');
    }

     /**
     * Display the file of game history.
     *
     * @return \Illuminate\Http\Response
     */
    public function GameHistory(Request $request) {
        $user = Auth::user();
        $gameName = $this->game;
        // Get input values
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $selectedGameName = $request->input('gameName');
        // Start building the query
        $query = UserGame::select('user_games.*', 'contests.contest_name as game_name')
            ->leftJoin('contests', 'contests.id', '=', 'user_games.game_id')
            ->where('user_games.user_id', $user->id);
    
            // Add filters if values are provided
            if ($fromDate) {
                $query->whereDate('user_games.created_at', '>=', $fromDate);
            }
            if ($toDate) {
                $query->whereDate('user_games.created_at', '<=', $toDate);
            }
            if ($selectedGameName) {
                $query->where('contests.contest_name', $selectedGameName);
            }
            // Finalize the query
            $game_history = $query->orderBy('user_games.id', 'desc')->get();
            return view('admin.game-dashboard.game_history', compact('game_history', 'gameName','fromDate','toDate','selectedGameName'));
    }
    
     /**
     * Display the file of wallet history.
     *
     * @return \Illuminate\Http\Response
     */
    public function WalletHistory(Request $request){
        $user = Auth::user();
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $selectedType = $request->input('selectType');
        $query = Wallet::select('wallets.*','user_games.id as game_order_id', 'user_games.user_choise', 'contests.contest_name as game_name')
        ->leftJoin('user_games', 'wallets.paid_for', '=', 'user_games.id')
        ->leftJoin('contests', 'contests.id', '=', 'user_games.game_id')
        ->where('wallets.user_id', $user->id);

        // Add filters if values are provided
        if ($fromDate) {
            $query->whereDate('wallets.created_at', '>=', $fromDate);
        }
        if ($toDate) {
            $query->whereDate('wallets.created_at', '<=', $toDate);
        }
        if ($selectedType) {
            $query->where('wallets.type', $selectedType);
        }
        // Finalize the query
        $wallet_history = $query->orderBy('wallets.id', 'desc')->get();
        //dd($wallet_history);
        return view('admin.game-dashboard.wallet_history',compact('wallet_history','fromDate','toDate','selectedType'));
    }

     /**
     * Display the file of game List.
     *
     * @return \Illuminate\Http\Response
     */
    public function GameList(Request $request){
        $data = Contest::where('status',1)->orderBy('id','DESC')->get();
        return view('admin.game-dashboard.game_listing',compact('data'));
    }

    /** 
     * Method For store Play Game.
     *
     * @return \Illuminate\Http\Response
     */
    public function playGameStore(Request $request){
        //dd($request->all());
        $id = Auth::id();
        $input = $request->all();
        $jantariGameData = [];
        $andarGameData = [];
        $baharGameData = [];
        $total_amount = 0;
                //Jantari Game Save loops
                for ($i = 0; $i < count($input['game_number']); $i++) {
                    $jantarigameNumberAmount = $input['game_number_amount'][$i];
                
                    if ($jantarigameNumberAmount !== null && is_numeric($jantarigameNumberAmount)) {
                        $jantarigameNumberAmount = (float) $jantarigameNumberAmount; // Convert to float
                        $item = [
                            'game_number' => $input['game_number'][$i],
                            'game_number_amount' => $jantarigameNumberAmount,
                        ];
                        $jantariGameData[] = $item;
                        $total_amount += $jantarigameNumberAmount;
                    }
                }
            $jantariGameString = json_encode($jantariGameData);

             //Ander Game Save Loop
             for ($i = 0; $i < count($input['ander_game_number']); $i++) {
                $andergameNumberAmount = $input['ander_game_number_amount'][$i];
            
                if ($andergameNumberAmount !== null && is_numeric($andergameNumberAmount)) {
                    $andergameNumberAmount = (float) $andergameNumberAmount; // Convert to float
                    $item = [
                        'ander_game_number' => $input['ander_game_number'][$i],
                        'ander_game_number_amount' => $andergameNumberAmount,
                    ];
                    $andarGameData[] = $item;
                    $total_amount += $andergameNumberAmount;
                }
            }
            $anderGameString = json_encode($andarGameData);

            //Bahar Game Save Loop
            for ($i = 0; $i < count($input['bahar_game_number']); $i++) {
                $bahargameNumberAmount = $input['bahar_game_number_amount'][$i];
            
                if ($bahargameNumberAmount !== null && is_numeric($bahargameNumberAmount)) {
                    $bahargameNumberAmount = (float) $bahargameNumberAmount; // Convert to float
                    $item = [
                        'bahar_game_number' => $input['bahar_game_number'][$i],
                        'bahar_game_number_amount' => $bahargameNumberAmount,
                    ];
                    $baharGameData[] = $item;
                    $total_amount += $bahargameNumberAmount;
                }
            }
            $baharGameString = json_encode($baharGameData);

         DB::beginTransaction();
        try {
             //check if user have sufficient ballance or not
             $check_amt = UserWallet::where('user_id', $id)->first();
             $check_amt = $check_amt ? (int)$check_amt->total_amount : 0;
             if ($check_amt < $total_amount) {
                 return back()->with('danger','Insufficient Ballance Please add ballanec in Your Wallet !');
             }

            $data_to_store = [
                'user_id' => $id,
                'game_id' => $input['game_id'] ? $input['game_id'] : NULL,
                'total_amount' => $total_amount,
                'game_type' => $input['game_type'] ? $input['game_type'] : NULL,
                'user_choise' => $jantariGameString ? $jantariGameString : NULL,
                'user_andar_haruf_choise' => $anderGameString ? $anderGameString : NULL,
                'user_bahar_haruf_choise' => $baharGameString ? $baharGameString : NULL,
            ];
            //return $data_to_store;

            $save_game = UserGame::create($data_to_store);
            $userWalletBallance = UserWallet::where('user_id', $id)->first();
            $userBalanceForUpdate = $userWalletBallance->total_amount - $total_amount;
            $unique_id = Str::random(9);
            $wallets = [
                'user_id' => $id,
                'transaction_id' => $unique_id,
                'wallet_amount' => $total_amount,
                'type' => 'Debit',
                'pay_status' => 'success',
                'paid_for' => $save_game->id,
            ];
            $user_wallet = [
                'total_amount' => $userBalanceForUpdate
            ];
            Wallet::create($wallets);
            UserWallet::where('user_id', $id)->update($user_wallet);
            DB::commit();
            return redirect()->route('user-game.index')->with('success','Congratulations your game submitted successfully !');
        } catch (\Throwable $th) {
            //throw $th;die;
            DB::rollback();
            return back()->with('danger','Something went wrong');
        }
    }

    /** 
     * Method For Add Ballance in Wallet.
     */
    public function addBallanceWallet(Request $request){
    $user = Auth::user();
    $input = $request->all();

    try {
        $request->validate([
            'amount' => 'required|numeric',
        ]);
        $check = UserWallet::where('user_id', $user->id)->first();
        $data = [
            'user_id'           => $user->id, // Fix the variable name
            'transaction_id'    => $request->transaction_id ? $request->transaction_id : Str::random(9),
            'wallet_amount'     => $request->amount,
            'type'              => 'Credit',
            'pay_status'        => 'success',
        ];

        $wallet = Wallet::create($data);
        if ($check) {
            //if ($request->pay_status == 'successful') {
            $previous_amount = $check->total_amount ?? 0; // Handle the case when total_amount is not set
            $current_amount = $request->amount;
            $total_amount = (int)$previous_amount + (int)$current_amount;
            UserWallet::updateOrCreate(
                ['user_id' => $user->id],
                ['total_amount' => $total_amount]
            );
            // } else {
            //     return response()->json([
            //         'success' => false,
            //         'message' => 'Transaction Failed',
            //     ], 200);
            // }
            return back()->with('success', 'Amount added successfully');
        } else {
            UserWallet::create([
                'user_id' => $user->id,
                'total_amount' => $request->amount,
            ]);

                return back()->with('success', 'Amount added successfully');
            }
    } catch (ValidationException $e) {
        return back()->with('danger', 'Invalid amount');
    } catch (Throwable $th) {
        return response()->json([
            'success' => false,
            'message' => 'Something went wrong',
        ], 400);
    }
}
    /**
     * Method For show History of Played Game.
     *
     * @return \Illuminate\Http\Response
     */
    public function playGameHistory(Request $request){
        $user = Auth::user();
        $data = UserGame::select('user_games.*', 'u.name AS user_name', 'c.contest_name AS game_name', 'c.start_end_time', 'c.end_time')
                            ->leftjoin('users as u','u.id','=','user_games.user_id')
                            ->leftjoin('contests as c','c.id','=','user_games.game_id');
        if($user->hasRole(['User'])){
            $data = $data->where('user_games.user_id', $user->id)
                        ->orderBy('user_games.id','DESC')->get();
        }else{
            $data = $data->orderBy('user_games.id','DESC')->get();
        }
        //dd($data);
        return view('admin.user-game.play_game_history',compact('data'));
    }

    /**
     * Method For show Dashboard of Played Game.
     *
     * @return \Illuminate\Http\Response
     */
    public function playGameDashboard(Request $request,$id=NULL){
        $today = $this->today;
        $user = Auth::user();
        $data = Contest::where('status',1)->find($id);
        $user_wallet = UserWallet::where('user_id',$user->id)->first();
        //dd($user_wallet);
        return view('admin.user-game.play_game_dashboard',compact('data', 'user_wallet', 'id', 'today'));
    }

     /**
     * Method For Show data user's previous game history.
     *
     * @return \Illuminate\Http\Response
     */
    public function previousGame(Request $request, $id = NULL){
        $today = $this->today;
        $user = Auth::user();
        // Assuming your UserGame model has a 'created_at' timestamp
        $previousGames = UserGame::select('user_games.*','c.contest_name AS game_name')->leftjoin('contests as c','c.id','=','user_games.game_id')
        ->where('user_games.user_id', $user->id)
        ->whereDate('user_games.created_at', $today)
        ->orderBy('user_games.id', 'desc')->get();
        return response()->json($previousGames);
    }
 
}

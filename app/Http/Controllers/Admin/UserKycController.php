<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserDetail, UserKYC, UserContest};
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
class UserKycController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();
        $user_kyc = UserKYC::select('user_k_y_c_s.*', 'u.name as user_name')
        ->leftJoin('users as u', 'u.id', '=','user_k_y_c_s.user_id')
        ->whereBetween('user_k_y_c_s.created_at', [$from, $to])
        ->orderBy('user_k_y_c_s.id', 'desc')->get();
        return view('admin.user_kyc.index',compact('user_kyc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

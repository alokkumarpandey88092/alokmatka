<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Videolist;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\VideoSubcategory;
use App\Models\Genre;
use App\Models\VideoGenre;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Exception;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data = Videolist::select('videolists.*','cat.category_name')
            ->leftJoin('categories as cat','cat.id','=', 'videolists.category')
            ->orderBy('id','DESC')->get();
        return view('admin.videos.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $videos_category = Category::orderBy('id','desc')->get();
        $videos_subcategory = Subcategory::orderBy('id','desc')->get();
        $videos_genre = Genre::orderBy('id','desc')->get();
        $roles = Role::select('name')->get();
        return view('admin.videos.create',compact('roles','videos_category','videos_subcategory','videos_genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [
            'category' => 'required',
            'subcategory' => 'required',
            'genres' => 'required',
            'title' => 'required',
            'link' => 'required'
        ]);

        try {
            $video_list = [
                'category' => $request->category ? $request->category : NULL,
                'title' => $request->title ? $request->title : NULL,
                'link' => $request->link ? $request->link : NULL,
                'description' => $request->description ? $request->description : NULL,
                'video_thumb' => $request->thumb_image ? $request->thumb_image : NULL,
            ];
            // if ($request->hasFile('thumb_image')) {
            //     $file = $request->file('thumb_image');
            //     $image = uniqid() . $file->getClientOriginalName();
            //     $path = public_path('assets/videos_thumb');
            //     $file->move($path, $image);
            //     $video_list['video_thumb'] = $image;
            // }
            $query = Videolist::create($video_list);
            if ($query) {
                if (isset($request->subcategory)) {
                    for ($i=0; $i < count($request->subcategory) ; $i++) {
                        $subcategory_name = Subcategory::select('subcate_name')->where('id',$request->subcategory[$i])->first();
                        $videos_subcategory = [
                            'video_id' => $query->id,
                            'subcategory_id' => $request->subcategory[$i],
                            'subcategory_name' => $subcategory_name->subcate_name,
                        ];
                        VideoSubcategory::create($videos_subcategory);
                    }
                }
                
                if (isset($request->genres)) {
                    for ($i=0; $i < count($request->genres) ; $i++) {
                        $genre_name = Genre::select('genre_name')->where('id',$request->genres[$i])->first();
                        $videos_genre = [
                            'video_id' => $query->id,
                            'genre_id' => $request->genres[$i],
                            'genre_name' => $genre_name->genre_name,
                        ];
                        VideoGenre::create($videos_genre);
                    }
                }
            }
            return back()->with('success','Video created successfully');
        } catch (\Throwable $th) {
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $data = Videolist::with('videoSubcategory','videoGenre')->find($id);
        //dd($data->videoSubcategory);
        $videos_category = Category::orderBy('id','desc')->get();
        $videos_subcategory = Subcategory::orderBy('id','desc')->get();
        $videos_genre = Genre::orderBy('id','desc')->get();
        return view('admin.videos.edit',compact('data','videos_category','videos_subcategory','videos_genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'category' => 'required',
            'subcategory' => 'required',
            'genres' => 'required',
            'title' => 'required',
            'link' => 'required'
        ]);
        try {
            $video_list = [
                'category' => $request->category ? $request->category : NULL,
                'title' => $request->title ? $request->title : NULL,
                'link' => $request->link ? $request->link : NULL,
                'description' => $request->description ? $request->description : NULL,
                'video_thumb' => $request->thumb_image ? $request->thumb_image : NULL,
            ];
            // if ($request->hasFile('thumb_image')) {
            //     $file = $request->file('thumb_image');
            //     $image = uniqid() . $file->getClientOriginalName();
            //     $path = public_path('assets/videos_thumb');
            //     $file->move($path, $image);
            //     $exist_img = Videolist::select('video_thumb')->where('id', $id)->first();
            //     File::delete(public_path('assets/videos_thumb/' . $exist_img->video_thumb));
            //     $video_list['video_thumb'] = $image;
            // }
            $query = Videolist::where('id',$id)->update($video_list);
            if ($query) {
                if (isset($request->subcategory)) {
                    VideoSubcategory::where('video_id',$id)->delete();
                    for ($i=0; $i < count($request->subcategory) ; $i++) {
                        $subcategory_name = Subcategory::select('subcate_name')->where('id',$request->subcategory[$i])->first();
                        $videos_subcategory = [
                            'video_id' => $id,
                            'subcategory_id' => $request->subcategory[$i],
                            'subcategory_name' => $subcategory_name->subcate_name,
                        ];
                        VideoSubcategory::create($videos_subcategory);
                    }
                }
                
                if (isset($request->genres)) {
                    VideoGenre::where('video_id',$id)->delete();
                    for ($i=0; $i < count($request->genres) ; $i++) {
                        $genre_name = Genre::select('genre_name')->where('id',$request->genres[$i])->first();
                        $videos_genre = [
                            'video_id' => $id,
                            'genre_id' => $request->genres[$i],
                            'genre_name' => $genre_name->genre_name,
                        ];
                        VideoGenre::create($videos_genre);
                    }
                }
            }
            return back()->with('success','Video updated successfully');
        } catch (\Throwable $th) {
            echo $th ;die;
            return back()->with('danger','Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try {
            $query = Videolist::find($id)->delete();
            $query2 = VideoSubcategory::where('video_id', $id)->delete();
            $query3 = VideoGenre::where('video_id', $id)->delete();
            if ($query) {
            return back()->with('danger','Videos deleted successfully !');
            }
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }

     /**
     *Method for Approve Videos.
     *
     * @return \Illuminate\Http\Response
     */   
    public function approveVideos(Request $request,$id){
        $video_list = Videolist::find($id);
        try {
            if($video_list){
                $approved_status = ($video_list->status == 1) ? 0 : 1; 
                //dd($approved_status);
                $approved_msg = ($video_list->status == 1) ? 'Pending' : 'Approved'; 
                Videolist::where('id',$id)->update(['status'=>$approved_status]);  
            }
        return back()->with('success','Video '.$approved_msg.' updated successfully');
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong');
        }
    }

    public function matkaTest(Request $request){
        // Initializing key in some variable. You will receive this key from Eko via email
        $key = "d2fe1d99-6298-4af2-8cc5-d97dcf46df30";

        // Encode it using base64
        $encodedKey = base64_encode($key);

        // Get current timestamp in milliseconds since UNIX epoch as STRING
        // Check out https://currentmillis.com to understand the timestamp format
        $secret_key_timestamp = (int)(round(microtime(true) * 1000));

        // Computes the signature by hashing the salt with the encoded key 
        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        // Encode it using base64
        $secret_key = base64_encode($signature);
        $user_code = 20810200;
        $initiator_id = 9962981729;
        $input=[
            'amount' => 1000,
            'recipient_name' => 'ravi yadav',
            'account' => 234243534,
            'beneficiary_account_type' => 1,
            'payment_mode' => 5,
            'ifsc' => 'SBIN0000001'
        ];
        //dd($input);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "25004",
            CURLOPT_URL => "https://staging.eko.in:25004/ekoapi/v1/agent/user_code:$user_code/settlement",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "initiator_id=$initiator_id&amount=".$input['amount']."&payment_mode=".$input['payment_mode']."&client_ref_id=Settlement7206123420&recipient_name=".$input['recipient_name']."&ifsc=".$input['ifsc']."&account=".$input['account']."&service_code=45&sender_name=Flipkart&source=NEWCONNECT&tag=Logistic&beneficiary_account_type=".$input['beneficiary_account_type'],
            CURLOPT_HTTPHEADER => array(
                "developer_key: becbbce45f79c6f5109f848acd540567",
                "secret-key: ".$secret_key,
                "secret-key-timestamp: ".$secret_key_timestamp
            ),
            CURLOPT_SSL_VERIFYPEER => false, // Disable SSL certificate verification
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);  
        if ($err) {
            return response()->json([
                'success' => false,
                'message' =>  $err
            ]);
        } else {
            return $response;
        }
        
    }
}

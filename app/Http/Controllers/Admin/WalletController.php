<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Wallet;
use App\Models\UserWallet;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;

class WalletController extends Controller
{
    private $allow_permission;

    public function __construct(){
        $this->allow_permission = false;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();
        $user = Auth::user();
        $data = User::select('users.id','users.name','uw.total_amount')
                        ->rightJoin('user_wallets as uw','uw.user_id','=','users.id');
        if($user->hasRole(['User'])){
            $allow_permission = $this->allow_permission;
            $data = $data->where('users.id', $user->id)
                        ->orderBy('users.id','DESC')->get();
        }else{
            $allow_permission = true;
            $data = $data->whereBetween('uw.created_at', [$from, $to])
                        ->orderBy('users.id','DESC')->get();
        }
                    //dd($data);
        return view('admin.wallet.wallet',compact('data','allow_permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $input = $request->all();
        //dd($input);
        $id = Auth::id();
        DB::beginTransaction();
        try {
            $data = [
                'user_id'           => $id,
                'transaction_id'    => 77892,
                'wallet_amount'     => $input['amount'] ? $input['amount'] : NULL,
                'type'              => 'Credit',
                'pay_status'        => 'success'
            ];
            Wallet::create($data);
            $check_total_amount = UserWallet::where('user_id', $id)->first();
            if ($check_total_amount) {
                //if ($request->pay_status == 'successful') {
                    $previous_amount = $check_total_amount->total_amount;
                    $current_amount = $input['amount'];
                    $total_amount = (int)$previous_amount + (int)$current_amount;
                    UserWallet::where('user_id', $id)->update(['total_amount' => $total_amount]);

                // } else {
                //     return response()->json([
                //         'success' => false,
                //         'message' => 'Transaction Failed',
                //     ], 200);
                // }
            } else {
                $wallet =  UserWallet::create([
                    'user_id' => $id,
                    'total_amount' => $input['amount']
                ]);
            }

            DB::commit();
            return back()->with('success','Amount Add Successfully !');
        } catch (\Throwable $th) {
            //throw $th;die;
            DB::rollback();
            return back()->with('danger','Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id=NULL){
        $user = Auth::user();
        $f =  Carbon::now()->startOfMonth()->toDateString();
        $t = Carbon::now()->endOfMonth()->toDateString();
        if(isset($request->fromDate) and $request->toDate){
            $f =  $request->fromDate;
            $t = $request->toDate; 
        }
        $from = Carbon::createFromFormat('Y-m-d', $f)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d',  $t)->endOfDay();

        $data = Wallet::select('transaction_id','wallet_amount','u.name', 'type', 'wallets.created_at as transaction_date')
                ->leftJoin('users as u','u.id','=','wallets.user_id')
                ->where('wallets.user_id',$id);
                
        if($user->hasRole(['User'])){
            $allow_permission = $this->allow_permission;
            $data = $data->orderBy('wallets.id','DESC')->get();
        }else{
            $allow_permission = true;
            $data = $data->whereBetween('wallets.created_at', [$from, $to])
                            ->orderBy('wallets.id','DESC')->get();
        }
        
        return view('admin.wallet.users_wallet',compact('data','allow_permission')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

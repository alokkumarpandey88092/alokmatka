<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\ContestResult;
use App\Models\Wallet;
use App\Models\UserWallet;
use App\Models\AnalystAccount;
use App\Models\QuestionAnswer;
use App\Models\UserAnswer;
use App\Models\UserContestWinloss;
use App\Models\banner;
use App\Models\Genre;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Indexfunds;
use App\Models\Contest;
use App\Models\ContestData;
use App\Models\ContestWinner;
use App\Models\UserContest;
use App\Models\UserKYC;
use App\Models\Post;
use App\Models\LeaderBoard;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\RefreshTokenRepository;
use DB;
use Exception;
use Response;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Classes\AgoraDynamicKey\RtcTokenBuilder;
use Illuminate\Support\Facades\File;


class ApiController extends Controller
{
    protected function sendOTP($mobile, $otp)
    {
        // $sms_api_url = "http://172.105.50.198:5618/SMSApi/send";
        // $sms_api_params = array(
        //     'userid' => 'ccfeltd',
        //     'password' => 'Eq5Q79b6',
        //     'sendMethod' => 'quick',
        //     'mobile' => $mobile,
        //     'msg' => $otp . ' is your verification code for VideosAlarm Powered by Coloured Checkers',
        //     'senderid' => 'VALARM',
        //     'msgType' => 'text',
        //     'dltEntityId' => '1401613590000051630',
        //     'dltTemplateId' => '1407166721049573079',
        //     'duplicatecheck' => 'true',
        //     'output' => 'json'
        // );
        // $sms_api_url .= '?' . http_build_query($sms_api_params);
        // $curl = curl_init($sms_api_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // curl_exec($curl);
        // curl_close($curl);
        //end  


        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://www.mysmsapp.in/api/push.json?apikey=652cd113502eb&sender=BEMAXM&mobileno='.$mobile.'&text=Dear User '.$otp.'is your OTP for BeMax verification.',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_SSL_VERIFYPEER => 0
        ));
        $response = curl_exec($curl);
        curl_close($curl);

    }

    public function sendAppLink(Request $request)
    {
        // http://www.mysmsapp.in/api/push.json?apikey=638dede44a75c&sender=AMASHA&mobileno=$phone&text=Dear $otp your OTP for Login is Pls Keep it confidential. Regards- Team www.analystji.com
        $mobile = $request->mobile;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://www.mysmsapp.in/api/push.json?apikey=638dede44a75c&sender=AMASHA&mobileno=' . $mobile . '&text=Dear Analyst,
          A user is waiting to connect with you. Connect on Analystji Expert App.
          smso.in/hTgQ',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_SSL_VERIFYPEER => 0
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return response()->json([
            'success' => true,
            'message' => 'success',

        ]);
    }

    public function register(Request $request)
    {
        //$a = json_decode($request->all());
        $input = $request->only(['mobile', 'device_token', 'name', 'gender', 'email', 'term_condition']);
        //print_r($input[]);die;

        $validate_data = [
            'mobile'    => 'required|numeric|digits:10|unique:users',
            'name'      => 'required|max:16',
            'gender'    => 'required|numeric',
            'email'     => 'required',
            'term_condition'     => 'required',
            //'device_token'   => 'required',           
        ];
        $customMessages = [
            'required' => 'The :attribute field is required.',
            'mobile.unique' => 'The mobile address is already taken.',
        ];
        $validator = Validator::make($input, $validate_data, $customMessages);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),

            ]);
        }

        try {
            // $user = User::create([
            //     'name' => $input['name'], 
            //     'mobile' =>$input['mobile'], 

            // ]);
            // UserDetail::create([
            //     'user_id' => $user->id,
            // ]);


            //$user->assignRole('User'); //assign role to user
            $mobile = $input['mobile'];
            $otp = rand(1000, 9999);

            $check = DB::table('otp')->where('mobile', $input['mobile'])->first();
            if ($check) {
                $otpSave = DB::table('otp')->where('mobile', $input['mobile'])->update([
                    'name' => $input['name'],
                    'mobile' => $input['mobile'],
                    'email' => $input['email'],
                    'gender' => $input['gender'],
                    'otp' => $otp,
                    'status' => 0,
                ]);
            } else {
                $otpSave = DB::table('otp')->insert([
                    'name' => $input['name'],
                    'mobile' => $input['mobile'],
                    'email' => $input['email'],
                    'gender' => $input['gender'],
                    'otp' => $otp,
                    'status' => 0,
                ]);
            }


            if ($otpSave) {
                $this->sendOTP($mobile, $otp);
            }
            return response()->json([
                'success' => true,
                'message' => 'OTP has been sent on registered mobile no',
                

            ], 200);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went Wrong',

            ], 400);
        }
    }

    public function login(Request $request)
    {
        $input = $request->only(['mobile']);

        $validate_data = [
            'mobile'    => 'required|numeric|digits:10',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            DB::table('otp')->where('mobile', $input['mobile'])->delete();
            $check = User::role(['User', 'analyst'])->where('mobile', $input['mobile'])->count();
            //print_r($input);die;
            if ($check > 0) {
                $mobile = $input['mobile'];
                $otp = rand(1000, 9999);

                $check = DB::table('otp')->where('mobile', $input['mobile'])->first();
                if ($check) {
                    $otpSave = DB::table('otp')->where('mobile', $input['mobile'])->update([

                        'mobile' => $input['mobile'],
                        'otp' => $otp,
                        'status' => 0,
                    ]);
                } else {
                    $otpSave = DB::table('otp')->insert([

                        'mobile' => $input['mobile'],
                        'otp' => $otp,
                        'status' => 0,
                    ]);
                }
                $this->sendOTP($mobile, $otp);
                return response()->json([
                    'success' => true,
                    'message' => 'Otp has been sent on your mobile.',
                    
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'User not registerd',

                ], 200);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went Wrong',

            ], 400);
        }
    }

    public function verifyOtp(Request $request)
    {
        $input = $request->only(['mobile', 'otp', 'device_token']);

        $validate_data = [
            'mobile' => 'required|numeric|digits:10',
            'otp'    => 'required|numeric|digits:4',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {

            if ($request->mobile == 9999999999 && $request->otp == 1234) {
                $user = User::role(['User', 'Analyst'])->with('userdetails')->where(['mobile' => $input['mobile']])->first();
                if ($user) {
                    DB::table('otp')->where(['mobile' => $input['mobile']])->delete();
                    $userid = $user->id;
                    $userToken = User::find($userid);
                    $token = $userToken->createToken('passport_token')->accessToken;

                    if ($user->status != 1) {
                        $string = 'user suspended. Please contact to Admin';
                        return $string;
                    } else {
                        DB::table('otp')->where(['mobile' => $input['mobile']])->delete();
                        if ($request->device_token != null) {

                            User::where('id', $userid)->update(['device_token' => $request->device_token]);
                        }
                        return response()->json([
                            'success'   => true,
                            'message'   => 'OTP verified successfully',
                            'token'     => $token,
                            'user'      => $user
                        ], 200);
                    }
                }
            }


            $otp = DB::table('otp')
                ->where(['mobile' => $input['mobile'], 'otp' => $input['otp'], 'status' => 0]);
            $check = $otp->count();
            //print_r($check);die;
            if ($check > 0) {
                $user = User::role(['User'])->with('userdetails')->where(['mobile' => $input['mobile']])->first();
                if ($user) {
                    DB::table('otp')->where(['mobile' => $input['mobile']])->delete();
                    $userid = $user->id;
                    $userToken = User::find($userid);
                    $token = $userToken->createToken('passport_token')->accessToken;

                    if ($user->status != 1) {
                        $string = 'user suspended. Please contact to Admin';
                        return $string;
                    } else {
                        DB::table('otp')->where(['mobile' => $input['mobile']])->delete();
                        if ($request->device_token != null) {

                            User::where('id', $userid)->update(['device_token' => $request->device_token]);
                        }
                        return response()->json([
                            'success'   => true,
                            'message'   => 'OTP verified successfully',
                            'token'     => $token,
                            'user'      => $user
                        ], 200);
                    }
                } else {

                    $userName = $otp->first();
                    // DB::transaction(function() use($userName){
                    $user = User::create([
                        'name'      => $userName->name,
                        'mobile'    => $userName->mobile,
                        'email'     => $userName->email ? $userName->email : Null,
                        'password'  => Hash::make('12345'),
                        'device_token'  => $request->device_token ? $request->device_token : null,

                    ]);

                    $channal_name = (string) Str::uuid();
                    UserDetail::create([
                        'user_id' => $user->id,

                    ]);
                    UserKYC::create([
                        'user_id' => $user->id,
                        'status' => 0,

                    ]);
                    $token = $user->createToken('passport_token')->accessToken;

                    $user->assignRole('User'); //assign role to user
                    $useridnew = 'AJU' . $user->id;
                    $message = 'User registered successfully';
                    //});

                    User::where('id', $user->id)->update(['userid' => $useridnew]);
                    UserWallet::create([
                        'user_id' => $user->id,
                        'total_amount' => 0,
                    ]);
                    $user = User::role(['User'])->with('userdetails')->where(['mobile' => $input['mobile']])->first();
                    DB::table('otp')->where(['mobile' => $input['mobile']])->delete();
                    return response()->json([
                        'success'   => true,
                        'message'   => $message,
                        'token'     => $token,
                        'user'      => $user
                    ], 200);
                }
            } else {
                $data = DB::table('otp')->where(['mobile' => $input['mobile']])->get();
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid OTP',

                ], 200);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function analystRegistration(Request $request)
    {
        $input = $request->only(['name', 'mobile', 'email', 'sebi', 'device_token']);
        $mobile = $input['mobile'];


        $validate_data = [
            'name'      => 'required',
            'mobile'    => 'required|numeric|digits:10|unique:users',
            'email'     => 'required|unique:users',
            'sebi'      => 'required|unique:user_details',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            // $user = DB::table('users')->orderBy('id', 'DESC')->get();
            // print_r($user);die;

            // $user = User::create([
            //     'name' => $input['name'], 
            //     'mobile' =>$input['mobile'], 
            //     'email' =>$input['email'], 
            //     'password' =>Hash::make('12345'), 

            // ]);

            // UserDetail::create([
            //     'user_id' => $user->id,
            //     'sebi' => $input['sebi'],
            // ]);
            // $user->assignRole('Analyst');
            $otp = rand(1000, 9999);
            DB::table('otp')->insert([
                'name' => $input['name'],
                'mobile' => $input['mobile'],
                'email' => $input['email'],
                'sebi' => $input['sebi'],
                'otp' => $otp,
                'status' => 0,
            ]);
            $this->sendOTP($mobile, $otp);
            return response()->json([
                'success' => true,
                'message' => 'OTP has been sent on registered mobile no',

            ], 200);
        } catch (\Throwable $th) {
            //throw $th;die;

            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function analystLogin(Request $request)
    {
        $input = $request->only(['mobile', 'password']);


        $validate_data = [

            'mobile'    => 'required|numeric|digits:10',
            'password'  => 'required'
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }


        try {
            $user = User::with('userdetails')->where('mobile', $input['mobile'])->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Analyst not registered. Please register',

                ], 200);
            }
            if ($user->hasRole('Analyst')) {
                if (Auth::attempt(array('mobile' => $input['mobile'], 'password' => $input['password']))) {

                    if ($user->status != 1) {
                        return response()->json([
                            'success' => true,
                            'message' => 'Sorry your account has been suspended by Admin. Please contact to Admin',

                        ], 200);
                    } else {
                        $token = $user->createToken('passport_token')->accessToken;
                        return response()->json([
                            'success' => true,
                            'message' => 'Analyst login succesfully.',
                            'token'     => $token,
                            'user'      => $user
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Credentials are not correct',

                    ], 200);
                }
            }
            return response()->json([
                'success' => false,
                'message' => 'Analyst not registered. Please register',

            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function updateProfile(Request $request)
    {   

        $input = $request->only([
            'id',
            'name',
            'profile',
            'email',
        ]);
        //print_r($request->file('profile'));die;

        // $validate_data = [
        //     'id'    => 'required|numeric',
        // ];
        // $validator = Validator::make($input, $validate_data);
        // if ($validator->fails()) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $validator->errors(),

        //     ]);
        // }
        try {
            $user = User::with('userdetails')->where('id', Auth::user()->id)->first();
            
            //return $user['userdetails'][0];die;
            if ($user) {
                if ($request->file('profile')) {
                    $file = $request->file('profile');
                    $image = uniqid() . $file->getClientOriginalName();
                    $path = public_path('/members_image');
                    $file->move($path, $image);
                    
                }else{
                    $image = $user['userdetails'][0]->profile;
                }
                $name =  $request ? $request->name : $user->name;
                $email =  $request ? $request->email : $user->email;
                //print_r($request->all());die;
                //$add = $user['userdetails'][0]->address;
                
                $result = User::find(Auth::user()->id);
                $result->update(['name'=> $name, 'email' => $email]);
                $result->userdetails()->update(['profile' => $image]);

                // $result = User::where('id', $input['id'])->update($datauser);
                // $result1 = UserDetail::where('user_id', $input['id'])->update($data);
                return response()->json([
                    'success'   => true,
                    'message'   => 'Data update successfully',
                    'data'      => $user,

                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'id not matched',

                ], 200);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function analystList(Request $request)
    {


        try {

            $list = User::select(
                'users.id',
                'users.name',
                'users.mobile',
                'users.email',
                'users.status',
                'users.device_token',
                'ud.channal_name',
                'ud.profile',
                'ud.language',
                'ud.location',
                'ud.address',
                'ud.experience',
                'ud.consultant',
                'ud.sebi',
                'ud.bio',
                'ud.description',
                "ud.voice_call_charges",
                "ud.video_call_charges",
                "ud.chat_call_charges",
                "ud.voice_call_enable",
                "ud.video_call_enable",
                "ud.chat_call_enable",
                "ud.analyst_charges",
                "ud.approved",
                'ud.is_busy'
            )
                ->leftjoin('user_details as ud', 'ud.user_id', '=', 'users.id')
                ->where('ud.approved', 1)
                ->orderBY('ud.voice_call_charges', 'ASC')
                ->get();
            return response()->json([
                'success' => true,
                'message' => '',
                'analyst_list' => $list,

            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ], 200);
        }
    }

    public function userDetails(Request $request)
    {
        $id = Auth::user()->id;
        try {
            $user = User::where('id', $id)->get();
            $data = $user->map(function ($item) {
                $dynamicContestTime = UserKYC::where('user_id', $item->id)->first();
                $userDetail = UserDetail::where('user_id', $item->id)->first();
                $userWallet = UserWallet::where('user_id', $item->id)->first();
                $item->bank_name = $dynamicContestTime->bank_name;
                $item->bank_number = $dynamicContestTime->bank_account;
                $item->profile = $userDetail->profile;
                $item->address = $userDetail->address;
                $item->gender = $userDetail->gender;
                $item->total_amount = $userWallet->total_amount;

                return $item;
            });
            return response()->json([
                'success'   => true,
                'data'      =>  $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ], 200);
        }
    }

    public function addWallet(Request $request)
    {

        $input = $request->only(['transaction_id', 'amount', 'pay_status']);
        $id = $request->user()->id;
        $validate_data = [
            'transaction_id'    => 'required',
            'amount'            => 'required',
            'pay_status'        => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {

            $check = UserWallet::where('user_id', $id)->first();
            $data = [
                'user_id'           => $id,
                'transaction_id'    => $request->transaction_id,
                'wallet_amount'     => $request->amount,
                'type'              => 'Credit',
                'pay_status'        => $request->pay_status,
            ];
            // print_r($data);
            // die;
            $wallet =  Wallet::create($data);

            if ($check) {
                if ($request->pay_status == 'successful') {
                    $previous_amount = $check->total_amount;
                    $current_amount = $request->amount;
                    $total_amount = (int)$previous_amount + (int)$current_amount;
                    UserWallet::where('user_id', $id)->update(['total_amount' => $total_amount]);
                    return response()->json([
                        'success' => true,
                        'message' => 'Amount added successfully',
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Transaction Failed',
                    ], 200);
                }
            } else {
                $wallet =  UserWallet::create([
                    'user_id' => $id,
                    'total_amount' => $request->amount,
                ]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function userWallet(Request $request)
    {
        $id = $request->user()->id;

        try {
            $data = UserWallet::where('user_id', $id)->first();
            $amount = $data->total_amount;
            return response()->json([
                'success'   => true,
                'data'      =>  $amount
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function userWalletTransaction(Request $request)
    {
        $id = $request->user()->id;

        try {
            $wallet_history = DB::Table('wallets')->select('transaction_id', 'wallet_amount', 'type', 'pay_status', 'created_at')->where('user_id', $id)->orderBy('id', 'DESC')->get();

            return response()->json([
                'success' => true,
                'message' => 'User wallet history',
                'data' => $wallet_history
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function userCallLog(Request $request)
    {
        $id = $request->user()->id;
        try {
            $data = AnalystAccount::select('u.name as analyst', 'analyst_accounts.call_type', 'analyst_accounts.call_start', 'analyst_accounts.call_end', 'analyst_accounts.call_duration', 'analyst_accounts.amount', 'analyst_accounts.call_recording')
                ->leftjoin('users as u', 'u.id', '=', 'analyst_accounts.analyst_id')
                ->where('user_id', $id)
                ->orderBy('analyst_accounts.id', 'DESC')
                ->get();

            return response()->json([
                'success'   => true,
                'data'      =>  $data
            ], 200);
        } catch (\Throwable $th) {
            // throw $th;
            // die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',
            ], 400);
        }
    }

    public function Agoratoken(Request $request)
    {
        $input = $request->only(['analyst_id']);
        $id = $request->user()->id;
        $validate_data = [
            'analyst_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        $user = User::with('userdetails')->where('id', $id)->first();
        $name =  $user->name;
        $appID = env('AGORA_APP_ID');
        $appCertificate = env('AGORA_APP_CERTIFICATE');
        $channelName = $user->userdetails->channal_name;
        $user = $name; //$user()->name;
        $role = 'Analyst'; //RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = now()->getTimestamp();
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

        $token = RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);

        return response()->json([
            'success' => True,
            'message' => '',
            'token'   => $token

        ]);
    }

    function send_notification(Request $request)
    {
        $input = $request->only(['device_token', 'title', 'message']);
        $id = $request->user()->id;
        $validate_data = [
            'device_token'    => 'required',
            'title'    => 'required',
            'message'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        $device_token = $request->device_token;
        error_reporting(0);


        try {
            $fcmMsg = array(
                'body' => 'testing', //$msg,
                'title' => 'testing', //$title,
                'sound' => "default",
            );

            $fcmFields = array(
                'to' => trim($device_token),
                'priority' => 'high',
                'notification' => $fcmMsg
            );
            $headers = array(
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
            $result = curl_exec($ch);
            curl_close($ch);
            echo 'success';
            return $result;
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Someting went wrong'
            ]);
        }
        //$id,$voice,$location

    }

    public function updateDeviceToken(Request $request)
    {
        $input = $request->only('device_token');
        $id = $request->user()->id;
        $validate_data = [
            'device_token'    => 'required',

        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            $result = User::where('id', $id)->update(['device_token' => $request->device_token]);
            if ($result) {
                return response()->json([
                    'success' => True,
                    'message' => 'device token updated successfully',
                ]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Someting went wrong'
            ]);
        }
    }

    public function preQuestionnaire()
    {
        $data = QuestionAnswer::select('question')->where('status', 1)->get();
        return response()->json([
            'success' => false,
            'message' => '',
            'data' => $data

        ]);
    }

    public function userAnswere(Request $request)
    {

        $input = $request->only(['user_id', 'analyst_id', 'answer', 'is_skip']);
        $validate_data = [
            'user_id'    => 'required',
            'analyst_id' => 'required',
            'is_skip'    => 'required',

        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            $call_id = rand(10000, 99999);
            UserAnswer::create([
                'call_id' => $call_id,
                'user_id' => $request->user_id,
                'analyst_id' => $request->analyst_id,
                'answer' => $request->answer,
                'is_skip' => $request->is_skip,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Answer submitted successfully'
            ]);
        } catch (\Throwable $th) {
            // throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Someting went wrong'
            ]);
        }
    }

    public function answerAnalyst(Request $request)
    {
        $input = $request->only(['user_id']);
        $validate_data = [
            'user_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            $id = $request->user()->id;
            $user_id = $request->user_id;
            $ans = UserAnswer::select('answer', 'is_skip')->where(['user_id'  => $user_id, 'analyst_id' => $id])->orderBy('id', 'desc')->first();
            return response()->json([
                'success' => true,
                'message' => $ans,

            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function videoAndChatCallProcess(Request $request)
    {
        $input = $request->only(['user_id', 'analyst_id', 'call_start', 'call_end', 'call_type']);
        $validate_data = [
            'user_id'    => 'required',
            'analyst_id' => 'required',
            'call_start' => 'required',
            'call_end'   => 'required',
            'call_type'  => 'required',

        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            $user_id = User::where('mobile', $input['user_id'])->first();
            $userid = $user_id->id;
            $expert = User::with('userdetails')->where('mobile', $input['analyst_id'])->first();

            $analyst_id     = $expert->id;
            $call_type      = $request->call_type;
            $call_start     = $request->call_start;
            $call_end       = $request->call_end;
            $analyst_percent  = $expert['userdetails']->analyst_charges;

            $admin_percentage = 100 - $analyst_percent;
            $start  = new Carbon($call_start);
            $end    = new Carbon($call_end);
            if ($call_type == 2) {
                $call_charges    = $expert['userdetails']->video_call_charges;
            } else if ($call_type == 3) {
                $call_charges    = $expert['userdetails']->chat_call_charges;
            }
            if ($call_end != 0) {
                $call_duration = $start->diffInSeconds($end);
            } else {
                $call_duration = 0;
            }

            if ($call_duration > 30) {
                $actual_call_duration = $call_duration - 30;
                $amount =   ($call_charges / 60) * $actual_call_duration;
                $amount = round($amount, 2);

                $analyst_amount = ($amount * $analyst_percent) / 100;
                $analyst_amount = round($analyst_amount, 2);
                $admin_amount = $amount - $analyst_amount;
            } else {
                $actual_call_duration = $call_duration;
                //$amount = ($call_charges/60)*$actual_call_duration;
                $amount = 0; //round($amount, 2);
                $analyst_amount = 0; // round($analyst_amount, 2);
                $admin_amount = 0;
            }

            $data = [
                'analyst_id' => $analyst_id,
                'user_id' => $userid,
                'call_type' => $call_type,
                'call_start' => $call_start,
                'call_end' => $call_end,
                'call_duration' => $call_duration,
                'amount' => $amount,
                'analyst_call_price' => $call_charges,
                'analyst_percentage' => $analyst_percent,
                'admin_percentage' => $admin_percentage,
                'analyst_amount' => $analyst_amount,
                'admin_amount' => $admin_amount,
                'reconcilation' => 'pending',
            ];
            AnalystAccount::create($data);
            $userWalletData = UserWallet::where('user_id', $userid)->first();

            $userBalance = $userWalletData->total_amount - $amount;
            $unique_id = Str::random(9);
            $wallet_data = [
                'user_id' => $userid,
                'transaction_id' => $unique_id,
                'wallet_amount' => $amount,
                'type' => 'Debit',
            ];

            $user_wallet = [
                'user_id' => $userid,
                'total_amount' => $userBalance
            ];
            Wallet::create($wallet_data);
            UserWallet::where('user_id', $userid)->update($user_wallet);
            return response()->json([
                'success' => true,
                'message' => 'Account updated successfully'
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Someting went wrong'
            ]);
        }
    }

    public function busyStatus(Request $request)
    {
        $input = $request->only('analyst_no', 'is_busy');
        $validate_data = [

            'analyst_no'    => 'required',
            'is_busy'       => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        $mobile = $request->analyst_no;
        $is_busy = $request->is_busy;
        try {
            DB::select("UPDATE users u INNER JOIN  user_details ud on ud.user_id = u.id SET ud.is_busy = $is_busy WHERE u.mobile = $mobile");
            return response()->json([
                'success' => true,
                'message' => 'Analyst busy successfully'
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Someting went wrong'
            ]);
        }
    }

    public function resetPassword(Request $request)
    {
        $input = $request->only(['old_password', 'new_password', 'mobile']);
        if (count($request->toArray()) > 3) {
            return response()->json([
                'success' => false,
                'message' => "Extra parameter are not allowed. Only ('old_password', 'new_password', 'mobile')",

            ]);
        }
        $validate_data = [
            'new_password'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            if (isset($input['old_password'])) {
                //print_r($input['old_password']);die;
                if (Auth::attempt(array('mobile' => $input['mobile'], 'password' => $input['old_password']))) {

                    $password = Hash::make($input['new_password']);
                    $data = ['password' => $password];
                    $result = User::where('mobile', $input['mobile'])->update($data);
                    //Auth::logoutOtherDevices($password);
                    return response()->json([
                        'success' => true,
                        'message' => 'Password updated successfully',

                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Password updated  not',

                    ]);
                }
            } else {
                $password = Hash::make($input['new_password']);
                $result = User::where('mobile', $input['mobile'])->update(['password' => $password]);
                //Auth::logoutOtherDevices($password);
                return response()->json([
                    'success' => true,
                    'message' => 'Password updated successfully',

                ]);
            }
        } catch (\Throwable $th) {
            // throw $th;
            // die;
            return response()->json([
                'success' => false,
                'message' => 'something went wrong',

            ]);
        }
    }

    public function checkCallBalance(Request $request)
    {
        $input = $request->only(['user_balance', 'analyst_charges']);
        $validate_data = [

            'user_balance'    => 'required',
            'analyst_charges'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {


            $user_balance = $input['user_balance'];
            $analyst_charges = $input['analyst_charges'];
            if ($user_balance > $analyst_charges) {
                $data = ($user_balance / $analyst_charges) * 60;
                $data = round($data);

                return response()->json([
                    'success' => true,
                    'message' => 'success',
                    'data' => $data

                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Insufficient balance. Please add money to wallet',

                ]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

    public function makeLive(Request $request)
    {
        $input = $request->only(['channal_name']);
        $validate_data = [

            'channal_name'    => 'required',

        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            $id = $request->user()->id;
            $channal_name = $input['channal_name'];

            DB::table('analyst_live')->insert(['analyst_id' => $id, 'channal_name' => $channal_name]);
            return response()->json([
                'success' => true,
                'message' => 'success',

            ]);
        } catch (\Throwable $th) {
            //throw $th;die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

    public function liveAnalyst(Request $request)
    {
        $list = DB::table('analyst_live')
            ->select('users.id as analyst_id', 'users.name', 'user_details.profile', 'analyst_live.channal_name', 'user_details.live_stream_charges')
            ->leftjoin('users', 'users.id', '=', 'analyst_live.analyst_id')
            ->leftjoin('user_details', 'user_details.user_id', '=', 'analyst_live.analyst_id')
            ->get();
        return response()->json([
            'success' => true,
            'message' => $list,

        ]);
    }

    public function endLive(Request $request)
    {

        try {
            $id = $request->user()->id;
            DB::table('analyst_live')->where('analyst_id', $id)->delete();
            DB::table('user_join_live')->where('analyst_id', $id)->delete();
            return response()->json([
                'success' => true,
                'message' => 'Success',

            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function banner(Request $request)
    {

        try {
            $data = banner::select('banner')->get();
            return response()->json([
                'success' => true,
                'message' => 'Success',
                'data' => $data,

            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function userJoinLive(Request $request)
    {
        $input = $request->only('analyst_id', 'amount');
        $validate_data = [

            'analyst_id'    => 'required',
            'amount'        => 'required',

        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
            $userid = $request->user()->id;
            $analyst_id = $request->analyst_id;
            $amount = $request->amount;

            $expert = User::with('userdetails')->where('id', $analyst_id)->first();

            $analyst_id         = $expert->id;
            $analyst_percent    = $expert['userdetails']->analyst_charges;
            $call_charges    = $expert['userdetails']->live_stream_charges;
            $admin_percentage   = 100 - $analyst_percent;
            $analyst_amount = ($amount * $analyst_percent) / 100;
            $analyst_amount = round($analyst_amount, 2);
            $admin_amount = $amount - $analyst_amount;

            $data = [
                'analyst_id' => $analyst_id,
                'user_id' => $userid,
                'call_type' => 4,
                'call_start' => null,
                'call_end' => null,
                'call_duration' => null,
                'amount' => $amount,
                'analyst_call_price' => $call_charges,
                'analyst_percentage' => $analyst_percent,
                'admin_percentage' => $admin_percentage,
                'analyst_amount' => $analyst_amount,
                'admin_amount' => $admin_amount,
                'reconcilation' => 'pending',
            ];
            AnalystAccount::create($data);
            $userWalletData = UserWallet::where('user_id', $userid)->first();

            $userBalance = $userWalletData->total_amount - $amount;
            $unique_id = Str::random(9);
            $wallet_data = [
                'user_id' => $userid,
                'transaction_id' => $unique_id,
                'wallet_amount' => $amount,
                'type' => 'Debit',
            ];

            $user_wallet = [
                'user_id' => $userid,
                'total_amount' => $userBalance
            ];
            Wallet::create($wallet_data);
            UserWallet::where('user_id', $userid)->update($user_wallet);
            $detail = ['analyst_id' => $analyst_id, 'user_id' => $userid];
            $userLive = DB::table('user_join_live')->insert($detail);
            return response()->json([
                'success' => true,
                'message' => 'User can access live streaming'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function checkLiveAccess(Request $request)
    {
        $input = $request->only('analyst_id');
        $validate_data = [
            'analyst_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            $id = $request->user()->id;
            $userLive = DB::table('user_join_live')->where(['analyst_id' => $request->analyst_id, 'user_id' => $id])->first();
            if ($userLive) {
                return response()->json([
                    'success' => true,
                    'message' => 'Permission granted'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'User cannot access live streaming'
                ]);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function genreList()
    {
        $list = Genre::select('id', 'thumbnail', 'genre_name')->get();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $list
        ]);
    }
    public function categoryList()
    {
        $list = Category::select('id', 'category_name')->get();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $list
        ]);
    }
    public function subcategoryList()
    {
        $list = Subcategory::select('id', 'subcate_name')->get();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $list
        ]);
    }

    public function videoList()
    {
        try {
            //$video = Videolist::with('videoSubcategory', 'videoGenre')->get();
            $video = VideoSubcategory::select('vcat.category_name', 'vl.link', 'vl.video_thumb', 'vl.description', 'video_subcategories.subcategory_name')
                ->leftjoin('videolists as vl', 'vl.id', '=', 'video_subcategories.video_id')
                ->leftjoin('categories as vcat', 'vcat.id', '=', 'vl.category')
                ->leftjoin('video_genres as gn', 'gn.video_id', '=', 'vl.id')
                ->where('vl.status', 1)
                ->get();
            $video = Subcategory::select('id', 'subcate_name')->get();
            $data = [];
            $a = [];
            $b = [];
            foreach ($video as $key => $val) {
                $a['title'] = $val->subcate_name;
                // $data[] = VideoSubcategory::select('vcat.category_name', 'vl.link', 'vl.video_thumb', 'vl.description','video_subcategories.subcategory_name')
                //                     ->leftjoin('videolists as vl', 'vl.id', '=', 'video_subcategories.video_id')
                //                     ->leftjoin('categories as vcat', 'vcat.id', '=', 'vl.category')
                //                     ->leftjoin('video_genres as gn', 'gn.video_id', '=', 'vl.id')
                //                     ->where('video_subcategories.subcategory_id', $val->id)
                //                     ->where('vl.status', 1)
                //                     ->get();
                $a['details'] = VideoSubcategory::select('vl.title', 'vl.link', 'vl.video_thumb', 'vl.description')
                    ->leftjoin('videolists as vl', 'vl.id', "=", 'video_subcategories.video_id')
                    ->where('video_subcategories.subcategory_id', $val->id)
                    ->where('vl.status', 1)
                    ->get();
                array_push($data, $a);
                //array_push($data, $b );
            }


            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function genreVideo(Request $request)
    {
        $input = $request->only('genre_id');
        $validate_data = [
            'genre_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            $data =  VideoGenre::select('*')
                ->leftjoin('videolists', 'video_genres.video_id', '=', 'videolists.id')
                ->leftjoin('video_subcategories', 'video_subcategories.video_id', '=', 'videolists.id')
                ->where('video_genres.genre_id', $input['genre_id'])
                ->get();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    public function subcategoryVideo(Request $request)
    {
        $input = $request->only('subcategory_id');
        $validate_data = [
            'subcategory_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            $data =  VideoSubcategory::select('*')
                ->leftjoin('videolists', 'video_subcategories.video_id', '=', 'videolists.id')
                ->leftjoin('video_genres', 'video_genres.video_id', '=', 'videolists.id')
                ->where('video_subcategories.subcategory_id', $input['subcategory_id'])
                ->get();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            // $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function movieList(Request $request)
    {
        try {
            $data = Videolist::where('category', 1)->get();

            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    public function videosList(Request $request)
    {
        try {
            $data = Videolist::where('category', 3)->get();

            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    public function tvshowsList(Request $request)
    {
        try {
            $data = Videolist::where('category', 2)->get();

            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            //echo $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function BannerList(Request $request)
    {
        try {
            $data =  banner::select('banners.*', 'vl.link', 'vl.title', 'vl.video_thumb', 'vl.description')
                ->leftjoin('videolists as vl', 'vl.id', '=', 'banners.video_id')->orderBy('banners.id', 'desc')->get();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function termCondition(Request $request)
    {
        $data  = DB::table('term_and_conditions')->first();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $data
        ], 200);
    }
    public function privacy_policy(Request $request)
    {
        $data  = DB::table('privacy_policy')->first();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $data
        ], 200);
    }

    public function indexfund(Request $request)
    {
        try {
            $data = Indexfunds::all();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function calculateDynamicContestTime($id)
    {
        $result = Contest::where('id', $id)->first();
        $current_time = now();
        $end_time = Carbon::parse($result->end_time);
        $timeDifference = $end_time->diff($current_time);
        $timeDifference = $timeDifference->format('%h:%i:%s');
        
        if ($current_time < $end_time) {
            // Current time is less than the specific time
           return $timeDifference;
        } else {
            // Current time is equal to or greater than the specific time
            // You can perform other actions here if needed
            return "00:00:00"; // For example, set $result to 1
        }
        
    }
    
    public function remainingSeats($id){
        $userjoined = UserContest::where('contest_id', $id)->count();
        $contest = Contest::where('id', $id)->first();
        $remainingSeats = (int)$contest->seats - $userjoined;
        return $remainingSeats;
    }
    public function contest(Request $request)
    {
        try {
            $result = Contest::where('start_date', now()->toDateString())->orderBy('id', 'DESC')->get();
            $data = $result->map(function ($item) {
                $dynamicContestTime = $this->calculateDynamicContestTime($item->id);
                $item->remaining_time = $dynamicContestTime;

                return $item;
            });
            $data = $result->map(function ($item) {
                $remaining = $this->remainingSeats($item->id);
                $item->remaining_seats = (int)$remaining;

                return $item;
            });
            //$data = [];
            // foreach ($result as $key => $val) {
            //     $val->contest_time = '10:12:00';
            // }
            //$data['current_time'] = now()->format('H:i:s');
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            // echo $th;
            // die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    public function post(Request $request)
    {
        try {
            $data = Post::orderBy('id', 'DESC')->get();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function contestData(Request $request)
    {
        $input = $request->only('contest_id');
        $validate_data = [
            'contest_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            $result = ContestData::where('contest_id', $input['contest_id'])->get();
            $remaining_time = $this->calculateDynamicContestTime($result[0]->contest_id);
            //$result = $result->toArray(); // Convert collection to array
            //$reult[0] = ['remaining_time' => $remaining_time];
            // Add the remaining_time as a new key-value pair

            return response()->json([
                'success' => true,
                'message' => '',
                'remaining_time' => $remaining_time,
                'data' => $result
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function joinContest(Request $request)
    {   
        
        
        $input = $request->only('user_id', 'contest_id', 'share_list');

        $validate_data = [
            'contest_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        $contest = Contest::where('id', $input['contest_id'])->first();
        $contest_fee = $contest->joining_fee;
        $contest_enddate = Carbon::parse($contest->end_date)->toDateString();
        $contest_endtime = Carbon::parse($contest->end_time)->toTimeString();
        $currentTime = Carbon::now()->toTimeString();
        $user_wallet = DB::table('user_wallets')->where('user_id', $input['user_id'])->first();
        $user_contest = UserContest::where(['contest_id' => $input['contest_id']]);
        $user_contest_clone_count = clone $user_contest;                         
        $user_contest_clone_data = clone $user_contest;                         
        $contest_booking = $user_contest_clone_count->count();
        $userBalance = $user_wallet->total_amount;
        if ($contest_fee > $userBalance) {
            return response()->json([
                'success' => false,
                'message' => 'Insufficient wallet balance to join contest',

            ]);
        }

        if ($contest_enddate <= now()->toDateString() && $contest_endtime < $currentTime) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry contest is finised',

            ]);
        }
        if ($contest_booking == $contest->seats) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry no seats available for booking the contest',

            ]);
        }


        try {
            $a = $request->share_list;
            $ans = "";
            foreach($a as $v){
                $ans .= $v['id'].",";
            }   
            $ans = rtrim($ans, ', ');  
    
            $check = $user_contest_clone_data->where('user_id', $input['user_id'])->count();
            $remainingBalance = (int)$userBalance - (int)$contest_fee;
            if($check == 0){
            //dd($remainingBalance);
                $updateWallet = DB::table('user_wallets')->where('user_id', $input['user_id'])->update(['total_amount' => $remainingBalance]);
             Wallet::insert(['user_id'=>$input['user_id'],'transaction_id' => rand(1000, 999999), 'wallet_amount'=> (int)$contest_fee, 'type' => 'Entry paid', 'pay_status' => 'successful']);
            }
            UserContest::updateOrInsert(['contest_id' => $input['contest_id']],['user_id' => $input['user_id'],     'contest_id' => $input['contest_id'], 'answer' => $ans, 'created_at'=>now(), 'updated_at'=>now()]);
            
            // check Leader board entry available or not if not then create new entry

            $check = LeaderBoard::where('user_id', $input['user_id'])->count();
            if($check == 0){
                LeaderBoard::create(['user_id' => $input['user_id']]);
            }

            return response()->json([
                'success' => true,
                'message' => 'Congratulations contest is joined successfully',
                'balance' => $remainingBalance

            ]);
        } catch (\Throwable $th) {
            // throw $th;
            //  die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

    public function userContesList(Request $request)
    {
        $user_id = $request->user()->id;
        try {
            $data = UserContest::select('contests.*','users.name', 'contests.contest_name', 'contests.id as contest_id','contests.contest_image', 'contest_data.share_name','user_contests.answer', DB::raw('DATE_FORMAT(user_contests.created_at, "%Y-%m-%d %H:%i:%s") as created_at'))
                ->leftjoin('users', 'users.id', '=', 'user_contests.user_id')
                ->leftjoin('contests', 'contests.id', '=', 'user_contests.contest_id')
                ->leftjoin('contest_data', 'contest_data.id', '=', 'user_contests.contest_id')
                ->where('user_contests.user_id', $user_id)->orderBy('user_contests.id', 'DESC')
                ->get();
            $data = $data->map(function ($item) {
                $dynamicContestTime = $this->calculateDynamicContestTime($item->contest_id);
                if($item->contest_date < now()){
                
                $item->remaining_time = "00:00:00";
                }else{
                
                    $item->remaining_time = $dynamicContestTime;
                }

                return $item;
            });
            $data = $data->map(function ($item) {
                $remaining = $this->remainingSeats($item->contest_id);
                $item->remaining_seats = $remaining;

                return $item;
            });
            $data = $data->map(function ($item) {
               $answer = explode(',', $item->answer);
                $newArray = [];

            foreach ($answer as $element) {
                $sharename = ContestData::where('id', $element)->first();
                $a['id'] = (int)$element;
                $a['share_name'] = $sharename->share_name;
                $a['contest_id'] = (int)$sharename->contest_id;
                array_push($newArray, $a);
            }
               $item->useranswer = $newArray; 
                return $item;
            });
            return response()->json([
                'success' => true,
                'message' => '',
                'data'  => $data

            ]);
        } catch (\Throwable $th) {
            //throw $th;die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function UpdateUserContest(Request $request)
    {
        $input = $request->only('user_id', 'contest_id', 'share_id', 'join_id');

        $validate_data = [
            'contest_id'    => 'required',
            'join_id'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        // $contest = Contest::where('id', $input['contest_id'])->first();
        // $contest_price = $contest->joining_fee;
        $contest_enddate = Carbon::parse($contest->end_date)->toDateString();
        $contest_endtime = Carbon::parse($contest->end_time)->toTimeString();
        $currentTime = Carbon::now()->toTimeString();
        $user_wallet = DB::table('user_wallets')->where('user_id', $input['user_id'])->first();
        $contest_booking = UserContest::where(['contest_id' => $input['contest_id']])->count();
        // if ($contest_price > $user_wallet->total_amount) {
        //     return response()->json([
        //         'success' => true,
        //         'message' => 'Insufficient wallet balance to join contest',

        //     ]);
        // }

        if ($contest_enddate <= now() && $contest_endtime < $currentTime) {
            return response()->json([
                'success' => true,
                'message' => 'Sorry contest is finised',

            ]);
        }
        if ($contest_booking == $contest->seats) {
            return response()->json([
                'success' => true,
                'message' => 'Sorry no seats available for booking the contest',

            ]);
        }


        try {
            UserContest::where('id', $input['join_id'])->update(['contest_id' => $input['contest_id'], 'share_id' => $input['share_id']]);
            return response()->json([
                'success' => true,
                'message' => 'Congratulations contest is joined successfully',

            ]);
        } catch (\Throwable $th) {
            // throw $th;
            // die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

    public function checkKyc(Request $request)
    {
        $user_id = Auth::user()->id;
        $data = UserKYC::select( 'users.name','user_k_y_c_s.*')->leftjoin('users', 'users.id', '=', 'user_k_y_c_s.user_id')->where('user_id', $user_id)->get();


        return response()->json([
            'success' => true,
            'message' => '',
            'data'  => $data
        ]);
    }

    public function userKyc(Request $request)
    {
        $input = $request->only('pan_image', 'mobile', 'email',  'address', 'bank_name', 'bank_account', 'bank_ifsc', 'dob', 'pan_number');

        $validate_data = [
            'pan_image'    => 'required',
            'mobile'    => 'required',
            'email'    => 'required',
            'address'    => 'required',
            'bank_name'    => 'required',
            'bank_account'    => 'required',
            'bank_ifsc'    => 'required',
            'dob'    => 'required',
            'pan_number'    => 'required',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }


        try {
            if ($request->file('pan_image')) {
                $file = $request->file('pan_image');
                $image = uniqid() . $file->getClientOriginalName();
                $path = public_path('/kyc_document');
                $file->move($path, $image);
            } else {
                $image = null;
            }
            $user_id = Auth::user()->id;

            $result = UserKYC::where('user_id', $user_id)->update([
                'pan_image' => $image,
                'mobile' => $input['mobile'],
                'email' => $input['email'],
                'address' => $input['address'],
                'bank_name' => $input['bank_name'],
                'bank_account' => $input['bank_account'],
                'bank_ifsc' => $input['bank_ifsc'],
                'status' => 2,
                'dob' => $input['dob'],
                'pan_number' => $input['pan_number'],
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Details submitted successfully',

            ]);
        } catch (\Throwable $th) {
            //throw $th;
            //die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

    public function leaderBoard(Request $request){
        $id = Auth::user()->id;

        $data = LeaderBoard::leftjoin('users', 'users.id', '=', 'leader_boards.user_id')
                            ->leftjoin('user_details', 'leader_boards.user_id', '=', 'user_details.user_id')->get();
        //$data = $data->sortBy('user_id');
        $user = $data->where('user_id', $id)->first();
        $data[] = $user;
        // Perform binary search to find the record with the specified user ID
       // $data['user']= $this->binarySearch($data, $id);
     

         return response()->json([
                'success' => true,
                'message' => 'success',
                'user' => $user,
                'data' => $data

            ]);
    }
    // public function binarySearch($collection, $target) {
    // $left = 0;
    // $right = $collection->count() - 1;
    
    //     while ($left <= $right) {
    //         $mid = floor(($left + $right) / 2);
    //         $current = $collection[$mid]->user_id;

    //         if ($current === $target) {
    //             return $collection[$mid];
    //         } elseif ($current < $target) {
    //             $left = $mid + 1;
    //         } else {
    //             $right = $mid - 1;
    //         }
    //     }

    //     return null; // Record not found
    // }
    public function userResult(Request $request){
        // $input = $request->only('contest_id');

        // $validate_data = [
        //     'contest_id'    => 'required',
            
        // ];
        // $validator = Validator::make($input, $validate_data);
        // if ($validator->fails()) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $validator->errors(),

        //     ]);
        // }
        try {
            $id = Auth::user()->id; 
            $result = UserContestWinloss::select('contests.contest_name', 'contests.start_date', 'contests.end_date','contests.pool_price', 'contests.joining_fee', 'user_contest_winlosses.*')->leftjoin('contests', 'contests.id', '=', 'user_contest_winlosses.contest_id')->where(['user_id' => $id])->get();
            $result = $result->map(function ($item) {
                $winamount = ContestWinner::where(['user_id' => $item->user_id, 'contest_id' => $item->contest_id])->first();
                if($winamount){
                    $item->win_amount = $winamount->win_amount;
                    $item->rank = $winamount->random_order;
                    return $item;
                
                }else{
                     $item->win_amount = 0;
                     $item->rank = 0;
                    return $item;
                }
            });
            if($result){
                return response()->json([
                'success'   => true,
                'message'   => 'success',
                'data'      => $result,

            ]);  
            }
        } catch (\Throwable $th) {
            throw $th;die;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
    }

     public function ContestWinner(Request $request){
        $input = $request->only('contest_id');

        $validate_data = [
            'contest_id'    => 'required',
            
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
        try {
            
            $result = ContestWinner::select('contest_winners.*', 'users.name', 'contests.contest_name', 'contest_winners.random_order as rank')->leftJoin('users','users.id', '=', 'contest_winners.user_id')->leftJoin('contests','contests.id', '=', 'contest_winners.contest_id')->where('contest_id', $input['contest_id'])->orderBy('random_order', 'ASC')->get();
            return response()->json([
                'success' => true,
                'message' => 'Success',
                'data' => $result

            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong',

            ]);
        }
        

    }
    public function userContestANS(Request $request){
        $input = $request->only('contest_id');

        $validate_data = [
            'contest_id'    => 'required',
            
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }

        try {
             $id = Auth::user()->id;
             $userans = UserContest::where(['user_id' => $id, 'contest_id' => $input['contest_id']])->first();
             $Contestans = ContestResult::where(['contest_id' => $input['contest_id']])->first();
            $ans = explode(',', $userans->answer);
            //print_r($ans);die;
            $ansarray = [];
            foreach($ans as $val){
                $share_name =  ContestData::where('id', $val)->first();
                
                $ansarray[]= $share_name->share_name;
            }
            $userAnswer['user_ans'] = $ansarray; 
            $Contestans = explode(',', $Contestans->result);

            $Contestansarray = [];
            foreach($Contestans as $val){
                $share_name =  ContestData::where('id', $val)->first();
               
                $Contestansarray[]= $share_name->share_name;
            }
            $contestAnswer['contest_ans'] = $Contestansarray;
            //print_r($ansarray);die;
            $answerArray = array_merge($userAnswer,$contestAnswer);
             $winners['winner_list'] = ContestWinner::select('users.name','contest_winners.*', 'contests.contest_name', 'contest_winners.random_order as rank')->leftJoin('users','users.id', '=', 'contest_winners.user_id')->leftJoin('contests','contests.id', '=', 'contest_winners.contest_id')->where('contest_id', $input['contest_id'])->orderBy('random_order', 'ASC')->get();
            $finalData = array_merge($answerArray,$winners);
             return response()->json([
                'success' => true,
                'message' => 'Success',
                'data' => $finalData

            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function matkaPaymentGateway(Request $request){
        $input = $request->only(['account', 'ifsc', 'amount', 'recipient_name', 'payment_mode', 'beneficiary_account_type']);
        $validate_data = [
            'account' => 'required|numeric',
            'ifsc' => 'required',
            'amount' => 'required|numeric',
            'recipient_name' => 'required',
            'payment_mode' => 'required|numeric',
            'beneficiary_account_type' => 'required|numeric',
        ];
        $validator = Validator::make($input, $validate_data);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),

            ]);
        }
       try {
        $secret_key_timestamp = $this->SecretKeyTimeStamp();
        $user_code = 20810200;
        $initiator_id = 9962981729;
    
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "25004",
            CURLOPT_URL => "https://staging.eko.in:25004/ekoapi/v1/agent/user_code:$user_code/settlement",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "initiator_id=$initiator_id&amount=".$input['amount']."&payment_mode=".$input['payment_mode']."&client_ref_id=Settlement7206123420&recipient_name=".$input['recipient_name']."&ifsc=".$input['ifsc']."&account=".$input['account']."&service_code=45&sender_name=Flipkart&source=NEWCONNECT&tag=Logistic&beneficiary_account_type=".$input['beneficiary_account_type'],
            CURLOPT_HTTPHEADER => array(
                "developer_key: becbbce45f79c6f5109f848acd540567",
                "secret-key: ".$secret_key_timestamp['secret_key'],
                "secret-key-timestamp: ".$secret_key_timestamp['secret_key_timestamp']
            ),
            CURLOPT_SSL_VERIFYPEER => false, // Disable SSL certificate verification
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);  
        if ($err) {
            return response()->json([
                'success' => false,
                'message' =>  $err
            ]);
        } else {
            return $response;
        }
        return response()->json([
            'success' => true,
            'message' =>  $err
        ]);

       } catch (\Throwable $th) {
        //throw $th;
        return response()->json([
            'success' => false,
            'message' => 'Something went wrong'
        ]);
       }
        
    }
   
    //Method for get Unique secret Key and Time-Stamp
    protected function SecretKeyTimeStamp(){
        // Initializing key in some variable. You will receive this key from Eko via email
            $key = "d2fe1d99-6298-4af2-8cc5-d97dcf46df30";

            // Encode it using base64
            $encodedKey = base64_encode($key);

            // Get current timestamp in milliseconds since UNIX epoch as STRING
            // Check out https://currentmillis.com to understand the timestamp format
            $secret_key_timestamp = (int)(round(microtime(true) * 1000));

            // Computes the signature by hashing the salt with the encoded key 
            $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

            // Encode it using base64
            $secret_key = base64_encode($signature);

            //return $secret_key and time stamp both;
            return $data = [
                'secret_key' => $secret_key,
                'secret_key_timestamp' => $secret_key_timestamp
            ];  
    }
}
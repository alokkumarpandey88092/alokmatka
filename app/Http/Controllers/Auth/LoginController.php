<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Create Login Method .
     */
    public function login(Request $request){   
        $input = $request->all();
        $this->validate($request, [
            //'email' => 'required|email',
            'mobile' => 'required|numeric|digits:10',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('mobile' => $input['mobile'], 'password' => $input['password'], 'status' => 1),true)){
           
             $user = Auth::user();
            if($user->hasRole(['Admin'])){
                return redirect()->route('home');
            }elseif($user->hasRole(['Partner'])){
                return redirect()->route('partner.dashboard');
            }elseif($user->hasRole(['Agent'])){
                return redirect()->route('agent.dashboard');
            }elseif($user->hasRole(['User'])){
                return redirect()->route('user-game.index');
            }else{
                Auth::logout();
                return redirect()->route('login')
                ->with('danger','You have not permission.');
            }
           
        }else{
            
            Auth::logout();
            return redirect()->route('login')
                ->with('danger','Invalid login credentials!');
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }  

    protected function sendOTP($mobile, $otp){
        // http://www.mysmsapp.in/api/push.json?apikey=638dede44a75c&sender=AMASHA&mobileno=$phone&text=Dear $otp your OTP for Login is Pls Keep it confidential. Regards- Team www.analystji.com
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://www.mysmsapp.in/api/push.json?apikey=638dede44a75c&sender=AMASHA&mobileno='.$mobile.'&text='.$otp.' is your OTP for Login. Pls keep it confidential. Regards- Team www.analystji.com',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_SSL_VERIFYPEER => 0
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
    }
    public function checkOtp(Request $request){
        return view('auth.login-old');
    }
    public function verifyOtp(Request $request){
        $otp = $request->otp;



        $email = $request->email;
        $password = $request->password;
        $id = $request->id;
        $check = DB::table('admin_otp')->where(['user_id' => $id ,'otp' => $otp])->first();
        if($check){
            $id = $check->id;
            
            if(auth()->attempt(array('email' => $email, 'password' => $password),true)){
                $user = Auth::user();
            if($user->hasRole(['Admin'])){
                return redirect()->route('home');
            }elseif($user->hasRole(['Analyst'])){
                return redirect()->route('profiles.index');
            }else{
                Auth::logout();
                return redirect()->route('login')
                ->with('danger','You have not permission.');
            }
            }
            
        }else{
            return redirect('/login');
        }
    }
    
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\{User, UserDetail, BankAccount};
use DB;
use Auth;
use Exception;
use Response;
use Illuminate\Support\Str;
use Carbon\Carbon;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BankAccount::where('user_id',auth()->user()->id)->get();
        return view('admin.bank-account.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bank-account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_holder_name' => 'required|string|max:255',
            'account_number' => 'required|string|max:20|unique:bank_accounts',
            'ifsc_code' => 'required|string|max:11',
            // Add other validation rules as needed
        ]);
        DB::beginTransaction();
        try {
            $data = [
                'user_id' => auth()->user()->id,
                'account_holder_name' => $request->account_holder_name,
                'account_number' => $request->account_number,
                'ifsc_code' => $request->ifsc_code
            ];
            $query = BankAccount::create($data);
             DB::commit();
             return redirect()->route('bank-accounts.index')->with('success','Bank Added successfully !');
        } catch (\Throwable $th) {
             DB::rollback();
            //echo $th ;die;
            return back()->with('danger','Something went wrong');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = BankAccount::find($id);
        //dd($data);
        return view('admin.bank-account.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
{
    $request->validate([
        'account_holder_name' => 'required|string|max:255',
        'account_number' => 'required|string|max:20|unique:bank_accounts,account_number,' . $id,
        'ifsc_code' => 'required|string|max:11',
        // Add other validation rules as needed
    ]);

    DB::beginTransaction();
    try {
        $data = [
            'account_holder_name' => $request->account_holder_name,
            'account_number' => $request->account_number,
            'ifsc_code' => $request->ifsc_code
        ];

        $query = BankAccount::where('id', $id)->update($data);
        DB::commit();

        return redirect()->route('bank-accounts.index')->with('success', 'Bank Updated successfully!');
    } catch (\Throwable $th) {
        DB::rollback();
        return back()->with('danger', 'Something went wrong');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            BankAccount::find($id)->delete();
            return back()->with('danger','Bank Details deleted successfully');
        } catch (\Throwable $th) {
            return back()->with('danger','Something went wrong !');
        }
    }
}

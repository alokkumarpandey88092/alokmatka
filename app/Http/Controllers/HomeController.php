<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\AnalystAccount;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use File;
use Auth;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $user = User::role('User')->count();
        $agent = User::role('Agent')->count();
        $partner = User::role('Partner')->count();
        $today_users = User::role('User')->with('userdetails')->where('users.created_at',Carbon::today())->count();
        $today_agent = User::role('Agent')->with('userdetails')->where('users.created_at',Carbon::today())->count();
        $data = array(
            'user' => $user,
            'agent' => $agent,
            'partner' => $partner,
            'today_users' => $today_users,
            'today_agent' => $today_agent
        );
        //dd($data);
        return view('home',compact('data'));
    }

    public function testing(Request $request){
        die;
        $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://kpi.knowlarity.com/Basic/v1/account/calllog?call_logs=true&limit=100&offset=1&start_time=2022-11-03%2000:00:01&end_time=2023-12-22%2023:59:59',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'x-api-key: p7nCSznkUy3hsCr7cXlzu4t7Kz1zNZcR3QwUy2aI',
                'Authorization: 0d419bbd-2b71-434a-a601-422cb87d12f2',
                'Content-Type: application/json',
                'cache-control: no-cache',
                'channel: Basic',
                'customer_number:+919389752279'
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $array  = json_decode($response);
            echo"<pre>";print_r($array);
            // foreach($array as $key => $value){

            //     if($key == 'objects'){
            //         foreach($value as $key => $v){
            //             foreach($v as $key=> $vd){
            //                 echo"<pre>";print_r($key);
            //             }
            //      }
            //     }
            // }
    }

    

  
  
}

<?php
    
namespace App\Http\Controllers;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{User, UserDetail};
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;
use ThiagoAlessio\TesseractOCR\TesseractOCR;
use Symfony\Component\Process\Exception\ProcessFailedException;
    
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->get();
        return view('users.index',compact('data'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::select('name')->get();
        //$roles = Role::select('name')->where('name','User')->get();
        return view('users.create',compact('roles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required',
            //'email' => 'required|email|unique:users,email',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        if($user){
            //insert data in userdetail table
            UserDetail::create([
                'user_id' => $user->id
            ]);
         }
        $user->assignRole($request->input('roles'));
    
        return back()->with('success','User created successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::select('name')->get();
        //$roles = Role::select('name')->where('name','User')->get();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('users.edit',compact('user','roles','userRole'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            //'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,'.$id,
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('danger','User deleted successfully');
    }


    public function register(Request $request){
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required',
            //'email' => 'email|unique:users,email',
            'mobile' => 'required|numeric|digits:10|unique:users,mobile',
            'password' => 'required|min:8|confirmed',
        ]);
        
        try {
            $data = [
                'name' => $input['name'],
                //'email' => $input['email'],
                'mobile' => $input['mobile'],
                'password' => Hash::make($input['password'])
            ];
            $user = User::create($data);
             if($user){
                //insert data in userdetail table
                UserDetail::create([
                    'user_id' => $user->id
                ]);
             }
            $user->assignRole('User');
            return redirect()->route('login')
                        ->with('success','You have registered successfully ! pls login');
            //code...
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('danger','Something went wrong !');
        }
    }


    public function decodeBase64(Request $request)
    {
        // Set the TesseractOCR path if needed
        // TesseractOCR::setDefaultConfig(['tesseract_command' => '/path/to/tesseract']);

        // Replace with your actual input name
        $captchaBase64String = $request->input('base64_string');

        // Decode the Base64 string to binary data
        $imageData = base64_decode($captchaBase64String);

        // Save the binary data as an image file
        $imagePath = storage_path('app/captcha_image.png');

        // Remove existing image file if it exists
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        // Save the new image file
        file_put_contents($imagePath, $imageData);

        // Verify that the image file was saved successfully
        if (!file_exists($imagePath)) {
            // Handle the error, log, or return a response
            return response()->json(['error' => 'Failed to save the image file.']);
        }

        try {
            // Use Tesseract OCR to recognize the text
            $text = (new TesseractOCR($imagePath))->run();

            // Suppress errors and forcefully convert to UTF-8
            $text = @iconv('UTF-8', 'UTF-8//IGNORE', $text);

            // Output the result
            return response()->json(['text' => $text]);
        } catch (ProcessFailedException $e) {
            // Log or handle the error
            return response()->json(['error' => 'Error during Tesseract OCR process.']);
        } catch (\ThiagoAlessio\TesseractOCR\UnsuccessfulCommandException $e) {
            // Log or handle the error
            return response()->json(['error' => $e->getMessage()]);
        }
    }
}
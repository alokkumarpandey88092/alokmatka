<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'user_details';
    protected $fillable = [
        'id',
        'user_id',
        'channal_name',
        'profile',
        'address',
        'location',
        'sebi',
        'bio',
        'description',
        "voice_call_charges",
        "video_call_charges",
        "chat_call_charges",
        "voice_call_enable",
        "video_call_enable",
        "chat_call_enable",
        "analyst_charges_id",
        "analyst_charges",
        "approved",
        "created_at",
        "updated_at"

    ];

    protected $hidden = [
        'id',
        'user_id',
        "voice_call_charges",
        "video_call_charges",
        "chat_call_charges",
        "analyst_charges_id",
        "created_at",
        "updated_at"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

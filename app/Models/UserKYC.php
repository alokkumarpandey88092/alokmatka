<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserKYC extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $table = 'user_k_y_c_s';
}

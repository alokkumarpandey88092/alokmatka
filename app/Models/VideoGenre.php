<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class VideoGenre extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    public function videoList(){
        return $this->belongsTo(Videolist::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Videolist extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];
    public function videoSubcategory(){
        return $this->hasMany(VideoSubcategory::class, 'video_id', 'id' );
    }
    public function videoGenre(){
        return $this->hasMany(VideoGenre::class, 'video_id', 'id');
    }

}

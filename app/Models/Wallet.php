<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id',
        'user_id',
        'wallet_amount',
        'type',
        'pay_status',
        'paid_for',
    ];
}

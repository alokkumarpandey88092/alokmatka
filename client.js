
global.EventSource = require('eventsource');
var mysql = require('mysql');
const moment = require('moment');
require('moment-timezone');
moment.tz.setDefault('Asia/Kolkata');
const { customAlphabet } = require('nanoid');
const generateUniqueId = customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 15);
var uniqueId = generateUniqueId();



var con = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "he5df7dff6fdf7d67hj7k5jh",
    database: "analystji"
  });


var URL = require('url').Url;
URL = "https://konnectprodstream4.knowlarity.com:8200/update-stream/0d419bbd-2b71-434a-a601-422cb87d12f2/konnect?source=router"


source = new EventSource(URL);
const { v4: uuidv4 } = require('uuid');
  
  // getting a random RFC 4122 Version 4 UUID
  // by using randomUUID() method
 

source.onmessage = function (event) {
 
  var data = JSON.parse(event.data)
	console.log('Received an event .......');
	console.log(data);
  
  if(data.event_type == 'AGENT_CALL'){
    //var uuid = uuidv4();
    var uuid = data.uuid

    var analyst_no = data.agent_number
    var analyst_no = analyst_no.replace(/\D/g, '').slice(-10);
    var query = `INSERT INTO call_logs (uuid, analyst_no) VALUES ('${uuid}',  ${analyst_no})`;
    var query2 = `UPDATE users u INNER JOIN  user_details ud on ud.user_id = u.id SET ud.is_busy = 1 WHERE u.mobile = '${analyst_no}'`;
    if(query != ''){
      con.query(query, (err, results) => {
        if (err) {
          console.error('Error1 saving event data:', err);
          return;
        }
        console.log('Event data saved to database');
      });
      con.query(query2, (err, results) => {
        if (err) {
          console.error('update busy:', err);
          return;
        }
        console.log('update busy successfully');
      });
    }
    
  }
  
  if(data.event_type == 'CUSTOMER_ANSWER'){
    var uuid = data.uuid
    var analyst_no = data.agent_number
    var analyst_no = analyst_no.replace(/\D/g, '').slice(-10);
    var call_start = data.Event_Date_Local
    var query = `UPDATE call_logs SET call_start = '${call_start}' WHERE uuid = '${uuid}'`;
    
    if(query != ''){
      con.query(query, (err, results) => {
        if (err) {
          console.error('Error1 saving event data:', err);
          return;
        }
        console.log('Event data saved to database');
      });

     
    }
    
  }
  if(data.event_type == 'BRIDGE'){
    var bridge = 1
    var uuid = data.uuid
    var query = `UPDATE call_logs SET bridge = ${bridge} WHERE uuid = '${uuid}'`;
    if(query != ''){
      con.query(query, (err, results) => {
        if (err) {
          console.error('Error1 saving event data:', err);
          return;
        }
        console.log('Event data saved to database');
      });
    }
    //console.log(call_Start);
  }
  if(data.event_type == 'HANGUP'){
    var call_end = data.Event_Date_Local
    var business_call_type = data.business_call_type

    var uuid = data.uuid
    var query = `UPDATE call_logs SET call_end = '${call_end}',  business_call_type = '${business_call_type}' WHERE uuid = '${uuid}'`;
    
    if(query != ''){
      con.query(query, (err, results) => {
        if (err) {
          console.error('Error1 saving event data:', err);
          return;
        }
        console.log('Event data saved to database');
      });
     
    }
   }
  // if(data.caller_id){
  //   var user_number = data.caller_id
  //   var uuid = data.uuid
  //   var call_recording = data.resource_url
  //   var total_call_duration = data.call_duration
  //   var destination = data.destination
   
  //   //console.log(user_number);
  // }
  
    if(data.dispnumber){
      var disp_business_call_type = data.business_call_type
      var total_call_duration = data.call_duration
      var call_recording = data.resource_url
      var destination = data.caller_id
      var destination = destination.replace(/\D/g, '').slice(-10);
      var agent_no = data.destination
      var agent_no = agent_no.replace(/\D/g, '').slice(-10);
      console.log(destination)
      var uuid = data.uuid
      var queryup = `UPDATE call_logs SET user_no = ${destination}, total_call_duration = ${total_call_duration}, disp_business_call_type = '${disp_business_call_type}', call_recording = '${call_recording}' WHERE uuid = '${uuid}'`;
      var query2 = `SELECT * FROM call_logs WHERE uuid = '${uuid}'`;
      var busyquery = `UPDATE users u INNER JOIN  user_details ud on ud.user_id = u.id SET ud.is_busy = 0 WHERE u.mobile = '${agent_no}'`;
      if(queryup != ''){
        let output;
        con.query(queryup, (err, results) => {
          if (err) {
            console.error('Error2 saving event data:', err);
            return;
          }

        });
      }
      if(busyquery != ''){
        let output;
        con.query(busyquery, (err, results) => {
          if (err) {
            console.error('Error2 saving event data:', err);
            return;
          }
          console.log('busy remove')
        });
      }

      if(query2 != ''){
        let output;
        con.query(query2, (err, results) => {
          if (err) {
            console.error('Error2 saving event data:', err);
            return;
          }
          resultData(results)
         
          //console.log(results);
        });
      }
    }
   
    
    
  
  
}

function resultData(result){
  const currentDateTimeIndia = moment().format('YYYY-MM-DD HH:mm:ss');
  //console.log(result)

  if(result[0].call_start != null){
    var t1 = new Date(result[0].call_start);
    var t2 = new Date(result[0].call_end);
    var call_duration = Math.abs(t1.getTime() - t2.getTime()) / 1000;
  }else{
    var call_duration = 0
  }
  var call_start = result[0].call_start
  var call_end = result[0].call_end
  var call_recording = result[0].call_recording
  const query2 = `select users.id, users.userid, ud.voice_call_charges, ud.analyst_charges from users left join user_details as ud on ud.user_id = users.id where users.mobile = '${result[0].analyst_no}'`
  con.query(query2, (err, res) => {
        if (err) {
          console.error('Error3 saving event data:', err);
          return;
        }

        var analyst_id = res[0].id
        var analyst_charges = res[0].voice_call_charges
        var analyst_percent = res[0].analyst_charges
        var admin_percentage = 100 - analyst_percent
        if(call_duration > 30){
          var actual_call_duration = parseInt(call_duration) - 30
          var amount =  (parseInt(res[0].voice_call_charges)/60)*actual_call_duration
          var amount =  amount.toFixed(2)
          var analyst_percentage = parseInt(res[0].analyst_charges)
          var analyst_amount = (amount*analyst_percentage)/100
          var analyst_amount = analyst_amount.toFixed(2)
          var admin_amount = amount - analyst_amount
          var admin_amount = admin_amount.toFixed(2)


        }else{
          var amount = 0
          var analyst_amount = 0
          var admin_amount = 0
        }

        //console.log(result[0].user_no)
        const query3 = `SELECT users.id, w.total_amount FROM users left join user_wallets as w on w.user_id = users.id WHERE users.mobile = '${result[0].user_no}'`
        con.query(query3, (err, res1) => {
          
          if (err) {
            console.error('Error4 saving event data:', err);
            return;
          }
          var user_id = res1[0].id
          var user_wallet_amount = parseInt(res1[0].total_amount)
          var current_balance = user_wallet_amount - amount
          var current_balance = current_balance.toFixed(2)

            query5 = `UPDATE user_wallets SET total_amount = ${current_balance} WHERE user_id = ${user_id}`;
            con.query(query5, (err, balance)=>{
              if (err) {
                console.error('Error5 saving event data:', err);
                return;
              }
            })
            if(call_duration > 30){
              query6 = `INSERT INTO wallets (user_id, transaction_id, wallet_amount, type, created_at, updated_at) VALUES (${user_id}, '${uniqueId}', ${amount}, 'Debit', '${currentDateTimeIndia}', '${currentDateTimeIndia}')`;
              con.query(query6, (err, balance)=>{
                if (err) {
                  console.error('Error6 saving event data:', err);
                  return;
                }
              })
            }

            query4 = `INSERT INTO analyst_accounts (analyst_id, user_id, call_type, call_start, call_end, call_duration,analyst_call_price,amount, analyst_percentage, admin_percentage, analyst_amount, admin_amount, call_recording, reconcilation,created_at, updated_at) VALUES(${analyst_id}, ${user_id}, 1, '${call_start}', '${call_end}', '${call_duration}', '${analyst_charges}','${amount}', '${analyst_percent}', '${admin_percentage}', '${analyst_amount}', '${admin_amount}', '${call_recording}', 'pending', '${currentDateTimeIndia}', '${currentDateTimeIndia}' )`
            con.query( query4, (err, resfinal) => {
              if (err) {
                console.error('Error6 saving event data:', err);
                return;
              }
            })
        })
        
      });
}

function saveEventDataToDatabase(data) {
  
  //console.log(data)

  // const query = "INSERT INTO call_logs (uuid, analyst_no, user_no, business_call_type, call_start, call_end, call_duration, total_call_duration, bridge, disp_business_call_type, call_recording) VALUES (?)";
  // console.log(query);
  // con.query(query, [data], (err, results) => {
  //   if (err) {
  //     console.error('Error saving event data:', err);
  //     return;
  //   }
  //   console.log('Event data saved to database');
  // });
}




// const https = require('https');
// var mysql = require('mysql');
// const zlib = require('zlib');

// var con = mysql.createConnection({
//   host: "127.0.0.1",
//   user: "root",
//   password: "he5df7dff6fdf7d67hj7k5jh",
//   database: "analystji"
// });




// // Define the HTTPS server options
// const options = {
//   hostname: 'konnectprodstream4.knowlarity.com',  // Replace with the server hostname
//   port: 8200,                // Replace with the server port
//   path: 'https://konnectprodstream4.knowlarity.com:8200/update-stream/0d419bbd-2b71-434a-a601-422cb87d12f2/konnect?source=router',                // Replace with the desired path
//   method: 'GET',            // Replace with the desired HTTP method
// };


// // Make the HTTPS request
// const req = https.request(options, (res) => {
//   // Handle the response
//   console.log(`Status code: ${res.statusCode}`);
//   let responseData = '';

//     res.on('data', (data) => {
//       //responseData += chunk.toString('utf8');
//       console.log( typeof `${data}`);  
      
//     });
    
//     res.on('end', () => {
//       const encoding = res.headers['content-encoding'];
//       if (encoding === 'gzip') {
//         zlib.gunzip(responseData, (error, decompressedData) => {
//           if (error) {
//             console.error('Error decompressing gzip data:', error);
//             return;
//           }
//           const parsedData = JSON.parse(decompressedData.toString('utf8'));
//           // Use the parsed data here
//         });
//       } else {
//         const parsedData = JSON.parse(responseData);
//         // Use the parsed data here
//       }
//     });
    
//   });

  
     
    
// // Handle errors
// req.on('error', (error) => {
//   console.error(`Request error: ${error}`);
// });


// // Send the request
// req.end( console.log('hello'));
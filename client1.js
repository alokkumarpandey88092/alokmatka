//const WebSocket = require('websocket').client;
const WebSocket = require('ws');
const deviceIP = '122.176.118.118';
const devicePort = 9156;
encodedToken = 'YWRtaW46MTIzNA==';
// WebSocket connection options
const connectionOptions = {
  // Adjust the URL according to the third-party API documentation
  url: 'wss://kpi.knowlarity.com/Basic/v1/account/calllog?call_logs=true&limit=1&offset=1&start_time=2022-11-03%2000:00:01&end_time=2023-12-22%2023:59:59',
  headers: {
    "Authorization": `0d419bbd-2b71-434a-a601-422cb87d12f2`,
    "x-api-key" : `p7nCSznkUy3hsCr7cXlzu4t7Kz1zNZcR3QwUy2aI`,
  }
};

// Create a WebSocket client instance

const client = new WebSocket(connectionOptions.url, { headers: connectionOptions.headers });
client.on('error', console.error);
// Handle connection event
client.on('connect', (connection) => {
  console.log('WebSocket connected.');

  // Handle received data
  connection.on('message', (message) => {
    console.log('Received data:', message.utf8Data);
  });

  // Handle connection close
  connection.on('close', () => {
    console.log('WebSocket connection closed.');
  });
});

// Connect to the WebSocket API
//client.connect(connectionOptions);
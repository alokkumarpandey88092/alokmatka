<?php
    /*
    |--------------------------------------------------------------------------
    | Copyright © 2021 Ramagya
    |--------------------------------------------------------------------------
    |
    | This file is part of Ramagya Copyright.
    | 
    | Strictly restricted to unauthorize using this file code for personal or commercial or distribution. 
    |
    |
    */
    define('DATE_FORMAT',               'd M Y, h:i A');
    define('SIMPLE_DATE',               'd M Y');
    define('SORT_DATE',               'M Y');
    define('MONTH_NAME',               'M');
    define('SIMPLE_DATETIME',           'd M Y, h:i:s A');
    define('SIMPLE_DATETIME1',          'd M Y, h:i');
    define('DB_DATE_FORMAT',            'Y-m-d');
    define('DB_DATETIME_FORMAT',        'Y-m-d h:i:s');
    define('DB_DATETIME_OTHER_FORMAT',  'd M Y, H:i:s');
    define('DB_SOWDATETIME_FORMAT',     'Y-m-d');
    
    define('CALENDAR_DATE_FORMAT',     'm/d/Y');
    define('SERVICE_DATE_FORMAT',     'd/m/Y');
    define('SIMPLE_DATE_DEMO',          'd M, Y');
    define('SIMPLE_TIME',          'H:i:s');
    define('API_NEXT_FOLLOWUP',          'd/m');
    define('ATTENDANCE_TIME',          'H:i');
    define('TIME_FORMAT_AM_PM', 'h:i A');
    

   // define('FIELDS_REQUIRED_MESSAGE',   'All fields marked with an asterisk are mandatory'); 
      
   define('NO_RECORD_FOUND',        'No Record(s) Found');
   define('DEFAULT_RECORD_PER_PAGE', 5);
   define('DEFAULT_PAGE_RANGE', [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100]);
   define('SEARCH_ICON', '<i class="fas fa-search action-color" title="Search"></i>');
   define('RESET_ICON', '<i title="Reset" class="fas fa-sync-alt action-color"></i>');


   define('MAIL_SENDER', env('MAIL_USERNAME'));
   //define('MANAGMENT_EMAIL', env('bhupesh.kathuria@ramagyagroup.com'));
   define('MANAGMENT_EMAIL', 'phpdeveloper@ramagyagroup.com');

   define('API_ACCESS_KEY', 'AAAANjvWGoc:APA91bFKQZZTcmW5OQGQ8WQy3DynWBfEi0LAD_Dlu3yO1DQSIBdA9YL5_9Ac8FyuYX0YYGfsOqrLkiaP-OqlwNodXCF4ce3vOWAM30FOil7e0HcL9YUKUo2z6IXuFGI237VOI_7Vq3pJ');

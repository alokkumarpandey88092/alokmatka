<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->uuid('channal_name')->unique();
            $table->string('profile')->nullable();
            $table->string('experience')->nullable();
            $table->string('consultant')->nullable();
            $table->string('language')->nullable();
            $table->string('location')->nullable();
            $table->text('address')->nullable();
            $table->string('sebi')->unique()->nullable();
            $table->text('bio')->nullable();
            $table->text('description')->nullable();
            $table->string('voice_call_charges')->nullable();
            $table->string('video_call_charges')->nullable();
            $table->string('chat_call_charges')->nullable();
            $table->string('voice_call_enable')->nullable()->comment('0=disbale,1=enable');
            $table->string('video_call_enable')->nullable()->comment('0=disbale,1=enable');
            $table->string('chat_call_enable')->nullable()->comment('0=disbale,1=enable');
            $table->integer('is_busy')->default(0)->comment('0=not,1=busy');
            $table->string('analyst_charges_id')->nullable()->comment('1=percentage,2=fixed');
            $table->string('analyst_charges')->nullable();
            $table->string('approved')->nullable()->default(0)->comment('0=not,1=approved');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}

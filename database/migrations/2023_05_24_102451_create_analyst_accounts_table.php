<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalystAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyst_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('analyst_id');
            $table->unsignedBigInteger('user_id');
            $table->string('call_type');
            $table->string('call_start');
            $table->string('call_end');
            $table->string('call_duration');
            $table->string('analyst_call_price');
            $table->string('amount');
            $table->string('analyst_percentage');
            $table->string('admin_percentage');
            $table->string('analyst_amount');
            $table->string('admin_amount');
            $table->string('reconcilation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analyst_accounts');
    }
}

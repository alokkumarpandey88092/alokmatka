<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();
            $table->uuid('kuuid');
            $table->string('analyst_no')->nullable();
            $table->string('user_no')->nullable();
            $table->string('business_call_type')->nullable();
            $table->string('call_start')->nullable();
            $table->string('call_end')->nullable();
            $table->string('call_duration')->nullable();
            $table->string('total_call_duration')->nullable();
            $table->string('bridge')->nullable();
            $table->string('disp_business_call_type')->nullable();
            $table->string('call_recording', 2048)->nullable();
           // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_logs');
    }
}

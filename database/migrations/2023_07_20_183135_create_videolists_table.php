<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideolistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videolists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category')->nullable();
            $table->string('title')->nullable();
            $table->string('link', 1000)->nullable();
            $table->string('video_thumb', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videolists');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_genres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('video_id'); 
            $table->unsignedInteger('genre_id'); 
            $table->string('genre_name');
            $table->foreign('video_id')->references('id')->on('videolists')->onDelete('cascade')->onUpdate('cascade'); 
            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade')->onUpdate('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_genres');
    }
}

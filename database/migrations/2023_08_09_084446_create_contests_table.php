<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contest_name');
            $table->string('member_logo')->nullable();
            $table->string('contest_time')->nullable();
            $table->string('contest_image')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('start_end_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('jantari_winning_amount')->nullable()->comment('Percentage of Jantari Winning Amount');
            $table->string('bahar_winning_amount')->nullable()->comment('Percentage of Bahar Winning Amount');
            $table->string('ander_winning_amount')->nullable()->comment('Percentage of Ander Winning Amount');
            $table->string('company_commission')->nullable()->comment('Percentage of Company Commission');
            $table->string('result_time')->nullable()->comment('Game Result Upload Time');
            $table->string('pool_price')->nullable();
            $table->string('joining_fee')->nullable();
            $table->string('seats')->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests');
    }
}

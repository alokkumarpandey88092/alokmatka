<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserKYCSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_k_y_c_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('pan_image')->nullbale();
            $table->string('pan_number')->nullbale();
            $table->date('dob')->nullbale();
            $table->string('Mobile')->nullbale();
            $table->string('Email')->nullbale();
            $table->text('address')->nullbale();
            $table->string('bank_name')->nullbale();
            $table->string('bank_account')->nullbale();
            $table->string('bank_ifsc')->nullbale();
            $table->string('status')->default(0)->comment('0=not', '1=done', '2=pending');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_k_y_c_s');
    }
}

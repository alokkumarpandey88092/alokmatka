<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('partner_id');
            $table->string('winning_number')->nullable();
            $table->string('total_play_amount')->nullable();
            $table->string('winning_amount')->nullable();
            $table->string('admin_amount')->nullable();
            $table->string('closing_amount')->nullable();
            $table->string('admin_percentage')->nullable();
            $table->string('jantari_winning')->nullable();
            $table->string('andar_winning')->nullable();
            $table->string('bahar_winning')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('partner_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_accounts');
    }
}

<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://kpi.knowlarity.com/Basic/v1/account/calllog?call_logs=true&limit=100&offset=1&start_time=2022-11-03%2000:00:01&end_time=2023-12-22%2023:59:59',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'x-api-key: p7nCSznkUy3hsCr7cXlzu4t7Kz1zNZcR3QwUy2aI',
    'Authorization: 0d419bbd-2b71-434a-a601-422cb87d12f2',
    'Content-Type: application/json',
    'cache-control: no-cache',
    'channel: Basic',
    'customer_number:+919389752279'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
?>
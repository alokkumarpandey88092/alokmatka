@extends('layouts.app_new')
@section('title','Analyst Ji || Analyst Management')
@section('header_title','Analyst Account')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Analyst Account</h3>
                @can('role-create')
                
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_usermanagement" class="table table-bordered table-striped table-responsive">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Analyst Name</th>
                    <th>Analyst ID</th>
                    <th>User ID</th>
                    <th>Call Type</th>
                    <th>Call Start Time</th>
                    <th>Call End Time</th>
                    <th>Call Duration</th>
                    <th>Analyst/Min Call Price</th>
                    <th>Free Tier(in sec)</th>
                   
                    <th>Amount(&#8377;)</th>
                    <th>Analyst %</th>
                    <th>Platform %</th>
                    <th>Analyst Amount</th>
                    <th>Platform Amount</th>
                    <th>Reconcilation Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($list as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->call_date ? date(SIMPLE_DATE, strtotime($v->call_date)) : date(SIMPLE_DATE, strtotime($v->created_at))}}</td>
                    <td>{{$v->name ? $v->name : 'Not available'}}</td>
                    <td>{{$v->analyst_id ? $v->analyst_id : 'Not available'}}</td>
                    <td>{{$v->uid ? $v->uid : ''}}</td>
                    <td>@if($v->call_type == 1) Voice  @elseif($v->call_type == 2) Video @else Chat @endif</td>
                    <td>
                      @if($v->call_start and $v->call_start != 'null')
                      {{date(DB_DATETIME_OTHER_FORMAT,strtotime($v->call_start))}}
                      @elseif($v->call_start and $v->call_start == 'null')
                      Not Answerd
                      @endif
                    </td>
                    <td>{{$v->call_end ? date(DB_DATETIME_OTHER_FORMAT,strtotime($v->call_end)) : 'Not available'}}</td>
                    <td>{{$v->call_duration }}</td>
                    <td>{{$v->analyst_call_price   ? $v->analyst_call_price   : 'Not available'}}</td>
                    <td>30</td>
                    
                    <td>{{$v->amount}}</td>
                    <td>{{$v->analyst_percentage   ? $v->analyst_percentage   : 'Not available'}}</td>
                    <td>{{$v->admin_percentage   ? $v->admin_percentage   : 'Not available'}}</td>
                    <td>{{$v->analyst_amount   ? $v->analyst_amount   : 'Not available'}}</td>
                    <td>{{$v->admin_amount   ? $v->admin_amount   : 'Not available'}}</td>
                    <td>{{$v->reconcilation   ? $v->reconcilation   : 'Not available'}}</td>
               
                    
                  </tr>
                  @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_usermanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

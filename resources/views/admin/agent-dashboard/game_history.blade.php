<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Game History</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
	<!-- Datatables CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/datatables/datatables.min.css') }}">
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Game History</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->
 <!--Flash Message -->
 @include('layouts.user_game.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">

            <div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-hover table-center mb-0 datatable">
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>Game Name</th>
												<th>Type</th>
												<th>Choise</th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($game_history as $k => $v)
											<tr>
												<td>{{++$k}}</td>
												<td>{{ $v->created_at ? date(SIMPLE_DATE,strtotime($v->created_at)) : ''}}</td>
												<td>{{ $v->game_name ? $v->game_name : ''}}</td>
												<td>{{ $v->game_type ? $v->game_type : ''}}</td>
												<td>
                                                <a href="javascript:void(0)" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#staticBackdrop4" data-choise="{{ $v->user_choise ? $v->user_choise : ''}}" onclick="ViewGameChoise(this);">view</a></td>
											</tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

            </div>

          </div>


        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel4" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel4">Choise</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 ">
              <div class="col-lg-6 repeattt">
                <table style="width:100% ">
                </table>
                <div class="row repeat-secc">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="price">
                          <i class="fas fa-rupee-sign"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="today-btn">
                          <a href="javascript:void(0)"> Total Aount </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script>
    function ViewGameChoise(id){
        var data = $(id).attr('data-choise');
          var userChoices = JSON.parse(data);
          console.log(userChoices);
          //return false;
              // Initialize a variable to store the total amount
              var totalAmount = 0;
              var parentElement = $('#staticBackdrop4');
              $('table', parentElement).empty();
              // Iterate through the userChoices array using $.each
              $.each(userChoices, function(index, choice) {
                  // Append rows to the first table (index 0)
                  $('table', parentElement).eq(0).append('<tr><td>' + choice.game_number + '</td><td> = </td><td>' + choice.game_number_amount + '</td></tr>');
                  
                  // Append rows to the second table (index 1)
                  $('table', parentElement).eq(1).append('<tr><td>' + choice.game_number + '</td><td> = </td><td>' + choice.game_number_amount + '</td></tr>');
                  // Update the total amount
                    totalAmount += parseFloat(choice.game_number_amount);
              });
              // Update the content of the price div with the total amount
              $('.price i', parentElement).text('   ' + totalAmount.toFixed(2));
    }
  </script>
  
<!-- Datatables JS -->
<script src="{{ asset('game/assets/plugins/datatables/datatables.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
</body>

</html>
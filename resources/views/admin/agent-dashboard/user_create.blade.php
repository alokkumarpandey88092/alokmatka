<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Create User</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Create User</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.agent.sidebar')
    <!-- /Sidebar -->

     <!--Flash Message -->
     @include('layouts.agent.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
      <div class="row">
					<div class="col-xl-8 offset-xl-2">
					
						<!-- Page Header -->
						<div class="page-header">
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-title">Create User</h3>
								</div>
							</div>
						</div>
						<!-- /Page Header -->
						
						<div class="card">
							<div class="card-body">
							
								<!-- Form -->
								<form action="{{ route('agent.UserCreateStore') }}" method="post">
                  @csrf
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>User Name</label>
                        <input class="form-control" name="name" type="text" Placeholder="User Name" value="{{old('name')}}" required>
                        @error('name')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div>

                  <!-- <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>User Email</label>
                        <input class="form-control" name="email" type="email" Placeholder="User Email" value="{{old('email')}}" required>
                        @error('email')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div> -->

                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>User Mobile</label>
                        <input class="form-control mobile" name="mobile" type="text" Placeholder="User Mobile" value="{{old('mobile')}}" required>
                        @error('mobile')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div>

									<div class="mt-4">
										<button class="btn btn-primary" type="submit">Submit</button>
										<button class="btn btn-primary" type="reset">Reset</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Jodi Play</h2>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Crossing</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>From-to</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>ander-bahar haruf</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script>
  //alert()
      //TIMEPICKER
      $('#start_time, #end_time').timepicker({
        'timeFormat': 'H:i', // Use 'H' for 24-hour format without AM/PM
        'step': 30,
        'scrollDefault': 'now'
      });

// Event handler for start time change
$('#start_time').on('changeTime', function() {
    var startTime = $('#start_time').val();
    var endTime = $('#end_time').val();

    // Check if end time is the same or less than start time
    if (endTime <= startTime) {
      // Set end time to a minute after start time
      var newEndTime = new Date($('#start_time').timepicker('getTime'));
      newEndTime.setMinutes(newEndTime.getMinutes() + 1);
      $('#end_time').timepicker('setTime', newEndTime);
    }
  });

  // Event handler for end time change
  $('#end_time').on('changeTime', function() {
    var startTime = $('#start_time').val();
    var endTime = $('#end_time').val();

    // Check if end time is the same or less than start time
    if (endTime <= startTime) {
      // Set start time to a minute before end time
      var newStartTime = new Date($('#end_time').timepicker('getTime'));
      newStartTime.setMinutes(newStartTime.getMinutes() - 1);
      $('#start_time').timepicker('setTime', newStartTime);
    }
  });


  $("#start_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', minDate);
});

$("#end_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', maxDate);
});


  $('#member_logo').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#memberLogoPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      $('#contest_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#contestImagePreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      //Handle Input table only accept numeric value
     $(document).on('keypress keyup input dragover drop','.mobile',function(e){
          return e.charCode >= 48 && e.charCode <= 57 && this.value.length<10;
      });
</script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>User History</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
	<!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>User History</h3>
      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.agent.sidebar')
    <!-- /Sidebar -->
 <!--Flash Message -->
 @include('layouts.agent.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">

            <div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-hover table-center mb-0" id="tbl_user_history">
										<thead>
											<tr>
												<th>#</th>
												<th>User Name</th>
												<th>Mobile</th>
												<th>Created Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
                      @foreach($user_history as $k => $v)
											<tr>
												<td>{{++$k}}</td>
												<td>{{ $v->name ? $v->name : ''}}</td>
                        <td>{{ $v->mobile ? $v->mobile : ''}}</td>
                      <td>{{ $v->created_at ? date(SIMPLE_DATE,strtotime($v->created_at)) : ''}}</td>
                      <td><a href="{{ route('agent.UserEditView',$v->id) }}" type="button" class="" style="display:inline" title="edit"><i class="fa fa-edit"></i></a>
                      <form action="{{ route('agent.UserDestroy',$v->id) }}" method="post" style="display:inline;" onsubmit=" return confirm('Are you sure ?')">
                        @csrf
                        <button type="submit" class="float-left"><i class="fa fa-trash" title="delete"></i></button></td>
                      </form>
											</tr>
                        @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

            </div>

          </div>


        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel4" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel4">Choise</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 ">
              <div class="col-lg-6 repeattt">
                <table style="width:100% ">
                </table>
                <div class="row repeat-secc">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="price">
                          <i class="fas fa-rupee-sign"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="today-btn">
                          <a href="javascript:void(0)"> Total Aount </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Datatables JS -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<script>
  $(document).ready(function() { 
    $('#tbl_user_history').DataTable();  
  });
</script>
</body>
</html>
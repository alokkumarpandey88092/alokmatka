@extends('layouts.app_new')
@section('title','Analyst Ji || User Answers')
@section('header_title','Answers')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Answers</h3>
                <!-- @can('role-create')
                <a href="{{ route('questions.create') }}" type="button" class="btn btn-info float-right">Create New Analyst</a>
                @endcan -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_usermanagement" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Answer</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $k => $v) 
                  
                  
                  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->name ? $v->name : ''}}</td>
                    <td>
                    <?php 
                  $answer = '';
                  if($v->is_skip == 0){
                    $answer_arr = explode(',', $v->answer);
                    $answer1 = explode('=', $answer_arr[0]);
                    $answer2 = explode('=', $answer_arr[1]);
                    $answer3 = explode('=', $answer_arr[2]);

                    echo "1. &nbsp;".$answer1[1]."<br>2. &nbsp;".$answer2[1]."<br>3.&nbsp; ".$answer3[1];
                  }else {
                    $answer ='No Response';
                    echo $answer;
                  }
                  ?>
                </td>
                <td>
                  <?php 
                 echo ($v->is_skip==0) ? "<button type='button' class='btn btn-success'>Answerd</button>" : "<button type='button' class='btn btn-warning'>Skipped</button>";
                  ?>
                
                  </tr>
                  @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_usermanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

@extends('layouts.app_new')
@section('title','Analyst Ji || Create Banners')
@section('header_title','Create Banners')
@push('css')

@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('banners.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_questions_save" method="post" action="{{ route('banners.store') }}" enctype="multipart/form-data">
                @csrf
                <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-6 col-sm-6">
                        <div class="form-group">
                        <label for="banner">Banner</label>
                        <input class="form-control" type="file" id="banner_image" name="banner_image" placeholder="Choose Banner">
                        </div>
                      </div>
                      <div class="col-6 col-sm-4 offset-md-2">
                          <div class="form-group">
                            <div class="input-group">
                            <img style="height:100px;width:200px" id="imgPreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar-male.png')}}';">
                            </div>
                          </div>
                      </div>
                      <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label for="question">Status</label>
                        <select class="select2" name="status" style="width: 100%;">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                          </select>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->

                    </div>
                    <!-- /.row -->
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page specific script -->
<script>
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();

    $('#banner_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#imgPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });

    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_questions_save').validate({
    rules: {
      question: {
        required: true,
      },
     
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

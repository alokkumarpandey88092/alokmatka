@extends('layouts.app_new')
@section('title','Analyst Ji || Analyst Banners')
@section('header_title','Banners')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Banners</h3>
                @can('product-create')
                <a href="{{ route('banners.create') }}" type="button" class="btn btn-info float-right">Create New Banners</a>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_usermanagement" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Banner</th>
                    <th>Status</th>
                    @can('product-edit')
                    <th>Action</th>
                    @endcan
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>
                    <img style="height:100px;width:200px" src="{{asset('banners_image')}}/{{$v->banner}}" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar-male.png')}}';"></td>
                    <td>
                      <?php 
                      echo ($v->status == 1) ? '<button type="button"  class="btn btn-success">Active</button>' : '<button type="button"  class="btn btn-danger">Inactive</button>';
                      ?>
                    </td>
                    @can('product-edit')
                    <td>
                      <a href="{{ route('banners.edit',$v->id) }}" type="button" class="btn btn-primary float-left" style="display:inline;@cannot('product-edit')pointer-events:none; @endcannot ">Edit</a>
                    
                      <!-- <a href="javascript:void(0)" type="button" class="btn btn-primary">Edit</a> -->
                  
                      <!-- <a href="{{ route('users.show',$v->id) }}" type="button" class="btn btn-info">Show</a> -->
                
                    <form action="{{ route('banners.destroy', $v->id) }}" method="POST" style="display:inline;">
                      @csrf
                      @method('DELETE')
                      <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger" @cannot('product-delete') disabled @endcannot>Delete</button>
                  </form>
                  <!-- <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger" style="display:inline">Delete</button> -->
                  
                </td>
                @endcan
                  </tr>
                  @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_usermanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

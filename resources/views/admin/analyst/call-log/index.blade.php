@extends('layouts.app_new')
@section('title','Analyst Ji || Call-Log')
@section('header_title','Call-Log')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Call-Log</h3>
                @can('role-create')
                
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_usermanagement" class="table table-bordered table-striped table-responsive">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Analyst Name</th>
                    <!-- <th>Analyst ID</th> -->
                    <th>User Name</th>
                    <th>Call Type</th>
                    <th>Call Start Time</th>
                    <th>Call End Time</th>
                    <th>Call Duration</th>
                    <th>Analyst/Min Call Price</th>
                    <th>Free Tier</th>
                    <th>Variance</th>
                    <th>Amount</th>
                    <th>Analyst %</th>
                    <th>Platform %</th>
                    <th>Analyst Amount</th>
                    <th>Platform Amount</th>
                    <th>Reconcilation Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($list as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->created_at ? date(SIMPLE_DATE, strtotime($v->created_at)) : ''}}</td>
                    <td>{{$v->analyst_name ? $v->analyst_name : ''}}</td>
                    <!-- <td>{{$v->analyst_id ? $v->analyst_id : ''}}</td> -->
                    <td>{{$v->user_name ? $v->user_name : ''}}</td>
                    <td>@if($v->call_type == 1) Voice  @elseif($v->call_type == 2) Video @else Chat @endif</td>
                    <td>
                      @if($v->call_start and $v->call_start != 'null')
                      {{date(DB_DATETIME_OTHER_FORMAT,strtotime($v->call_start))}}
                      @elseif($v->call_start and $v->call_start == 'null')
                      Not Answerd
                      @endif
                    </td>
                    <td>{{$v->call_end   ? date(DB_DATETIME_OTHER_FORMAT,strtotime($v->call_end))   : ''}}</td>
                    <td>{{$v->call_duration   ? $v->call_duration   : ''}}</td>
                    <td>{{$v->analyst_call_price   ? $v->analyst_call_price   : ''}}</td>
                    <td>30</td>
                    <td>20</td>
                    <td>{{$v->amount   ? $v->amount   : ''}}</td>
                    <td>{{$v->analyst_percentage   ? $v->analyst_percentage   : ''}}</td>
                    <td>{{$v->admin_percentage   ? $v->admin_percentage   : ''}}</td>
                    <td>{{$v->analyst_amount   ? $v->analyst_amount   : ''}}</td>
                    <td>{{$v->admin_amount   ? $v->admin_amount   : ''}}</td>
                    <td>{{$v->reconcilation   ? $v->reconcilation   : ''}}</td>
               
                    
                  </tr>
                  @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_usermanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

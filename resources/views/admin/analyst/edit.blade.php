@extends('layouts.app_new')
@section('title','Analyst Ji || Edit Analyst')
@section('header_title','Edit Analyst')
@push('css')

@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('analysts.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_analyst_save" method="post" action="{{ route('analysts.update',$user->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName1" required placeholder="Enter Name" value="{{ @$user->name ? @$user->name : old('name')}}">
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" required placeholder="Enter email" value="{{ @$user->email ? @$user->email : old('email')}}">
                            @error('email')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                            @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputMobile">Mobile</label>
                        <input type="text" name="mobile" class="form-control" id="exampleInputMobile" required placeholder="Enter Mobile" value="{{ @$user->mobile ? @$user->mobile : old('mobile')}}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 && this.value.length<10" maxlength="10">
                        @error('mobile')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Role</label> 
                          <select class="select2" multiple="multiple"  name="roles[]" data-placeholder="Select a role" style="width: 100%;">
                          @foreach($roles as $k => $v)
                            <option {{$v->name != 'Analyst' ? 'disabled' : ''}} {{$v->name == 'Analyst' ? 'selected' : ''}} value="{{$v->name ? $v->name : ''}}">{{$v->name ? $v->name : ''}}</option>
                          @endforeach
                          </select>
                          @error('roles')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                      </div> -->
                      

                      <!-- <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Role</label> 
                          <select class="select2" multiple="multiple"  name="roles[]" data-placeholder="Select a role" style="width: 100%;">
                          @foreach($roles as $k => $v)
                            <option {{$v->name != 'Analyst' ? 'disabled' : ''}} {{$v->name == 'Analyst' ? 'selected' : ''}} value="{{$v->name ? $v->name : ''}}">{{$v->name ? $v->name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                       
                      </div>
                     
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputPassword1">Confirm Password</label>
                          <input type="password" name="confirm-password" class="form-control" id="exampleInputConfirmPassword1" placeholder="Password">
                        </div>
                     
                      </div> -->
                   

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Description</label>
                        <textarea name="description" class="form-control" id="description" placeholder="Description">{{ @$user->userdetails['description'] ? @$user->userdetails['description'] : ''}}</textarea>
                    </div>
                        <!-- /.form-group -->
                    </div>

                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="experience">Bio</label>
                        <textarea name="bio" class="form-control" id="bio" placeholder="Bio" value="">{{ @$user->userdetails['bio'] ? @$user->userdetails['bio'] : ''}}</textarea>
                          
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                            <label for="formFile" class="form-label">Profile Picture</label>
                            <input class="form-control" type="file" id="profile" name="profile">
                        <!-- /.form-group -->
                      </div>
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                          <div class="input-group">
                          <img style="height:100px" src="{{asset('members_image')}}/{{@$user->userdetails['profile']}}" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar-male.png')}}';">
                          </div>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Experience</label>
                          <select class="select2" name="experience" data-placeholder="Select Experience" style="width: 100%;" required> 
                          <option value=""></option>
                          @for($i=1;$i<=50;$i++)
                          <option value="{{$i}}" {{(@$user->userdetails['experience'] == $i ) ? 'selected' : ''}}>{{$i}} Years</option>
                          @endfor
                          </select>
                        </div>
                    </div>
                        <!-- /.form-group -->
                    

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Consultant</label>
                          <input type="text" name="consultant" class="form-control" id="consultant" placeholder="Consultant" value="{{ @$user->userdetails['consultant'] ? @$user->userdetails['consultant'] : ''}}">
                    </div>
                        <!-- /.form-group -->
                      </div>

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Sebi Registration</label>
                          <input type="text" name="sebi" class="form-control" id="sebi" placeholder="Sebi Registration" value="{{ @$user->userdetails['sebi'] ? @$user->userdetails['sebi'] : ''}}">
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-4 col-sm-4"> 
                        <div class="form-group">
                        <label for="Language">Language</label>
                          <select class="select2" multiple="multiple"  name="language[]" data-placeholder="Choose language" style="width: 100%;">
                          @foreach($languages as $k => $v)
                            <option {{ in_array($v->name,explode(",",@$user->userdetails['language'])) ? 'selected' : '' }}  value="{{$v->name ? $v->name : ''}}">{{$v->name ? $v->name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Location By Pin Code</label>
                         <select name="" id="locationSearch" style="width: 100%;">
                        <option>Enter Pincode</option> 
                        </select>
                        </div>
                    </div>
                        <!-- /.form-group -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputFile">Location</label>
                        <input type="text" name="location" class="form-control" id="location" placeholder="Location" value="{{ @$user->userdetails['location'] ? @$user->userdetails['location'] : ''}}" readonly>
                    </div>
                        <!-- /.form-group -->
                    </div>
                      
                    <div class="col-4 col-sm-4">
                        <div class="form-group">
                        <label for="exampleInputFile">Voice Call Charges(/Min)</label>
                          <input type="number" name="voice_call_charges" class="form-control" id="voice_call_charges" placeholder="Voice Call Charges(/Min)" value="{{ @$user->userdetails['voice_call_charges'] ? @$user->userdetails['voice_call_charges'] : ''}}" min="1">
                        </div>
                    </div>
                        <!-- /.form-group -->
                    

                      <div class="col-4 col-sm-4">
                        <div class="form-group">
                        <label for="exampleInputFile">Video Call Charges(/Min)</label>
                          <input type="number" name="video_call_charges" class="form-control" id="video_call_charges" placeholder="Video Call Charges(/Min)" value="{{ @$user->userdetails['video_call_charges'] ? @$user->userdetails['video_call_charges'] : ''}}" min="1">
                    </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-4 col-sm-4"> 
                        <div class="form-group">
                        <label for="Language">Chat Call Charges(/Min)</label>
                          <input type="number" name="chat_call_charges" class="form-control" id="chat_call_charges" placeholder="Chat Call Charges(/Min)" value="{{ @$user->userdetails['chat_call_charges'] ? @$user->userdetails['chat_call_charges'] : ''}}" min="1">
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-4">
                        <div class="form-group">
                          <label for="exampleInputFile">Live streaming Charges</label>
                          <input type="number" name="live_charges" class="form-control" id="live_charges" placeholder="Analyst Charges" value="{{ @$user->userdetails['live_stream_charges'] ? @$user->userdetails['live_stream_charges'] : ''}}" min="0" >
                        </div>
                      </div>
                      <div class="col-12 col-sm-4">
                        <div class="form-group">
                          <label for="exampleInputFile">Analyst Charges</label>
                          <input type="number" name="analyst_charges" class="form-control" id="analyst_charges" placeholder="Analyst Charges" value="{{ @$user->userdetails['analyst_charges'] ? @$user->userdetails['analyst_charges'] : ''}}" min="1" >
                        </div>
                      </div>
                        <!-- /.form-group -->
                      <div class="col-12 col-sm-4">
                      <div class="form-group">
                          <label for="exampleInputFile">Enable/Disable</label><br>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" name="voice_call_enable" {{(@$user->userdetails['voice_call_enable'] == '1') ? 'checked' : ''}}> 
                            <label class="form-check-label" for="inlineCheckbox1">Voice Call</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" name="video_call_enable" {{(@$user->userdetails['video_call_enable'] == '1') ? 'checked' : ''}}>
                            <label class="form-check-label" for="inlineCheckbox2">Video Call</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" name="chat_call_enable" {{(@$user->userdetails['chat_call_enable'] == '1') ? 'checked' : ''}}>
                            <label class="form-check-label" for="inlineCheckbox3">Chat Call</label>
                          </div>
                      </div>
                        <!-- /.form-group -->
                    </div>

                    </div>
                    <!-- /.row -->
                  </div>
                
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page specific script -->
<script>
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

    $(document).ready(function () {


$('#locationSearch').select2({
    ajax: {
        placeholder: 'Enter Pincode',
        minimumInputLength: 6,
        maximumInputLength: 6,
        delay: 250,
        url: function (params) {
            return 'https://api.postalpincode.in/pincode/' + params.term;
        },
        dataType: 'json',
        processResults: function (data) {
            return {
                results: (data[0].PostOffice || []).map((row) => {
                    var append_val = {
                                id: row.Name, 
                                text: `${row.Name} , ${row.District}(${row.State})/${row.Pincode}` 
                                };
                    return append_val;
                }),
            };
        },
        cache: true // Cache the results to improve performance
    }
});
$('#locationSearch').on('select2:select', function (e) {
    var data = e.params.data;
    $('#location').val(data.text);
});
})

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_analyst_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

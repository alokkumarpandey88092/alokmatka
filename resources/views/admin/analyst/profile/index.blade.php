@extends('layouts.app_new')
@section('title','Matka || Profile')
@section('header_title','Profile')
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              @if ( Session::has('success'))
                <div class="alert alert-success" role="alert" id="alert_msg">
                    {{ Session::get('success') }}
                </div> 
                @endif
                @if ( Session::has('danger'))
                <div class="alert alert-danger" role="alert" id="alert_msg">
                    {{ Session::get('danger') }}
                </div> 
                @endif
          </div>
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('members_image')}}/{{@$user->userdetails['profile']}}"
                       alt="Analyst profile picture" onerror="this.onerror=null;this.src='{{asset('members_image/avatar-male.png')}}';">
                </div>

                <h3 class="profile-username text-center">{{@$user->name ? @$user->name : ''}}</h3>

                <p class="text-muted text-center">{{rtrim(ltrim(Auth::user()->roles->pluck('name'), '["'),']"')}}</p>

                <!-- <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Bio</strong>

                <p class="text-muted">
                {{ @$user->userdetails['bio'] ? @$user->userdetails['bio'] : 'Not Available'}}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted">{{ @$user->userdetails['location'] ? @$user->userdetails['location'] : 'Not Available'}}</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Language</strong>
                <?php 
                $lang = explode(",",@$user->userdetails['language']);
                ?>
                <p class="text-muted">
                @for ($i = 0; $i < count($lang); $i++)
                <span class="tag tag-success">{{$lang[$i]}}</span>
                @endfor
                </p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Description</strong>

                <p class="text-muted">{{ @$user->userdetails['description'] ? @$user->userdetails['description'] : 'Not Available'}}</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#basic" data-toggle="tab">Basic</a></li>
                  <li class="nav-item"><a class="nav-link" href="#additional" data-toggle="tab">Additional</a></li>
                  <li class="nav-item"><a class="nav-link" href="#other" data-toggle="tab">Others</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                <div class="active tab-pane" id="basic">
                    <form class="form-horizontal"  method="post" action="{{ route('profiles.update',$user->id) }}"  enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" placeholder="Name" value="{{ @$user->name ? @$user->name : old('name')}}" name="name">
                          @error('name')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" readonly value="{{ @$user->email ? @$user->email : old('email')}}">
                          @error('email')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                            @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">Mobile</label>
                        <div class="col-sm-10">
                          <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile"  value="{{ @$user->mobile ? @$user->mobile : old('mobile')}}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 && this.value.length<10" maxlength="10">
                        @error('mobile')
                            <li style="color:#eb0928fa">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Language</label>
                        <div class="col-sm-10">   
                        <select class="select2" multiple="multiple"  name="language[]" data-placeholder="Choose language" style="width: 100%;">
                          @foreach($languages as $k => $v)
                            <option {{ in_array($v->name,explode(",",@$user->userdetails['language'])) ? 'selected' : '' }}  value="{{$v->name ? $v->name : ''}}">{{$v->name ? $v->name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="Experience" class="col-sm-2 col-form-label">Experience</label>
                        <div class="col-sm-10">
                        <select class="select2" name="experience" data-placeholder="Select Experience" style="width: 100%;" required> 
                          <option value=""></option>
                          @for($i=1;$i<=50;$i++)
                          <option value="{{$i}}" {{(@$user->userdetails['experience'] == $i ) ? 'selected' : ''}}>{{$i}} Years</option>
                          @endfor
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="profile" class="col-sm-2 col-form-label">Profile Picture</label>
                        <div class="col-sm-10">
                        <input class="form-control" type="file" id="profile" name="profile">
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-10 col-sm-2">
                          <button type="button" id="btn_next" class="btn btn-primary">Next</button>
                        </div>
                      </div>
                    
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="additional">
                 
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                        <textarea name="description" class="form-control" id="description" placeholder="Description">{{ @$user->userdetails['description'] ? @$user->userdetails['description'] : ''}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Bio</label>
                        <div class="col-sm-10">
                        <textarea name="bio" class="form-control" id="bio" placeholder="Bio" value="">{{ @$user->userdetails['bio'] ? @$user->userdetails['bio'] : ''}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">Consultant</label>
                        <div class="col-sm-10">
                        <input type="text" name="consultant" class="form-control" id="consultant" placeholder="Consultant" value="{{ @$user->userdetails['consultant'] ? @$user->userdetails['consultant'] : ''}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Sebi Registration</label>
                        <div class="col-sm-10">   
                        <input type="text" name="sebi" class="form-control" id="sebi" placeholder="Sebi Registration" value="{{ @$user->userdetails['sebi'] ? @$user->userdetails['sebi'] : ''}}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="Experience" class="col-sm-2 col-form-label">Location By Pin Code</label>
                        <div class="col-sm-10">
                        <select name="" id="locationSearch" style="width: 100%;">
                        <option>Enter Pincode</option> 
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="profile" class="col-sm-2 col-form-label">Location</label>
                        <div class="col-sm-10">
                        <input type="text" name="location" class="form-control" id="location" placeholder="Location" value="{{ @$user->userdetails['location'] ? @$user->userdetails['location'] : ''}}" readonly>
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-8 col-sm-4">
                          <button type="button" class="btn btn-primary" id="btn_prev1">Previous</button>
                          <button type="button" class="btn btn-primary" id="btn_next1">Next</button>
                        </div>
                      </div>
                    
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="other">
                  
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Voice Call Charges(/Min)</label>
                        <div class="col-sm-10">
                        <input type="number" name="voice_call_charges" class="form-control" id="voice_call_charges" placeholder="Voice Call Charges(/Min)" value="{{ @$user->userdetails['voice_call_charges'] ? @$user->userdetails['voice_call_charges'] : ''}}" min="1" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Video Call Charges(/Min)</label>
                        <div class="col-sm-10">
                        <input type="number" name="video_call_charges" class="form-control" id="video_call_charges" placeholder="Video Call Charges(/Min)" value="{{ @$user->userdetails['video_call_charges'] ? @$user->userdetails['video_call_charges'] : ''}}" min="1" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">Chat Call Charges(/Min)</label>
                        <div class="col-sm-10">
                        <input type="number" name="chat_call_charges" class="form-control" id="chat_call_charges" placeholder="Chat Call Charges(/Min)" value="{{ @$user->userdetails['chat_call_charges'] ? @$user->userdetails['chat_call_charges'] : ''}}" min="1" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Analyst Charges</label>
                        <div class="col-sm-10">   
                        <input type="number" name="analyst_charges" class="form-control" id="analyst_charges" placeholder="Analyst Charges" value="{{ @$user->userdetails['analyst_charges'] ? @$user->userdetails['analyst_charges'] : ''}}" min="1" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="Experience" class="col-sm-2 col-form-label">Enable/Disable</label>
                        <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" name="voice_call_enable" {{(@$user->userdetails['voice_call_enable'] == '1') ? 'checked' : ''}}> 
                            <label class="form-check-label" for="inlineCheckbox1">Voice Call</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" name="video_call_enable" {{(@$user->userdetails['video_call_enable'] == '1') ? 'checked' : ''}}>
                            <label class="form-check-label" for="inlineCheckbox2">Video Call</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" name="chat_call_enable" {{(@$user->userdetails['chat_call_enable'] == '1') ? 'checked' : ''}}>
                            <label class="form-check-label" for="inlineCheckbox3">Chat Call</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group row">
                        <div class="offset-sm-8 col-sm-4">
                          <button type="button" id="btn_prev2" class="btn btn-primary">Previous</button>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
   $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();

     $('#btn_next').click(function(){
      $('a[href="#additional"]').addClass('active');
      $('a[href="#basic"],a[href="#other"]').removeClass('active');
      //$('a[href="#basic"]').removeClass('active');
       $('#additional').addClass('active');
       $('#basic ,#other').removeClass('active');
     });
     $('#btn_next1').click(function(){
      $('a[href="#other"]').addClass('active');
      $('a[href="#basic"],a[href="#additional"]').removeClass('active');
      //$('a[href="#basic"]').removeClass('active');
       $('#other').addClass('active');
       $('#basic ,#additional').removeClass('active');
     });
     $('#btn_prev1').click(function(){
      $('a[href="#additional"],a[href="#other"]').removeClass('active');
      $('a[href="#basic"]').addClass('active');
       $('#basic').addClass('active');
       $('#additional,#other').removeClass('active');
     });
     $('#btn_prev2').click(function(){
      $('a[href="#additional"]').addClass('active');
      $('a[href="#basic"],a[href="#other"]').removeClass('active');
      //$('a[href="#basic"]').removeClass('active');
       $('#additional').addClass('active');
       $('#basic ,#other').removeClass('active');
     });

      $('#locationSearch').select2({
      ajax: {
          placeholder: 'Enter Pincode',
          minimumInputLength: 6,
          maximumInputLength: 6,
          delay: 250,
          url: function (params) {
              return 'https://api.postalpincode.in/pincode/' + params.term;
          },
          dataType: 'json',
          processResults: function (data) {
              return {
                  results: (data[0].PostOffice || []).map((row) => {
                      var append_val = {
                                  id: row.Name, 
                                  text: `${row.Name} , ${row.District}(${row.State})/${row.Pincode}` 
                                  };
                      return append_val;
                  }),
              };
          },
          cache: true // Cache the results to improve performance
      }
  });
  $('#locationSearch').on('select2:select', function (e) {
      var data = e.params.data;
      $('#location').val(data.text);
  });
    });
  $(function () {
    $("#tbl_usermanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

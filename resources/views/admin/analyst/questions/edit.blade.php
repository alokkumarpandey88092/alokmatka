@extends('layouts.app_new')
@section('title','Analyst Ji || Edit Questions')
@section('header_title','Edit Questions')
@push('css')

@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('questions.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_analyst_save" method="post" action="{{ route('questions.update',$questions->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label for="question">Name</label>
                            <input type="text" name="question" class="form-control" id="question" required placeholder="Enter Question" value="{{ @$questions->question ? @$questions->question : old('question')}}">
                            @error('question')
                          <li style="color:#e71814">{{ $message }}</li>
                            @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      
                      <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label for="question">Status</label>
                        <select class="select2" name="status" style="width: 100%;">
                            <option value="1" {{ (@$questions->status==1) ? 'selected' : ''}}>Active</option>
                            <option value="0" {{ (@$questions->status==0) ? 'selected' : ''}}>Inactive</option>
                          </select>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      
                      </div>

                    

                    </div>
                    
                    </div>

                    </div>
                    <!-- /.row -->
                  </div>
                
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page specific script -->
<script>
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

    $(document).ready(function () {


$('#locationSearch').select2({
    ajax: {
        placeholder: 'Enter Pincode',
        minimumInputLength: 6,
        maximumInputLength: 6,
        delay: 250,
        url: function (params) {
            return 'https://api.postalpincode.in/pincode/' + params.term;
        },
        dataType: 'json',
        processResults: function (data) {
            return {
                results: (data[0].PostOffice || []).map((row) => {
                    var append_val = {
                                id: row.Name, 
                                text: `${row.Name} , ${row.District}(${row.State})/${row.Pincode}` 
                                };
                    return append_val;
                }),
            };
        },
        cache: true // Cache the results to improve performance
    }
});
$('#locationSearch').on('select2:select', function (e) {
    var data = e.params.data;
    $('#location').val(data.text);
});
})

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_analyst_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

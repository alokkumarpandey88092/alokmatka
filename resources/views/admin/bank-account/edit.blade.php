<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Edit Bank Account</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Edit Bank Account</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->

    @switch(true)
        @case(Auth::user()->hasRole('User'))
            @include('layouts.user_game.sidebar')
            @include('layouts.user_game.flash_msg')
            @break
        @case(Auth::user()->hasRole('Agent'))
            @include('layouts.agent.sidebar')
            @include('layouts.agent.flash_msg')
            @break
        @case(Auth::user()->hasRole('Partner'))
            @include('layouts.partner.sidebar')
            @include('layouts.partner.flash_msg')
    @endswitch
    <div class="page-wrapper">

      <div class="content container-fluid">
        
      <div class="row">
					<div class="col-xl-8 offset-xl-2">
					
						<!-- Page Header -->
						<div class="page-header">
							<div class="row">
								<div class="col-sm-12">
									<!-- <h3 class="page-title">Add Bank Account</h3> -->
								</div>
							</div>
						</div>
						<!-- /Page Header -->
						
						<div class="card">
							<div class="card-body">
							
								<!-- Form -->
								<form action="{{ route('bank-accounts.update',$data->id) }}" method="post">
                  @csrf
                  @method('PUT')
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>A/C Number</label>
                        <input class="form-control account_number" name="account_number" type="text" Placeholder="A/C Number" required value="{{ $data->account_number ? $data->account_number : ''}}">
                        @error('account_number')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div>

                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>Ifsc Code</label>
                        <input class="form-control" name="ifsc_code" type="text" Placeholder="Ifsc Code" required value="{{ $data->ifsc_code ? $data->ifsc_code : ''}}">
                        @error('ifsc_code')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div>

                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                        <label>A/C Holder Name</label>
                        <input class="form-control" name="account_holder_name" type="text" Placeholder="A/C Holder Name" required value="{{ $data->account_holder_name ? $data->account_holder_name : ''}}">
                        @error('account_holder_name')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                     </div>
									</div>

									<div class="mt-4">
										<button class="btn btn-primary" type="submit">Update</button>
										<button class="btn btn-primary" type="reset">Reset</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Jodi Play</h2>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Crossing</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>From-to</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>ander-bahar haruf</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script>
      //Handle Input table only accept numeric value
     $(document).on('keypress keyup input dragover drop','.account_number',function(e){
          return e.charCode >= 48 && e.charCode <= 57 && this.value.length<20;
      });
</script>

</body>
</html>
@extends('layouts.app_new')
@section('title','Matka || Create Game')
@section('header_title','Create Game')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('contest.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_contest_save" method="post" action="{{ route('contest.store') }}" enctype="multipart/form-data">
                @csrf
                <!-- /.card-header -->
                  <div class="card-body">
                      <div class="row">
                       <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Game Name</label>
                        <input type="text" name="contest_name" class="form-control" id="contest_name" placeholder="Enter Game Name" value="{{old('contest_name')}}" required>
                        @error('contest_name')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>
                    </div>
                      
                    <!-- <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Start Date</label> 
                        <input type="text" name="start_date" class="form-control" id="start_date" placeholder="Enter Start Date" value="{{old('start_date')}}" required readonly>
                        @error('start_date')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>

                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputEmail1">End Date</label>
                            <input type="text" name="end_date" class="form-control" id="end_date" placeholder="Enter End Date" value="{{old('end_date')}}" required readonly>
                        @error('end_date')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>
                      </div> -->

                      <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputMobile">Start Time</label>
                        <input type="text" name="start_time" class="form-control" id="start_time" placeholder="Enter Start Time" value="{{old('start_time')}}" required >
                        @error('start_time')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputPassword1">End Time</label>
                        <input type="text" name="end_time" class="form-control" id="end_time" placeholder="Enter End Time" value="{{old('end_time')}}" required >
                          @error('end_time')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div> 
                      </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-12 col-sm-12">
                          <label for="formFile" class="form-label">Description</label>
                          <textarea id="description" class="form-control" name="description" Placeholder=" Enter Description" required>{{old('description')}}</textarea>
                          @error('description')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>
                    </div>

                    <div class="row">
                        <!-- <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Member Logo</label>
                          <input class="form-control" type="file" id="member_logo" name="member_logo" placeholder="Choose Logo">
                          @error('member_logo')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror 
                          <br><img style="width:10%" id="memberLogoPreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar_male.png')}}';">
                        </div> -->

                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Game Image</label>
                          <input class="form-control" type="file" id="contest_image" name="contest_image" placeholder="Choose Contest Image">
                          @error('contest_image')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                          <br><img style="width:10%" id="contestImagePreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar_male.png')}}';">
                        </div>  
                    </div>

                  </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>

<!-- Page specific script -->
<script>
  //alert()
      //TIMEPICKER
        $('#start_time, #end_time').timepicker({
        'timeFormat': 'H:i', // Use 'H' for 24-hour format without AM/PM
        'step': 30,
        'scrollDefault': 'now'
      });

// Event handler for start time change
$('#start_time').on('changeTime', function() {
    var startTime = $('#start_time').val();
    var endTime = $('#end_time').val();

    // Check if end time is the same or less than start time
    if (endTime <= startTime) {
      // Set end time to a minute after start time
      var newEndTime = new Date($('#start_time').timepicker('getTime'));
      newEndTime.setMinutes(newEndTime.getMinutes() + 1);
      $('#end_time').timepicker('setTime', newEndTime);
    }
  });

  // Event handler for end time change
  $('#end_time').on('changeTime', function() {
    var startTime = $('#start_time').val();
    var endTime = $('#end_time').val();

    // Check if end time is the same or less than start time
    if (endTime <= startTime) {
      // Set start time to a minute before end time
      var newStartTime = new Date($('#end_time').timepicker('getTime'));
      newStartTime.setMinutes(newStartTime.getMinutes() - 1);
      $('#start_time').timepicker('setTime', newStartTime);
    }
  });


  $("#start_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', minDate);
});

$("#end_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', maxDate);
});


  $('#member_logo').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#memberLogoPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      $('#contest_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#contestImagePreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_contest_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

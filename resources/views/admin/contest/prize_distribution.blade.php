@extends('layouts.app_new')
@section('title','Be Max || Edit Contest Result')
@section('header_title','Edit Contest Result')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              @if ( Session::has('success'))
                <div class="alert alert-success" role="alert" id="alert_msg">
                    {{ Session::get('success') }}
                </div> 
                @endif
                @if ( Session::has('danger'))
                <div class="alert alert-danger" role="alert" id="alert_msg">
                    {{ Session::get('danger') }}
                </div> 
                @endif
          </div>
         
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="{{route('contest.index')}}">Back</a></li>
                  <!-- <li class="nav-item"><a class="nav-link" href="#additional" data-toggle="tab">Additional</a></li>
                  <li class="nav-item"><a class="nav-link" href="#other" data-toggle="tab">Others</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="basic">
                      <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="contest"> </label><br>
                                <img class="profile-user-img img-fluid img-circle" src='{{asset("/members_image/$contest->contest_image")}}' alt="User profile picture">
                              
                            </div>
                            <div class="col-md-6">
                                <label for="contest"> </label><br>
                                <span> <h3>{{ $contest->contest_name }}</h3>  </span>
                              
                            </div>
                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="contest"> Contest Type</label><br>
                            @if($contest->contest_type == 1) Single Choice @elseif($contest->contest_type == 2) Arrangment Type @endif 
                            </div>
                            <div class="col-md-2">
                                <label for="contest">Date Time</label><br>
                            {{ date(SIMPLE_DATE, strtotime($contest->start_date))}} 
                            </div>
                            <div class="col-md-2">
                                <label for="contest">Joning Fee(Rs.)</label><br>
                            {{ $contest->joining_fee}} 
                            </div>
                            <div class="col-md-2">
                                <label for="contest">Seats</label><br>
                            {{$reservedSeats}}/{{$contest->seats}} 
                            </div>
                            <div class="col-md-2">
                                <label for="contest">Pool Prize(Rs.)</label><br>
                           {{$totalCollection}}/{{$contest->pool_price}} 
                            </div>
                        </div>
                        <hr>
                        <h3>Options</h3><br>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <label for="formFile" class="form-label">Option A</label>
                                <input class="form-control" type="text" id="option_a" name="option_a" placeholder="Enter share name" value="{{@$contestData[0]['share_name']}}" required readonly>
                                @error('option_a')
                                <li style="color:#e71814">{{ $message }}</li>
                                @enderror 
                                <br>
                            </div>

                            <div class="col-12 col-sm-6">
                            <label for="formFile" class="form-label">Option B</label>
                            <input class="form-control" type="text" id="option_b" name="option_b" placeholder="Enter share name" value="{{@$contestData[1]['share_name']}}" readonly>
                            @error('option_b')
                            <li style="color:#e71814">{{ $message }}</li>
                            @enderror 
                            
                            </div>  
                            <div class="col-12 col-sm-6">
                                <label for="formFile" class="form-label">Option C</label>
                                <input class="form-control" type="text" id="option_a" name="option_a" placeholder="Enter share name" value="{{@$contestData[2]['share_name']}}" readonly>
                                @error('option_a')
                                <li style="color:#e71814">{{ $message }}</li>
                                @enderror 
                                <br>
                            </div>

                            <div class="col-12 col-sm-6">
                            <label for="formFile" class="form-label">Option D</label>
                            <input class="form-control" type="text" id="option_b" name="option_b" placeholder="Enter share name" value="{{@$contestData[3]['share_name']}}" readonly>
                            @error('option_b')
                            <li style="color:#e71814">{{ $message }}</li>
                            @enderror 
                            
                            </div>  
                            
                        </div>
                        <hr>
                        
                        <form method="post" action="{{route('contest.prizedistribution', $contest->crid)}}">
                        @csrf
                        <input type="hidden" value="{{$contest->contest_type}}" name="contest_type">
                        <input type="hidden" value="{{$contest->id}}" name="contest_id">
                        <input type="hidden" value="{{$contest->start_date}}" name="contest_date">
                        
                        <input type="hidden" value="{{$totalCollection}}" name="collection">
                        <hr>
                        <h3>Prize Distribution</h3><br>
                        <div class="row">
                            <div class="col-md-6">
                                <button id="rowAdder" type="button" class="btn btn-dark">
                                    <span class="bi bi-plus-square-dotted">
                                    </span> Add Row
                                </button>
                               <div id="newinput"></div>
                            </div>
                            <div class="col-md-6 mt-5">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mt-5">
                            </div>
                            <div class="col-md-6 mt-5">
                                <button class="btn btn-success" style="float:right;">Submit</button>
                            </div>
                        </div>
                        </form>    
                      </div>
                    </div> 
                  </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>

<!-- Page specific script -->
<script>
   
        $("#rowAdder").click(function () {
            newRowAdd =
                '<div id="row"> <div class="input-group m-3">' +
                '<div class="input-group-prepend">' +
                '<button class="btn btn-success" type="button">' +
                '<i class="bi bi-award-fill"></i>Winner</button> </div>' +
                '<input type="text" name="winner_id[]" class="form-control m-input" required><div class="input-group-prepend">' +
                '<button class="btn btn-primary" type="button">' +
                '<i class="bi bi-percent"></i></button> </div>'+'<input type="text" name="winner_percent[]" class="form-control m-input" required>'+'<div class="input-group-prepend">' +
                '<button class="btn btn-danger" id="DeleteRow" type="button">' +
                '<i class="bi bi-trash"></i> Delete</button> </div> </div> </div>';
 
            $('#newinput').append(newRowAdd);
        });
        $("body").on("click", "#DeleteRow", function () {
            $(this).parents("#row").remove();
        })
    
      //TIMEPICKER
      $('#start_time, #end_time').timepicker({
        'timeFormat': 'H:i', // Use 'H' for 24-hour format without AM/PM
        'step': 30,
        'scrollDefault': 'now'
      });
  $("#start_date,#end_date").datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            immediateUpdates: true,
            todayBtn: 'linked',
            todayHighlight: true,
            //startDate: new Date(),
        });
  $('#member_logo').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#memberLogoPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      $('#contest_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#contestImagePreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_contest_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Dashboard</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
    <style>
  .table-container {
      max-height: 250px; /* Adjust the maximum height as needed */
      overflow-y: auto;
      border: 1px solid #ccc; /* Optional: Add a border for better visibility */
  }
  #spinner-overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 9999;
  display: none;
}
.spinner {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 50px;
  height: 50px;
  border: 4px solid #ffffff;
  border-top-color: #000000;
  border-radius: 50%;
  animation: spin 1s linear infinite;
}
@keyframes spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>
</head>
<body>
  <div class="main-wrapper">
    <!-- Spinner Overlay -->
    <div id="spinner-overlay">
      <div class="spinner"></div>
      </div>
      <!-- ./Spinner Overlay -->
    <div class="header">
      <div class="header-left">
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Ghaziabad</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <!-- <span class="user-img text-white">
              <p>Profle</p>
            </span> -->
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->
   <!--Flash Message -->
   @include('layouts.user_game.flash_msg')
    <!-- /Flash Message -->
<?php 
use App\Helpers\Helper;
$getWinningNumber = new Helper();
?>
     <!-- Main content -->
 <div class="page-wrapper">

<div class="row">
  <div class="col-xl-9 col-sm-12 col-12 sec">
    <div class="row city">
        <div class="row">
            <div class="game-dasboard">
                <div class="firstbtn-game">Today</div>
                <div class="firstbtn-game">Weekly</div>
                <div class="firstbtn-game">Monthly</div>
            </div>
        </div>
     <div class="col-xl-1 col-sm-12 col-12">
        <div class="card date">
          <div class="card-body pay">
            <div class="dash-widget-header">
              <div class="dash-widget-info">
                <h6 class="text align-items-center">{{ date(SIMPLE_DATE,strtotime(now()))}}</h6>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-xl-4">
        <div class="card palika">
          <div class="card-body pker">
            <div class="row">
              <div class="col-lg-12">
                <div class="row pricearea">
                  <div class="col-lg-8">
                    <div class="pricing-card-price">
                      <h3 class="heading2 price">{{(@$data && $data[0]) ? $data[0]->contest_name : ''}}</h3>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="live">
                      <a href="javascript:void(0)" class="btn btn-danger" data-bs-target="#staticBackdrop">Live</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <h2>{{$getWinningNumber->getWinningNumber((@$data && $data[0]) ? $data[0]->id : NULL)}}</h2>
            <p>Winning Number</p>
            <!-- <h4>05</h4> -->
          </div>
        </div>
      </div>
      <div class="col-xl-7">
        <div class="cards radius-10">
          <div class="row row-cols-2 row-cols-md-2 rows row-cols-xl-2 g-3 bazzar">
          @foreach($data as $k => $v)
            @if($k !== 0)
            <div class="col">
              <div class="card radius-10 overflow-hidden mb-0 shadow-none border">
                <div class="card-body">
                  <div class="d-flex align-items-center">
                    <div class="game-chain">
                      <p class="mb-0 text-secondary del font-14 head-area">
                      {{$v->contest_name ? $v->contest_name : ''}}
                      </p>

                      <h5 class="my-0 num ">{{
                      $getWinningNumber->getWinningNumber($v->id ? $v->id : NULL)}}</h5>
                    </div>

                  </div>
                </div>
                <div class="mt-1" id="chart7">

                </div>
                <div class="position-absolute start-0 bottom-0 m-2">
                  <span class="text-black black">Winning Number</span>
                </div>
              </div>
            </div>
              @endif
            @endforeach
           
          </div>
          <!--end row-->
        </div>
      </div>

    </div>
  </div>

  <div class="col-xl-3 col-sm-12 col-12 sec2">
    <div class="row">
      <div class="card wallet">
        <div class="card-body gamet">
        <div class="row">
        <div class="col-md-6">
        <div class="pricing-header text-white">
            <h2>Pay wallet</h2>
            <p>(₹) {{@$user_wallet ? $user_wallet->total_amount : 0 }}</p>
          </div></div>
         <div class="col-md-6">
           <!-- <a href="javascript:void(0)" class="btn btn-light d-flex justify-content-center btn-block" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Add Money</a> -->
          <a href="javascript:void(0)" class="btn btn-light d-flex justify-content-center btn-block" data-toggle="modal" data-target="#addPaymentModal">Add Money</a></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card money ">
        <div class="card-body gamet">
        <div class="row">
        <div class="col-md-6">
          <div class="pricing-header text-white">
            <h2>Cash wallet</h2>
            <p>(₹) 00.00</p>
          </div>
          </div>
            <div class="col-md-6">
          <!-- <a href="javascript:void(0)" class="btn btn-light d-flex
            justify-content-center btn-block" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Withdrawal Money</a> -->
            <a href="javascript:void(0)" class="btn btn-light d-flex
            justify-content-center btn-block">Withdrawal Money</a>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content container-fluid">
  <div class="row pricing-box">
    <div class="col-xl-9">
      <div class="row">
        
      @foreach($data as $k => $v)
          @php
              $startTime = strtotime($v->start_end_time);
              $endTime = strtotime($v->end_time);
              $currentTime = time();
          @endphp
          <div class="col-md-6 col-lg-4 col-xl-4">
            <div class="card">
              <div class="card-body">
              
                <div class="pricing-card-price">
                  <h3 class="heading2 price">{{$v->contest_name ? $v->contest_name : ''}}</h3>
                </div>
                <ul class="pricing-options">
                  <li>
                    <i class="far fa-check-circle"></i> {{$v->start_end_time ? date(TIME_FORMAT_AM_PM, strtotime($v->start_end_time)) : ''}}-{{$v->end_time ? date(TIME_FORMAT_AM_PM, strtotime($v->end_time)) : ''}}
                  </li>
                  <li>
                    <i class="far fa-check-circle"></i> {{ date(SIMPLE_TIME,strtotime(now()))}}
                  </li>
                </ul>
                <a href="{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? route('user.playGame', $v->id) : 'javascript:void(0)' }}"
                class="btn btn-primary d-flex justify-content-center btn-block" {{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? '' : 'disabled'}}>{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? 'Play' : 'Time Out'}}</a>
              </div>
            </div>
          </div>
      @endforeach
        
      </div>
      
    </div>
    <div class="col-xl-3">
      <div class="row">
        <div class="card radius-10 w-100 leader">
          <div class="card-header">
            <h4 class="text-center">Leaderboard</h4>
          </div>
          <div class="row">
            <div class=" col-4 col-lg-4">
              <div class="circle1">
              <h3>#1</h3>
              <img src="{{ asset('game/assets/img/user (1).png') }}" alt="">
             </div>
            </div>
          <div class="col-4 col-lg-4" "="">
          <div class="circle2">
              <h3>#2</h3>
              <img src="{{ asset('game/assets/img/user (1).png') }}" alt="">
          </div>
          </div>
          <div class="col-4 col-lg-4" "="">
          <div class="circle3">
              <h3>#3</h3>
              <img src="{{ asset('game/assets/img/user (1).png') }}" alt="">
          </div>
          </div>
      </div>
      
      @php 
        $inc=1;
      @endphp
      @foreach($leader_board as $key => $val)
      <div class="row row1">
        <div class="col-3 col-lg-3">
          <div class="number">
          <p>#{{$inc++}}</p>
          </div>
        </div>
        <div class="col-3 col-lg-3">
          <div class="pic">
            <img src="{{ asset('game/assets/img/use.png') }}" alt="">
          </div>
        </div>
        <div class="col-3 col-lg-3">
          <div class="player">
          <p>{{$val->user_name ? $val->user_name : ''}}</p>
          </div>
        </div>
        <div class="col-3 col-lg-3">
          <div class="score">
          <p>{{$val->total_amount ? $val->total_amount : 0}}</p>
          </div>
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>

</div>

</div>
</div>
<!-- / Main content -->

  </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Jodi Play</h2>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Crossing</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>From-to</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>ander-bahar haruf</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>

  <!-- Add Payment In Wallet Modal -->
<div class="modal fade in" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="addPaymentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addPaymentModalLabel">Add Payment to Wallet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Form inside the modal -->
                <form action="{{ route('addBallanceWallet') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="amount">Amount:</label>
                        <input type="text" class="form-control amount" id="amount" name="amount" placeholder="Enter amount" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Add Payment</button>
                </form>
            </div>
        </div>
    </div>
</div>

  <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
   
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<!-- Bootstrap Core JS -->
<!-- <script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script> -->

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<script>
    // Spinner Show starting the page loading
    document.getElementById('spinner-overlay').style.display = 'block';
    //Handle Input table only accept numeric value
    $(document).on('keypress keyup input paste dragover drop','.amount',function(e){
      return e.charCode >= 48 && e.charCode <= 57;
    });
    // Spinner Show starting the page loading
    document.getElementById('spinner-overlay').style.display = 'none';
  </script>
</body>
</html>
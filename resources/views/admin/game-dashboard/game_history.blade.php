<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Game History</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
	<!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
  <!-- Include Select2 and DatePicker CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('game/assets/css/loader.css') }}">
</head>

<body>
  <!--Spinner -->
  <div id="spinner-overlay">
      <div class="spinner"></div>
    </div>
   <!--/.Spinner -->

  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Game History</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->
 <!--Flash Message -->
 @include('layouts.user_game.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">

            <div class="col-md-12">
						<div class="card">
							<div class="card-body">

                <div class="filter-section mb-3">
                    <form method="GET" action="" onsubmit="document.getElementById('spinner-overlay').style.display = 'block';">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="fromDate">From Date:</label>
                                <input type="text" name="fromDate" id="fromDate"
                                    class="form-control datepicker" placeholder="Select From Date" value="{{ $fromDate ?? '' }}">
                            </div>
                            <div class="col-md-3">
                                <label for="toDate">To Date:</label>
                                <input type="text" name="toDate" id="toDate"
                                    class="form-control datepicker" placeholder="Select To Date" value="{{ $toDate ?? '' }}">
                            </div>
                            <div class="col-md-3">
                                <label for="gameName">Game Name:</label>
                                <select name="gameName" id="gameName" class="form-control select2">
                                  <option value="">Select</option>
                                  @foreach($gameName as $k=>$v)
                                    <option value="{{ $v->contest_name ? $v->contest_name : ''}}" {{ $selectedGameName == $v->contest_name ? 'selected' : '' }}>{{ $v->contest_name ? $v->contest_name : ''}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-info form-control text-white" >Filter</button>
                            </div>
                        </div>
                    </form>
                </div>

								<div class="table-responsive">
									<table class="table table-hover table-center mb-0" id="tbl_game_history">
										<thead>
											<tr>
												<th>Date</th>
												<th>Game Name</th>
												<th>Choise</th>
											</tr>
										</thead>
										<tbody>
                      @php 
                        use App\Helpers\Helper;
                        $helper = new Helper();
                      @endphp
                      @foreach($game_history as $k => $v)
											<tr>
												<td>{{ $v->created_at ? date(SIMPLE_DATETIME,strtotime($v->created_at)) : ''}}</td>
												<td>{{ $v->game_name ? $v->game_name : ''}}</td>
												<td>
                          {!! $helper->formatGameData($v->user_choise ? $v->user_choise : '') !!}
                          <!-- <a href="javascript:void(0)" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#staticBackdrop4" data-choise="{{ $v->user_choise ? $v->user_choise : ''}}" game-name="{{ $v->game_name ? $v->game_name : ''}}" onclick="ViewGameChoise(this);">view</a> -->
                        </td>
											</tr>
                      @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

            </div>

          </div>


        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel4" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel4"></h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12 ">
              <div class="col-lg-12 repeattt">
                <!-- <table style="width:100% ">
                </table> -->
                <div id="view_history">
                  
                </div>
                <div class="row repeat-secc">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="price">
                          <i class="fas fa-rupee-sign"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="today-btn">
                          <a href="javascript:void(0)"> Total Aount </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script>
    function ViewGameChoise(id){
          var data = $(id).attr('data-choise');
          var game_name = $(id).attr('game-name');
          var userChoices = JSON.parse(data);
              var totalAmount = 0;
              var history = '';
              var parentElement = $('#staticBackdrop4');
              $('#view_history', parentElement).empty();
              $.each(userChoices, function(index, choice) {
                history += `${choice.game_number}(${choice.game_number_amount})${(index < userChoices.length - 1) ? ', ' :''}`;
                    totalAmount += parseFloat(choice.game_number_amount);
              });
              $('.price i', parentElement).text('   ' + totalAmount.toFixed(2));
              $('#view_history', parentElement).html(history);
              $('#staticBackdropLabel4').text(game_name);
    }
  </script>
  
<!-- Datatables JS -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- Include Bootstrap-datepicker and Select2 JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() { 
    // Initialize Select2
    $('#gameName').select2();
    // Initialize datepicker
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    }); 

    $('#tbl_game_history').DataTable();
  });
</script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=0"
    />
    <title>All Game</title>

    <!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
  </head>

  <body>
    <div class="main-wrapper">
      <div class="header">
        <div class="header-left">
        </div>
        <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
        <h3>
            Open Your Game
          </h3>

        <ul class="nav user-menu">
          <li class="nav-item dropdown noti-dropdown">
            <a
              href="#"
              class="dropdown-toggle nav-link"
              data-bs-toggle="dropdown"
            >
            </a>
          </li>
          <li class="nav-item dropdown">
            <a
              href="javascript:void(0)"
              class="dropdown-toggle user-link nav-link"
              data-bs-toggle="dropdown"
            >
            <!-- <span class="user-img text-white">
              <p>Profle</p>
            </span> -->
         <h4>Profile</h4>
            </a>
          </li>
        
        </ul>
      </div>
      <!-- /Header -->
      <!-- /Header -->
      <!-- /Header -->

      <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->

     <!--Flash Message -->
     @include('layouts.user_game.flash_msg')
    <!-- /Flash Message -->

      <div class="page-wrapper">
       
        <div class="content container-fluid">
         
          <div class="row pricing-box">
            <div class="col-xl-12">
              <div class="row">

              @foreach($data as $k => $v)
                @php
                    $startTime = strtotime($v->start_end_time);
                    $endTime = strtotime($v->end_time);
                    $currentTime = time();
                @endphp
                <div class="col-md-6 col-lg-3 col-xl-3">
                  <div class="card">
                    <div class="card-body">
                    
                      <div class="pricing-card-price text-center">
                        <h3 class="heading2 price">{{$v->contest_name ? $v->contest_name : ''}}</h3>
                      </div>
                      <ul class="pricing-options text-center">
                        <li>
                          <i class="far fa-clock"></i>{{$v->start_end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->start_end_time)) : ''}}-{{$v->end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->end_time)) : ''}}
                        </li>
                        <li>
                          <i class="far fa-clock"></i>{{ date(SIMPLE_TIME,strtotime(now()))}}
                        </li>
                      </ul>
                      <div class="baton text-center">
                        <a
                        href="{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? route('user.playGame', $v->id) : 'javascript:void(0)' }}" class="btn btn-info d-flex justify-content-center btn-block" data-bs-target="#staticBackdrop" {{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? '' : 'disabled'}}>{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? 'Play' : 'Time Out'}}</a>
                      </div>
                    </div>
                  </div>
                </div>
            @endforeach
        </div>
      </div>
    </div>
	<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header justify-content-center">
			  <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>
			  
			</div>
			<div class="modal-body">
			  <div class="row">
				<div class="game">
				  <div class="col-lg-12 col-md-12 col-sm-12">
					<h2>Jodi Play</h2>
				  </div>
				</div>
				
			  </div>
			  <div class="row">
				<div class="game">
				  <div class="col-lg-12 col-md-12 col-sm-12">
					<h2>Crossing</h2>
				  </div>
				</div>
			  </div>
			  <div class="row">
				<div class="game">
				  <div class="col-lg-12 col-md-12 col-sm-12">
					<h2>From-to</h2>
				  </div>
				</div>
			  </div>
			  <div class="row">
				<div class="game">
				  <div class="col-lg-12 col-md-12 col-sm-12">
					<h2>ander-bahar haruf</h2>
				  </div>
				</div>
			  </div>
			</div>
			<div class="modal-footer justify-content-center">
			  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			  
			</div>
		  </div>
		</div>
	  </div>
            <!-- jQuery -->
        <script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
        <script>
            var myModal = document.getElementById("myModal");
            var myInput = document.getElementById("myInput");

            myModal.addEventListener("shown.bs.modal", function () {
            myInput.focus();
            });
        </script>
        

        <!-- Bootstrap Core JS -->
        <script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Slimscroll JS -->
        <script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <!-- Select2 JS -->
        <script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
        <!-- Custom JS -->
        <script src="{{ asset('game/assets/js/admin.js') }}"></script>
  </body>
</html>

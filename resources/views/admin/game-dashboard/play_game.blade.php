<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Play Game</title>

  <!-- Favicons -->
  <link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
<style>
  .table-container {
      max-height: 250px; /* Adjust the maximum height as needed */
      overflow-y: auto;
      border: 1px solid #ccc; /* Optional: Add a border for better visibility */
  }
  #spinner-overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 9999;
  display: none;
}
.spinner {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 50px;
  height: 50px;
  border: 4px solid #ffffff;
  border-top-color: #000000;
  border-radius: 50%;
  animation: spin 1s linear infinite;
}
@keyframes spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>
</head>

<body>
  <div class="main-wrapper">
      <!-- Spinner Overlay -->
      <div id="spinner-overlay">
      <div class="spinner"></div>
      </div>
      <!-- ./Spinner Overlay -->
    <!-- Header -->
    <div class="header">
      <div class="header-left">
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <a href="javascript:void(0);" id="toggle_btn">
        <h3>{{@$data  ? $data->contest_name : ''}}</h3>
        <h6> {{ date(SIMPLE_DATE,strtotime(now()))}}</h6>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
        <h3>{{@$data  ? $data->contest_name : ''}}</h3>
      </a>

      <ul class="nav user-menu">
        <!--  -->
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <div class="timer">
              <i class="fas fa-clock" id="remainingTime"></i>
            </div>
            <input type="hidden" value="{{@$data ? $data->start_end_time : ''}}" id="game_start_time"/>
            <input type="hidden" value="{{@$data ? $data->end_time : ''}}" id="game_end_time"/>
            <div class="pro">
              <a href="javascript:void(0)"> <i class="fas fa-wallet"></i> (₹) {{@$user_wallet ? $user_wallet->total_amount : 0 }} </a>
            </div>

          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

     <!--Sidebar -->
     @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->

    <!--Flash Message -->
      @include('layouts.user_game.flash_msg')
  <!-- /Flash Message -->

    <div class="page-wrapper">
      <div class="row repit">
      </div>
      <div class="row">
        <div class="col-xl-12">
          <div class="tabs-content">
            <div role="tabpanel" class="tab-pane mt-4 page-calendar active" id="schedule">
              <div class="col-xl-12 col-12 col-md-12 into">
                <div class="how">


                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop" d-flex
                    justify-content-center btn-block">Repeat Game</a>
                  <a href="" class="btn " data-bs-toggle="modal" data-bs-target="#staticBackdrop6" d-flex
                    justify-content-center btn-block">From Into</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop2" d-flex
                    justify-content-center btn-block">Open Play</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop3" d-flex
                    justify-content-center btn-block">Crossing Play</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop4" d-flex
                    justify-content-center btn-block">Mesage Paste</a>
                <a href="javascript:void(0)" class="btn " data-bs-toggle="modal" data-bs-target="#" onclick="PaltiGame();">Palti Play</a>
                </div>
              </div>
              <div class="row g-3">
                <div class="col-md-12 col-lg-6 from">
                  <div class="table-box">
                    <section>
                      <div class="container-fluid">
                          @php
                          $num = 1;
                          @endphp
                        <form method="post" action="{{ route('user.playGame.store') }}" id="frm_play_game">
                          @csrf
                          <input type="hidden" name="game_type" id="game_type" value="normal">
                          <input type="hidden" name="game_id" value="{{ @$id }}">
                          <input type="hidden" name="user_wallet_amount" id="user_wallet_amount" value="{{ @$user_wallet ? $user_wallet->total_amount : 0 }}">
                        <div class="a111">
                          <h3>Jantari</h3>
                          @for ($i = 1; $i <= 10; $i++)
                          <div class="row raw1">
                          @for ($j = 1; $j <= 10; $j++)
                            <div class="col-lg-1">
                              <div class="one">
                                <p><?php 
                                  if ($num<10) {
                                    echo '0'.$num;
                                  }else if($num > 9 && $num < 100){
                                    echo $num;
                                  }elseif ($num == 100) {
                                    echo '00';
                                  }
                                  ?></p>
                                  <input type="hidden" name="game_number[]" value="<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>">
                                  <input type="text" data-value="<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>" id="game_number_amount_<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>" name="game_number_amount[]" class="in game_number_amount"/>
                              </div>
                              <div class="b1"></div>
                            </div>
                            @php
                              $num++;
                              @endphp
                            @endfor
                          </div>
                          @endfor
                          <!-- </form> -->
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="col-md-12 col-lg-4 repit">
                  <div class="body mb-3">
                    <div class="event-name b-lightred row">
                      <div class="col-12 andar">
                        <h4>Andar Haruf</h4>
                      </div>
                      <section>
                        <div class="container-fluid">
                          <div class="row side">
                          @for ($i = 1; $i <= 10; $i++)
                              <div class="col-lg-1">
                                  <div class="ones">
                                      <p>{{ ($i % 10 == 0) ? '0' : $i }}</p>
                                      <input type="hidden" name="ander_game_number[]" value="{{ ($i % 10 == 0) ? '0' : $i }}">
                                      <input class="inner ander_game_number_amount game_number_amount" type="text" data-value="{{ ($i % 10 == 0) ? '0' : $i }}" id="ander_game_number_amount_{{ ($i % 10 == 0) ? '0' : $i }}" name="ander_game_number_amount[]" />
                                  </div>
                                  <div class="b{{ ($i % 10 == 0) ? '0' : $i }}"></div>
                              </div>
                          @endfor
                          </div>
                        </div>
                      </section>
                    </div>
                    <div class="event-name b-greensea row">
                      <div class="col-12 bahar">
                        <h4>Bahar Haruf</h4>
                      </div>
                      <section>
                        <div class="container-fluid">
                          <div class="row side">
                          @for ($i = 1; $i <= 10; $i++)
                              <div class="col-lg-1">
                                  <div class="ones">
                                      <p>{{ ($i % 10 == 0) ? '0' : $i }}</p>
                                      <input type="hidden" name="bahar_game_number[]" value="{{ ($i % 10 == 0) ? '0' : $i }}">
                                      <input class="inner bahar_game_number_amount game_number_amount" type="text" data-value="{{ ($i % 10 == 0) ? '0' : $i }}" id="bahar_game_number_amount_{{ ($i % 10 == 0) ? '0' : $i }}" name="bahar_game_number_amount[]" />
                                  </div>
                                  <div class="b{{ ($i % 10 == 0) ? '0' : $i }}"></div>
                              </div>
                          @endfor
                          </form>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4 meet">
                      <!-- <a href="javascipt:void(0)" id="refreshButton" class="btn btn-primary  botn btn-lg disabled" tabindex="-1" role="button"
                        aria-disabled="true">Refresh</a> -->
                        <button type="button" class="" id="refreshButton">Refresh</button>
                    </div>
                    <div class="col-lg-4 total">
                      <p><b style="">Total:</b> &#x20B9; <span id="total_amount">0</span> </p>
                    </div>
                    <div class="col-lg-4 nice">
                      <!-- <a href="#" class="btn btn-primary botn btn-lg disabled" data-bs-toggle="modal"
                        data-bs-target="#exampleModal" tabindex="-1" role="button" aria-disabled="true">Calculate</a> -->
                        <button type="button" class="" id="btn_submit_game">Submit</button>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- pop up  -->
    <!-- Button trigger modal -->


    <!-- Modal -->
    <!-- Modal -->
    <!-- Modal -->

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  " style="width:100% ">
        <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-5 paste">

              <h4>Today History</h4>
              <div class="table-container">
                  <table style="width:100%;">
                  </table>
              </div>
              <p></p>

            </div>
            <div class="col-lg-5 repeat">
              <div class="table-container">
                  <table style="width:100%;">
                  </table>
              </div>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0)" onclick="getAutoFillPreviousGameHistory('staticBackdrop');" data-bs-dismiss="modal"> Submit Game </a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
  <!-- one popup -->
  <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop6" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabe6">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 form">
              <div class="row">
                <div class="col-lg-5">
                  <input type="text " class="left" placeholder="From">
                </div>
                <div class="col-lg-5">
                  <input type="text" class="right" placeholder="To">
                </div>
              </div>
              <i class="fas "></i>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <input type="text" class="amount" placeholder="Amount">
                  </div>
                
                </div>
              </div>
              <div class="row">
                <a href="javascript:void(0)" onclick="GoByFromInTo();">
                <div class="col-lg-10 go text-white">Go Submit</div>
                  </a>
              </div>
            </div>
            <div class="col-lg-6 repeatt">
              <div class="table-container">
                  <table style="width:100%;">
                  </table>
              </div>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0)" data-bs-dismiss="modal" onclick="ShowAfterFromToSelection('staticBackdrop6');"> Submit Game </a>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div> <!--one popup -->
  <!-- Third popup  -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style="width:100%">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel2" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel2">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 form">
              <div class="row">
                <div class="col-lg-12">
                  <input type="text" class="leftt" placeholder="Number">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <input type="text" class="amountt" placeholder="Amount">
                  </div>
                
                </div>
              </div>
              <!-- <div class="row mt-5">
                <a href="javascript:void(0)" onclick="GoByOpenPlay();">
                    <div class="col-lg-10 go text-white">Go Submit</div>
                 </a>
              </div> -->
            
            </div>
            <div class="col-lg-6 repeatt">
              <div class="table-container">
                  <table style="width:100%;">
                  </table>
              </div>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <a href="javascript:void(0);" data-bs-dismiss="modal" onclick="ShowAfterOpenPlaySelection('staticBackdrop2');">
                      <div class="today-btn text-white"> Submit Game</div> 
                    </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


  <!-- second popup end -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop3" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel3">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 hows">
             <div class="row">
              <div class="col-lg-12 cap">
                <div class="row side1">
                  <div class="col-lg-1">
                    <div class="ones">
                     
                      <input class="inner2" type="text" fdprocessedid="kuyui">
                    </div>
                    <div class="b1"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                      
                      <input class="inner2" type="text" fdprocessedid="6a99m">
                    </div>
                    <div class="b2"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                      
                      <input class="inner2" type="text" fdprocessedid="8msvhf">
                    </div>
                    <div class="b3"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                      
                      <input class="inner2" type="text" fdprocessedid="pybf5y">
                    </div>
                    <div class="b4"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                     
                      <input class="inner2" type="text" fdprocessedid="fir0l">
                    </div>
                    <div class="b5"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                     
                      <input class="inner2" type="text" fdprocessedid="1esl0r">
                    </div>
                    <div class="b6"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                     
                      <input class="inner2" type="text" fdprocessedid="xoq9os">
                    </div>
                    <div class="b7"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                      
                      <input class="inner2" type="text" fdprocessedid="kcaobm">
                    </div>
                    <div class="b8"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                  
                      <input class="inner2" type="text" fdprocessedid="jp5fgn">
                    </div>
                    <div class="b9"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">
                    
                      <input class="inner2" type="text" fdprocessedid="j7c3s">
                    </div>
                    <div class="b10"></div>
                  </div>
                </div>
              </div>
             </div>
             <div class="row">
              <div class="col-lg-12">
                <input type="text" class="ama" placeholder="Amount">
              </div>
             </div>
             <div class="row">
              <div class="col-lg-6">
                <a href="javascript:void(0)" onclick="generateJodi();">
                <div class="jodie text-white">With Jodie</div>
                </a>
              </div>
              <div class="col-lg-6">
               <a href="javascript:void(0)" onclick="generateWithoutJodi();">
                <div class="without text-white">Without Jodie</div>
                </a>
              </div>
             </div>
            </div>
            <div class="col-lg-6 repeatts">
              <div class="table-container">
              <table style="width:100%;">
              </table>
                </div>
              <div class="row repeat-secc">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <a href="javascript:void(0)" data-bs-dismiss="modal" onclick="ShowAfterCrossingPlaySelection('staticBackdrop3');">
                      <div class="today-btn text-white">Submit Game</div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


  <!-- third popup end -->
  <!-- four popup start -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel4" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel4">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 metal-row">
              <div class="row">
                <div class="col-lg-12">
                  <input type="text" class="input_message_paste metal" placeholder="Enter Value">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 ex">
                  <h6>Example(1):- 10(10),11(10),12(10),13(10),14(10),15(10),16(10)</h6>
                  <h6>Example(2):- 10(10)11(10)12(10)13(10)14(10)15(10)16(10)</h6>
                  <h6>Example(3):- 10(10)-11(10)-12(10)-13(10)-14(10)-15(10)-16(10)</h6>
                </div>
              </div>
             
            </div>
            <div class="col-lg-6 ">
              <div class="col-lg-6 repeattt">
                <div class="table-container">
                  <table style="width:100%;">
                  </table>
                </div>
                <div class="row repeat-secc">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="price">
                          <i class="fas fa-rupee-sign">00.00</i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <a href="javascript:void(0);" data-bs-dismiss="modal" onclick="SubmitAfterMessagePaste('staticBackdrop4');">
                          <div class="today-btn text-white"> Submit Game</div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>

  <!-- pop up end -->

  <!-- jQuery -->
  <script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
   <!-- Include SweetAlert CDN -->
   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script>
     // Spinner Show starting the page loading
    document.getElementById('spinner-overlay').style.display = 'block';
  //$(document).ready(function () {
        var isGameOver = false;
        // Assuming start_time and end_time are coming from the database
        var startTime = document.getElementById("game_start_time").value;
        var endTime = document.getElementById("game_end_time").value;
         // Update remaining time every second
        var intervalId = setInterval(updateRemainingTime, 1000);
        
        function updateRemainingTime() {
            var now = new Date();
            var startDate = new Date(now.toDateString() + " " + startTime);
            var endDate = new Date(now.toDateString() + " " + endTime);

            // Calculate remaining time in milliseconds
            var remainingTime = endDate - now;
            //console.log(remainingTime)

            if (remainingTime > 0) {
              isGameOver = true;
                // Convert remaining time to hours, minutes, and seconds
                var hours = Math.floor(remainingTime / (60 * 60 * 1000));
                var minutes = Math.floor((remainingTime % (60 * 60 * 1000)) / (60 * 1000));
                var seconds = Math.floor((remainingTime % (60 * 1000)) / 1000);
                // Format the remaining time
                var formattedTime =
                    padZero(hours) + "h " + padZero(minutes) + "m " + padZero(seconds) + "s";

                // Display remaining time
                document.getElementById("remainingTime").innerHTML = formattedTime
            } else {
                document.getElementById("remainingTime").innerHTML = "Event ended";
                isGameOver = false;
                clearInterval(intervalId);
                 // Show SweetAlert
                 Swal.fire({
                   title: "Game Over",
                   text: "Your game has over!",
                   icon: "info",
                   confirmButtonText: "OK",
                   allowOutsideClick: false,
                   allowEscapeKey: false,
                   allowEnterKey: false,
                   showCloseButton: false
               }).then((result) => {
                   // If the user clicks "OK", redirect to another route
                   if (result.isConfirmed) {
                       window.location.href = "{{ route('GameList') }}"; // Replace with your desired route
                       hasRedirected = true; // Set the flag to true after redirection
                   }
               });
            }
        }

        // Function to pad a number with zero if it's less than 10
        function padZero(number) {
            return (number < 10 ? "0" : "") + number;
        }
        
        // Initial update
        updateRemainingTime();

      // Call when Page has load for showing data in modal
      getPreviousGameHistory();
    function getPreviousGameHistory(){
          // Make an AJAX request when the page loads
        $.ajax({
          url:"{{ url('user/previous-game') }}",
          type: 'GET',
          dataType: 'json',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Use the CSRF token in headers
          },
          success: function (data) {
              var totalAmount = 0;
              var parentElement = $('#staticBackdrop');
            $.each(data, function(index, choice) {
                  var userJantariChoices = JSON.parse(choice.user_choise);
                  var userAndarChoices = JSON.parse(choice.user_andar_haruf_choise);
                  var userBaharChoices = JSON.parse(choice.user_bahar_haruf_choise);
                  //console.log(userBaharChoices)
                    userJantariString = '';
                    userAndarString = '';
                    userBaharString = '';
                  $.each(userJantariChoices, function(i, v) {
                    userJantariString += `${v.game_number}(${v.game_number_amount})${(i< (userJantariChoices.length-1) ? ',' : '')}`;
                    });
                  $.each(userAndarChoices, function(i, v) {
                    userAndarString += `${v.ander_game_number}(${v.ander_game_number_amount})${(i< (userAndarString.length-1) ? ',' : '')}`;
                    });
                  $.each(userBaharChoices, function(i, v) {
                    userBaharString += `${v.bahar_game_number}(${v.bahar_game_number_amount})${(i< (userBaharString.length-1) ? ',' : '')}`;
                    });
                  // $('table', parentElement).eq(0).append(`
                  //     <tr>
                  //     <td><b>${choice.game_name}</b></td>
                  //     <td>Jantari:- ${userJantariString}</td>
                  //     <td>Andar Haruf:- ${userAndarString}</td>
                  //     <td>Bahar Haruf:- ${userBaharString}</td>
                  //     <td><input type="radio" name="appendinsecondtable" onchange="AppendInSecondTable(this,'staticBackdrop');" data-choise="${choice.game_number}" data-amount="${choice.game_number_amount}" data-index="${index}" data-andar='${choice.user_andar_haruf_choise}' data-bahar='${choice.user_bahar_haruf_choise}' data-jantari='${choice.user_choise}'></td>
                  //     </tr>
                  //     `);
                  $('table', parentElement).eq(0).append(`
                      <tr>
                      <td><b>${choice.game_name}</b></td>
                      <td>${userJantariString}</td>
                      <td><input type="radio" name="appendinsecondtable" onchange="AppendInSecondTable(this,'staticBackdrop');" data-choise="${choice.game_number}" data-amount="${choice.game_number_amount}" data-index="${index}" data-andar='${choice.user_andar_haruf_choise}' data-bahar='${choice.user_bahar_haruf_choise}' data-jantari='${choice.user_choise}'></td>
                      </tr>
                      `);
                        totalAmount += parseFloat(choice.game_number_amount);
              });
          },
          error: function (error) {
              console.error('Error:', error);
          }
      });
    }

    // append number in other table on select and deselect
    function AppendInSecondTable(checkbox,modal_id){
      $(`#${modal_id} table`).eq(1).empty();
      var choise = $(checkbox).attr('data-choise');
      var amount = $(checkbox).attr('data-amount');
      var index = $(checkbox).attr('data-index');
      var jantari = $(checkbox).attr('data-jantari');
      //console.log(jantari);return false;
      var totalAmount = 0;
      if ($(checkbox).is(':checked')) {
        var userJantariChoices = JSON.parse(jantari);
        $.each(userJantariChoices, function(index, choise) {
          $(`#${modal_id} table`).eq(1).append(`
                <tr class="table_tr" id="table_tr_${index}" data-amount="${choise.game_number_amount}" data-choise="${choise.game_number}">
                <td>${choise.game_number}(${choise.game_number_amount})</td>
                <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'${modal_id}');"></button></td>
                </tr>
                `);
            });
       
      } else {
        $(`#table_tr_${index}`).remove();
      }
      $(`#${modal_id} .table_tr`).each(function(){
        totalAmount += parseFloat($(this).attr('data-amount'));
      });
      $(`#${modal_id} .price i`).text('   ' + totalAmount.toFixed(2));
    }

      // Remove Current Row on Click Button
      function RemoveRow(btn,modal_id){
        var totalAmount = 0;
        $(btn).parents('tr').remove();
        $(`#${modal_id} .table_tr`).each(function(){
          totalAmount += parseFloat($(this).attr('data-amount'));
        });
        $(`#${modal_id} .price i`).text('   ' + totalAmount.toFixed(2));
    }

    //refresh all value
    $('#refreshButton').on('click', function () {
      if(confirm('Are you sure you want to refresh ?')) {
        refreshAmount();

      }
    });
    //function for clear value 
    function refreshAmount(){
      $('.game_number_amount').each(function () {
            $(this).val('');
        });
        $('#total_amount').text('00.00');
    }
     
    //Handle Input table only accept numeric value
    $(document).on('keypress keyup input paste dragover drop','.game_number_amount, .ander_game_number_amount, .bahar_game_number_amount, .left, .right, .amount, .leftt, .amountt, .ama',function(e){
      if ($(this).hasClass('leftt') || $(this).hasClass('amountt')) {
        GoByOpenPlay();
        }
      return e.charCode >= 48 && e.charCode <= 57;
    });

    //Handle Input table only accept numeric value and also length should be 1
    $(document).on('input','.inner2',function(e){
        //return e.charCode >= 48 && e.charCode <= 57 && this.value.length<1;
        var enteredValue = this.value;

          // Check if the entered value is a single digit
          var isValidInput = /^[0-9]$/.test(enteredValue);

          // Check if the entered value is unique among other input values
          var isUnique = $('.inner2').filter(function () {
            return this.value === enteredValue;
          }).length === 1;

          // Check if Input is Valid and IsUnique
          if (isValidInput && isUnique) {
              var nextInput = $(this).closest('.col-lg-1').next().find('.inner2');
              if (nextInput.length > 0) {
                  nextInput.focus();
              } else {
                  console.log("No more '.inner2' elements after the current one.");
              }
          }
          // If the entered value is valid and unique, allow it, otherwise prevent the input
          if (!isValidInput || !isUnique) {
            this.value = ''; // Clear the input value if it's not valid
            e.preventDefault();
            return false;
          }
    });


    function getAutoFillPreviousGameHistory(model_id){
          var totalAmount = 0;
          //$('#refreshButton').trigger('click');
            refreshAmount();
           $(`#${model_id} .table_tr`).each(function(){
                  totalAmount += parseFloat($(this).attr('data-amount'));
                  var paddedNum = ("00" + parseInt($(this).attr('data-choise'))).slice(-2);
                  var inputField = $('#game_number_amount_' + paddedNum);
                  // Check if the input field exists before setting its value
                  if (inputField.length) {
                      inputField.val($(this).attr('data-amount'));
                  }
              });
              // Update the content of the price div with the total amount
              $('#total_amount').text(totalAmount.toFixed(2));
              $('#game_type').val('game_repeat');
    }

    //GoByFromInTo
    function GoByFromInTo(){
      var fromValue =$('#staticBackdrop6 .left').val();
      var toValue = $('#staticBackdrop6 .right').val();
      var amountValue =$('#staticBackdrop6 .amount').val();
      var total_amount_for_show = 0;
      // Check if the values are in the range 01 to 99
      if (/^[0-9]{2}$/.test(fromValue) && /^[0-9]{2}$/.test(toValue)) {
        // Check if "from" is not greater than "to"
        if (parseInt(fromValue, 10) <= parseInt(toValue, 10) || (toValue === "00" && parseInt(fromValue, 10) < 100)) {
          if(amountValue=='' || amountValue==null){
            alert("Please enter Amount.");
            return false;
          }
          $("#staticBackdrop6 table").empty();
              total_amount_for_show = 0;
              for (var i = parseInt(fromValue, 10); i <= parseInt(toValue, 10); i++) {
                var paddedNumber = ("0" + i).slice(-2);
                $("#staticBackdrop6 table").append(`
                <tr class="table_tr" id="table_tr_${i}" data-amount="${amountValue}" data-choise="${paddedNumber}">
                <td>${paddedNumber}(${amountValue})</td>
                <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'staticBackdrop6');"></button></td>
                </tr>
                `);
                total_amount_for_show += parseInt(amountValue, 10);
              }
              $("#staticBackdrop6 .price i").text(total_amount_for_show);
          //alert("Values are within the range.");
        } else {
          alert("'From' should not be greater than 'To'.");
        }
      } else {
        alert("Values should be in the range of 01 to 99.");
      }
    }

    //ShowAfterFromToSelection
    function ShowAfterFromToSelection(model_id){
          //$('#refreshButton').trigger('click');
            refreshAmount();
          var totalAmount = 0;
          $(`#${model_id} .table_tr`).each(function(){
              totalAmount += parseFloat($(this).attr('data-amount'));
              var paddedNum = ("00" + parseInt($(this).attr('data-choise'))).slice(-2);
              var inputField = $('#game_number_amount_' + paddedNum);
              // Check if the input field exists before setting its value
              if (inputField.length) {
                  inputField.val($(this).attr('data-amount'));
              }
          });
            // Update the content of the price div with the total amount
            $('#total_amount').text(totalAmount.toFixed(2));
            $('#game_type').val('game_from_to');
    }

    //GoByOpenPlay
    function GoByOpenPlay(){
        $("#staticBackdrop2 table").empty();
        var fromValue =$('#staticBackdrop2 .leftt').val();
        var amountValue =$('#staticBackdrop2 .amountt').val();
        if(amountValue=='' || amountValue==null){
          $("#staticBackdrop2 .price i").text('00.00');
            return false;
        }
        var total_amount_for_show = 0;
        var numberString = fromValue.toString();
        var pairs = [];
        // Loop through the string in steps of 2
        for (var i = 0; i < numberString.length; i += 2) {
          // Take pairs of two digits and add to the array
          var pair = numberString.substr(i, 2);
          // Check if the pair has two digits
          //if (pair.length === 2) {
          if (pair.length === 2 && pairs.indexOf(pair) === -1) {
            pairs.push(pair);
            $("#staticBackdrop2 table").append(`
            <tr class="table_tr" id="table_tr_${i}" data-amount="${amountValue}" data-choise="${pair}">
            <td>${pair}(${amountValue})</td>
            <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'staticBackdrop2');"></button></td>
            </tr>
            `);
            total_amount_for_show += parseInt(amountValue, 10);
          }
        }
        $("#staticBackdrop2 .price i").text(total_amount_for_show);
    }

    function allowOnlyNumbers(inputElement) {
        // Remove non-numeric characters using regular expression
        inputElement.value = inputElement.value.replace(/[^0-9]/g, '');
    }

    //Show Result After Message Paste In Other Table
    $(document).on('keypress keyup input paste dragover drop','.input_message_paste',function(e){
        GoByMessagePaste();
    });
    // Funtion For Showing Result After Message Paste In Other Table
    function GoByMessagePaste() {
        $("#staticBackdrop4 table").empty();
        var inputString = $('#staticBackdrop4 .input_message_paste').val();
        //var matches = inputString.match(/\d+\(\d+\)/g);
        var matches = inputString.match(/\b(?:00|0[1-9]|10|[1-9]\d)\(\d+\)/g);
        if (!matches) {
            $("#staticBackdrop4 .price i").text('00.00');
            return false;
        }
        var total_amount_for_show = 0;
        var uniqueNumbers = new Set();
        matches.forEach(function (match, index) {
            var [number, amount] = match.split(/[()]/);
            //if(!uniqueNumbers.has(number)){
              if (/^(?:00|0[1-9]|10|[1-9]\d)$/.test(number) && !uniqueNumbers.has(number)) {
              $("#staticBackdrop4 table").append(`
                <tr class="table_tr" id="table_tr_${index}" data-amount="${amount}" data-choise="${number}">
                    <td>${number}(${amount})</td>
                    <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'staticBackdrop4');"></button></td>
                </tr>
            `);
            total_amount_for_show += parseInt(amount, 10);
              // Add the number to the set to mark it as encountered
              uniqueNumbers.add(number);
            }
        });
        $("#staticBackdrop4 .price i").text(total_amount_for_show);
    }

    function SubmitAfterMessagePaste(model_id) {
      var totalAmount = 0;
      //$('#refreshButton').trigger('click');
        refreshAmount();
      $(`#${model_id} .table_tr`).each(function(){
              totalAmount += parseFloat($(this).attr('data-amount'));
              var paddedNum = ("00" + parseInt($(this).attr('data-choise'))).slice(-2);
              var inputField = $('#game_number_amount_' + paddedNum);
              // Check if the input field exists before setting its value
              if (inputField.length) {
                  inputField.val($(this).attr('data-amount'));
              }
          });
          // Update the content of the price div with the total amount
          $('#total_amount').text(totalAmount.toFixed(2));
          $('#game_type').val('game_message_paste'); 
}

    //ShowAfterFromToSelection
    function ShowAfterOpenPlaySelection(model_id){
      //$('#refreshButton').trigger('click');
        refreshAmount();
      var totalAmount = 0;
       $(`#${model_id} .table_tr`).each(function(){
              totalAmount += parseFloat($(this).attr('data-amount'));
              var paddedNum = ("00" + parseInt($(this).attr('data-choise'))).slice(-2);
              var inputField = $('#game_number_amount_' + paddedNum);
              // Check if the input field exists before setting its value
              if (inputField.length) {
                  inputField.val($(this).attr('data-amount'));
              }
          });
      $('#total_amount').text(totalAmount.toFixed(2));
      $('#game_type').val('game_open_play');
    }

     //generateWithoutJodi
     function generateWithoutJodi() {
      var number = "";
      $('.inner2').each(function () {
          var inputValue = $(this).val();
          // Remove spaces from the input value
          var cleanedValue = inputValue.replace(/\s/g, '');

          // Concatenate cleaned value to the result string
          number += cleanedValue;
      });
      var total_amount_for_show = 0;
      var amountValue = $('#staticBackdrop3 .ama').val();
      if (amountValue === '') {
          alert('Please enter amount');
          return false;
      }
      $("#staticBackdrop3 table").empty();
      var result = [];
      for (var i = 0; i < number.length; i++) {
          for (var j = 0; j < number.length; j++) {
              if (i !== j) {
                  // Skip combinations like 11, 22, 33, etc.
                  if (number[i] !== number[j]) {
                      result.push(number[i] + number[j]);
                      $("#staticBackdrop3 table").append(`
                      <tr class="table_tr" id="table_tr_${i}" data-amount="${amountValue}" data-choise="${number[i] + number[j]}">
                      <td>${number[i] + number[j]}(${amountValue})</td>
                      <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'staticBackdrop3');"></button></td>
                      </tr>
                      `);
                      total_amount_for_show += parseInt(amountValue, 10);
                  }
              }
          }
      }
      $("#staticBackdrop3 .price i").text(total_amount_for_show);
      $("#game_type").val('game_without_jodi');
}


    //generateJodi
    function generateJodi() {
      var number = "";
      $('.inner2').each(function() {
      var inputValue = $(this).val();
      // Remove spaces from the input value
      var cleanedValue = inputValue.replace(/\s/g, '');

      // Concatenate cleaned value to the result string
        number += cleanedValue;
    });
    var total_amount_for_show = 0;
    var amountValue =$('#staticBackdrop3 .ama').val();
      if(amountValue==''){
        alert('Please enter amount')
        return false;
      }
    $("#staticBackdrop3 table").empty();
      var result = [];
      var digits = number.toString().split('');

      for (var i = 0; i < digits.length; i++) {
        for (var j = 0; j < digits.length; j++) {
          //if (i !== j) {
            result.push(digits[i] + digits[j]);
            $("#staticBackdrop3 table").append(`
            <tr class="table_tr" id="table_tr_${i}" data-amount="${amountValue}" data-choise="${digits[i] + digits[j]}">
            <td>${digits[i] + digits[j]}(${amountValue})</td>
            <td><button type="button" class="btn-close btn-danger" aria-label="Close" onclick="RemoveRow(this,'staticBackdrop3');"></button></td>
            </tr>
            `);
            total_amount_for_show += parseInt(amountValue, 10);
         //}
        }
      }
      $("#staticBackdrop3 .price i").text(total_amount_for_show);
      $("#game_type").val('game_with_jodi');
      }

      //ShowAfterCrossingPlaySelection
    function ShowAfterCrossingPlaySelection(model_id){
      var totalAmount = 0;
      //$('#refreshButton').trigger('click');
        refreshAmount();
       $(`#${model_id} .table_tr`).each(function(){
              totalAmount += parseFloat($(this).attr('data-amount'));
              var paddedNum = ("00" + parseInt($(this).attr('data-choise'))).slice(-2);
              var inputField = $('#game_number_amount_' + paddedNum);
              // Check if the input field exists before setting its value
              if (inputField.length) {
                  inputField.val($(this).attr('data-amount'));
              }
          });
      $('#total_amount').text(totalAmount.toFixed(2));
    }

  // Palti Game Center
    function PaltiGame() {
        var paltNumber = '';
          $('.game_number_amount').trigger('keypress');
          if (parseInt($('#total_amount').text()) == 0) {
            alert("Please Select Minimum one number and amount can'nt be 0");
            return false;
            }
        $('.game_number_amount').each(function () {
            let inputValue = parseInt($(this).val());
            if (!isNaN(inputValue)) {
              if (paltNumber !== '') {
                  paltNumber += ','; // Add a comma if the string is not empty
              }
              paltNumber += $(this).attr('data-value');
            }
        });
      if (window.confirm(`Do you want Palti Play for all these numbers ?:- ${paltNumber}`)) {
        $('.game_number_amount').each(function () {
            let inputValue = parseInt($(this).val());
            if (!isNaN(inputValue)) {
              var value  = $(this).attr('data-value');
              var array_split = value.split('');
                $(`#game_number_amount_${array_split[1]}${array_split[0]}`).val(inputValue);
            }
        });
        $('.game_number_amount').trigger('keypress');
      }
    }
 
    $(document).ready(function(){
    var total_amount = 0;
    var user_wallet_amount = parseFloat($('#user_wallet_amount').val());
    
    //trigger keyup onchange amount
    $('.game_number_amount').on('keypress keyup input paste',function(){
      total_amount = 0;
      $('.game_number_amount').each(function(){
         var amount = parseFloat($(this).val());
         if (!isNaN(amount)) {
          total_amount += amount;
         }
      });
      $('#total_amount').text(total_amount.toFixed(2));
    });

    //start trigger keyup event onload page
    $('.game_number_amount').trigger('keypress');
    //end trigger keyup event onload page

    $('#btn_submit_game').click(function(){
      $('.game_number_amount').trigger('keypress');
      // Check if the balance is sufficient
      if (parseInt($('#total_amount').text()) == 0) {
         alert("Please Select Minimum one number and amount can'nt be 0");
         return false;
        } 
    if (user_wallet_amount >= total_amount) {
          $('#frm_play_game').submit();
        } else {
          alert('Insufficient balance. Please add Ballance in your wallets.');
        }
    });
  });

  // Spinner Hide after load page
  document.getElementById('spinner-overlay').style.display = 'none';
  </script>
  
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>

</body>

</html>
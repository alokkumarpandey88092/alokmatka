<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Game History</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
	<!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
   <!-- Include Select2 and DatePicker CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('game/assets/css/loader.css') }}">
</head>

<body>
   <!--Spinner -->
   <div id="spinner-overlay">
      <div class="spinner"></div>
    </div>
   <!--/.Spinner -->
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Wallet History</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->
     <!--Flash Message -->
     @include('layouts.user_game.flash_msg')
    <!-- /Flash Message -->

    <div class="page-wrapper">

      <div class="content container-fluid">

        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">

            <div class="col-md-12">
						<div class="card">
							<div class="card-body">

              <div class="filter-section mb-3">
                    <form method="GET" action="" onsubmit="document.getElementById('spinner-overlay').style.display = 'block';">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="fromDate">From Date:</label>
                                <input type="text" name="fromDate" id="fromDate"
                                    class="form-control datepicker" placeholder="Select From Date" value="{{ $fromDate ?? '' }}">
                            </div>
                            <div class="col-md-3">
                                <label for="toDate">To Date:</label>
                                <input type="text" name="toDate" id="toDate"
                                    class="form-control datepicker" placeholder="Select To Date" value="{{ $toDate ?? '' }}">
                            </div>
                            <div class="col-md-3">
                                <label for="selectType">Select Type:</label>
                                <select name="selectType" id="selectType" class="form-control select2">
                                  <option value="">Select Type</option>
                                  <option value="Debit" {{ $selectedType == "Debit" ? 'selected' : '' }}>Debit</option>
                                  <option value="Credit" {{ $selectedType == "Credit" ? 'selected' : '' }}>Credit</option>

                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-info form-control text-white" >Filter</button>
                            </div>
                        </div>
                    </form>
                </div>

								<div class="table-responsive">
									<table class="table table-hover table-center mb-0" id="tbl_wallet_history">
										<thead>
											<tr>
												<th>Txn. Order Id</th>
												<th>Date</th>
												<th>Transaction Id</th>
												<th>Amount</th>
												<th>Transaction Type</th>
												<th>Paid For</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
                    @php
                        use App\Helpers\Helper;
                        $helper = new Helper();
                      @endphp
                    @foreach($wallet_history as $k => $v)
											<tr>
                        <td>{{++$k}}</td>
												<td>{{ $v->created_at ? date(SIMPLE_DATETIME,strtotime($v->created_at)) : ''}}</td>
												<td>{{ $v->transaction_id ? $v->transaction_id : ''}}</td>
												<td> &#8377; {{ $v->wallet_amount ? $v->wallet_amount : ''}}</td>
												<td>{{ $v->type ? $v->type : ''}}</td>
												<td>
                        <span>{{ $v->game_name ? $v->game_name : ''}}</span>
                          @if($v->user_choise)
                          :- {!! $helper->formatGameData($v->user_choise) !!}
                          @else
                          Wallet
                          @endif

                        </td>
                        <td>{{ $v->pay_status ? $v->pay_status : ''}}</td>
											</tr>
                    @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

            </div>

          </div>


        </div>

      </div>
    </div>
  </div>

    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>

<!-- Datatables JS -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- Include Bootstrap-datepicker and Select2 JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    // Initialize Select2
    $('#gameName').select2();
    // Initialize datepicker
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $('#tbl_wallet_history').DataTable();
  });
</script>
</body>
</html>

@extends('layouts.user_game.app')
@section('title','Dashboard || Game')
@section('header_title','Game')
@push('css')
<!-- Select CSS -->
<link rel="stylesheet" href="{{ asset('ration/assets/css/select2.min.css') }}">
@endpush
@section('content')
 <!-- Main content -->
 <div class="page-wrapper">
			<div class="content container-fluid">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
					
						<!-- Page Header -->
						<div class="page-header">
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-title">Game</h3>
								</div>
							</div>
						</div>
						<!-- /Page Header -->
						
						<div class="card">
							<div class="card-body">
							
								<!-- Form -->
								<form  action="https://myaadhaar.uidai.gov.in/genricDownloadAadhaar" method="get">
								<!-- <form id="aadhaarForm" action="" method="post" > -->
									@csrf
									<div class="form-group">
										<label>Adhar Number</label>
										<input class="form-control" type="text" placeholder="Adhaar Number" name="adhaar_number">
									</div>
									<div class="form-group">
										<label>Bio Detail</label>
										<input class="form-control" type="text" placeholder=" Bio Detaail" name="bio_detaila">
									</div>
										<button class="btn btn-primary" type="submit">Submit</button>
										<button class="btn btn-primary" type="reset">Reset</button>
									</div>
								</form>
								<!-- /Form -->
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <!-- / Main content -->
@endsection

@push('scripts')

<script>
    function redirectToAadhaarPortal() {
        // Get the form input values
        var adhaarNumber = document.getElementsByName('adhaar_number')[0].value;
        var bioDetail = document.getElementsByName('bio_detail')[0].value;

        // Redirect to the Aadhaar portal with the input values
        var redirectUrl = 'https://myaadhaar.uidai.gov.in/genricDownloadAadhaar?adhaar_number=' + encodeURIComponent(adhaarNumber) + '&bio_detail=' + encodeURIComponent(bioDetail);
        window.location.href = redirectUrl;

        // Cancel form submission
        return false;
    }
</script>
@endpush

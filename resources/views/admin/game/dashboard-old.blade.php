@extends('layouts.user_game.app')
@section('title','Dashboard || Game')
@section('header_title','Game')
@push('css')
<!-- Select CSS -->
<link rel="stylesheet" href="{{ asset('ration/assets/css/select2.min.css') }}">
@endpush
@section('content')
<!-- Header -->
    <div class="header">
      <div class="header-left">
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
        <h3>Dashboard</h3><br>
        <!-- <h6> Wednesday 09/12/2023</h6> -->
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
        <h3>Ghaziabad</h3>
      </a>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown" style="position:static;">
            <span class="user-img text-white">
              <h4>Profile</h4>
            </span>
            <!-- <h4>Profile</h4> -->
          </a>
        </li>
      </ul>
    </div>
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.user_game.sidebar')
    <!-- /Sidebar -->

 <!-- Main content -->
 <div class="page-wrapper">

      <div class="row">
        <div class="col-xl-9 col-sm-12 col-12 sec">
          <div class="row city">
            <div class="row">

              <div class="col-xl-4 col-sm-12 col-12">
                <div class="card cards">
                  <div class="card-body">
                    <div class="dash-widget-header">
                      <div class="dash-widget-info">
                        <h6 class="text align-items-center text-capitalize text-white">
                          Today
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-4 col-sm-12 col-12">
                <div class="card cards">
                  <div class="card-body">
                    <div class="dash-widget-header">
                      <div class="dash-widget-info">
                        <h6 class="text align-items-center text-capitalize text-white">
                        weekly
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-4 col-sm-12 col-12">
                <div class="card cards">
                  <div class="card-body">
                    <div class="dash-widget-header">
                      <div class="dash-widget-info">
                        <h6 class="text align-items-center text-capitalize text-white">
                        Monthly
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-xl-1 col-sm-12 col-12">
              <div class="card date">
                <div class="card-body">
                  <div class="dash-widget-header">
                    <div class="dash-widget-info">
                      <h6 class="text align-items-center">{{ date(SIMPLE_DATE,strtotime(now()))}}</h6>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-xl-4">
              <div class="card palika">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="row">
                        <div class="col-lg-8">
                          <div class="pricing-card-price">
                            <h3 class="heading2 price">{{(@$data && $data[0]) ? $data[0]->contest_name : ''}}</h3>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="live">
                            <a href="" class="btn btn-danger " data-bs-target="#staticBackdrop" d-flex=""
                              justify-content-center="" btn-block"="">Live</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


                  <h2>{{\App\Helpers\Helper::getWinningNumber((@$data && $data[0]) ? $data[0]->id : NULL)}}</h2>
                  <p>Winning Number</p>
                  <!-- <h4>05</h4> -->
                </div>
              </div>
            </div>
            <div class="col-xl-7">
              <div class="cards radius-10">
                <div class="row row-cols-2 row-cols-md-2 rows row-cols-xl-2 g-3 bazzar">
                @foreach($data as $k => $v)
                  <div class="col">
                    <div class="card radius-10 overflow-hidden mb-0 shadow-none border">
                      <div class="card-body">
                        <div class="d-flex align-items-center">
                          <div>
                            <p class="mb-0 text-secondary del font-14">
                            {{$v->contest_name ? $v->contest_name : ''}}
                            </p>

                            <h5 class="my-0 num ">{{\App\Helpers\Helper::getWinningNumber($v->id ? $v->id : NULL)}}</h5>
                          </div>

                        </div>
                      </div>
                      <div class="mt-1" id="chart7">

                      </div>
                      <div class="position-absolute start-0 bottom-0 m-2">
                        <span class="text-black black">Winning Number</span>
                      </div>
                    </div>
                  </div>
                  @endforeach
                 
                </div>
                <!--end row-->
              </div>
            </div>

          </div>
        </div>

        <div class="col-xl-3 col-sm-4 col-4 sec2">
          <div class="row">
            <div class="card wallet">
              <div class="card-body">
                <div class="pricing-header text-white">
                  <h2>Pay wallet</h2>
                  <p>(₹) {{@$user_wallet ? $user_wallet->total_amount : 0 }}</p>
                </div>
                <a href="" class="btn btn-light data-bs-toggle=" modal" data-bs-target="#staticBackdrop" d-flex
                  justify-content-center btn-block">Add Money</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="card money ">
              <div class="card-body">
                <div class="pricing-header text-white">
                  <h2>Cash wallet</h2>
                  <p>(₹) 00.00</p>
                </div>
                <a href="" class="btn btn-light d-flex
                  justify-content-center btn-block" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Withdrawal Money</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="content container-fluid">
        <div class="row pricing-box">
          <div class="col-xl-9">
            <div class="row">
              
            @foreach($data as $k => $v)
                @php
                    $startTime = strtotime($v->start_end_time);
                    $endTime = strtotime($v->end_time);
                    $currentTime = time();
                @endphp
                <div class="col-md-6 col-lg-4 col-xl-4">
                  <div class="card">
                    <div class="card-body">
                    
                      <div class="pricing-card-price">
                        <h3 class="heading2 price">{{$v->contest_name ? $v->contest_name : ''}}</h3>
                      </div>
                      <ul class="pricing-options">
                        <li>
                          <i class="far fa-check-circle"></i> {{$v->start_end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->start_end_time)) : ''}}-{{$v->end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->end_time)) : ''}}
                        </li>
                        <li>
                          <i class="far fa-check-circle"></i> {{ date(SIMPLE_TIME,strtotime(now()))}}
                        </li>
                      </ul>
                      <!-- <a href="{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? route('user.playGame', $v->id) : 'javascript:void(0)' }}"
                      class="btn btn-primary d-flex justify-content-center btn-block " data-bs-toggle="modal" data-bs-target="#staticBackdrop" {{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? '' : 'disabled'}}>play</a> -->
                      <a href="{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? route('user.playGame', $v->id) : 'javascript:void(0)' }}"
                      class="btn btn-primary d-flex justify-content-center btn-block" {{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? '' : 'disabled'}}>{{ (($currentTime >= $startTime) && ($currentTime <= $endTime)) ? 'Play' : 'Time Out'}}</a>
                    </div>
                  </div>
                </div>
            @endforeach
              
            </div>
            
          </div>
          <div class="col-xl-3">
            <div class="row">
              <div class="card radius-10 w-100 leader">
                <div class="card-header">
                  <h4 class="text-center">Leaderboard</h4>
                </div>
                <div class="row">
				  <div class=" col-4 col-lg-4">
                    <div class="circle1">
                    <h3>#1</h3>
                    <img src="{{ asset('ration/assets/img/user (1).png') }}" alt="">
                   </div>
                  </div>
                <div class="col-4 col-lg-4" "="">
                <div class="circle2">
                    <h3>#2</h3>
                    <img src="{{ asset('ration/assets/img/user (1).png') }}" alt="">
                </div>
                </div>
                <div class="col-4 col-lg-4" "="">
                <div class="circle3">
                    <h3>#3</h3>
                    <img src="{{ asset('ration/assets/img/user (1).png') }}" alt="">
                </div>
                </div>
            </div>
            
            @php 
              $inc=1;
            @endphp
            @foreach($leader_board as $key => $val)
            <div class="row row1">
              <div class="col-3 col-lg-3">
                <div class="number">
                <p>#{{$inc++}}</p>
                </div>
              </div>
              <div class="col-3 col-lg-3">
                <div class="pic">
                  <img src="{{ asset('ration/assets/img/use.png') }}" alt="">
                </div>
              </div>
              <div class="col-3 col-lg-3">
                <div class="player">
                <p>{{$val->user_name ? $val->user_name : ''}}</p>
                </div>
              </div>
              <div class="col-3 col-lg-3">
                <div class="score">
                <p>{{$val->total_amount ? $val->total_amount : 0}}</p>
                </div>
              </div>
            </div>
            @endforeach

          </div>
        </div>
      </div>

    </div>

  </div>
  </div>
 <!-- / Main content -->



    <div class="modal fade" id="staticBackdrop1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Amount</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('wallets.store') }}">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Amount:</label>
            <input type="text" class="form-control" id="amount" name="amount" required pattern="^\d+(\.\d{1,2})?$">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')

<script>
    function redirectToAadhaarPortal() {
        // Get the form input values
        var adhaarNumber = document.getElementsByName('adhaar_number')[0].value;
        var bioDetail = document.getElementsByName('bio_detail')[0].value;

        // Redirect to the Aadhaar portal with the input values
        var redirectUrl = 'https://myaadhaar.uidai.gov.in/genricDownloadAadhaar?adhaar_number=' + encodeURIComponent(adhaarNumber) + '&bio_detail=' + encodeURIComponent(bioDetail);
        window.location.href = redirectUrl;

        // Cancel form submission
        return false;
    }
</script>
@endpush

@extends('layouts.user_game.app')
@section('title','Dashboard || Play Game')
@section('header_title','Play Game')
@push('css')
<!-- Select CSS -->
<link rel="stylesheet" href="{{ asset('ration/assets/css/select2.min.css') }}">
@endpush
@section('content')
 <!-- Main content -->
 <div class="page-wrapper">
        <div class="row repit">
          <div class="col-xl-12 col-12 col-md-12">
            <a 
              href="{{ route('user.playGame', $id) }}"
              class="btn btn-primary botn btn-lg"
              tabindex="-1"
              role="button"
              aria-disabled="true"
              >Repit Game</a
            >
            <a
              href="#"
              class="btn btn-primary botns btn-lg"
              tabindex="-1"
              role="button"
              aria-disabled="true"
              >Crossing Play</a
            >
            <a href="javascript:void(0)" class="btn btn-primary botnss btn-lg" tabindex="-1" role="button" aria-disabled="true" onclick="showTwoInputPrompt();" data-id="{{ route('user.playGame', $id) }}">From Into</a>
          <a
          href="#"
          class="btn btn-primary botnsss btn-lg"
          tabindex="-1"
          role="button"
          aria-disabled="true"
          >message paste</a
        >
          </div>
        </div>
        <div class="row">
          <div class="col-xl-12">
            <div class="tabs-content">
              <div
                role="tabpanel"
                class="tab-pane mt-4 page-calendar active"
                id="schedule"
              >
                <div class="row g-3">
                  <div class="col-md-12 col-lg-8">
                    <div class="table-box">
                      <section>
                        <div class="container-fluid">
                          @php
                            $num = 1;
                          @endphp
                          <form method="post" action="{{ route('user.playGame.store') }}" id="frm_play_game">
                            @csrf
                            <input type="hidden" name="game_id" value="{{ @$id }}">
                            <input type="hidden" name="user_wallet_amount" id="user_wallet_amount" value="{{ @$user_wallet ? $user_wallet->total_amount : 0 }}">
                            @for ($i = 1; $i <= 10; $i++)
                            <div class="row raw1">
                              @for ($j = 1; $j <= 10; $j++)
                              <div class="col-lg-1">
                                <div class="one">
                                  <p>@if($num < 10)
                                              0
                                      @endif
                                    @if($num==100)
                                      00
                                      @else
                                      {{ $num }}
                                    @endif
                                    </p>
                                  <input type="hidden" name="game_number[]" value="@if($num < 10){{0}}@endif{{ ($num==100) ? 00 : $num }}">
                                  <input type="text" data-value="@if($num < 10){{0}}@endif{{ ($num==100) ? 00 : $num }}" id="game_number_amount" name="game_number_amount[]" class="in game_number_amount"/>
                                </div>
                                <div class="b1"></div>
                              </div>
                                @php
                                $num++;
                                @endphp
                              @endfor
                            </div>
                             @endfor
                          </form>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-4">
                    <div class="body mb-3">
                      <div class="event-name b-lightred row">
                        <div class="col-12 text-center">
                          <h4>Ram</h4>
                        </div>
                        <section>
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-lg-1">
                                <div class="ones">
                                  <p>01</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b1"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="twos">
                                  <p>02</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b2"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="threes">
                                  <p>03</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b3"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="fours">
                                  <p>04</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b4"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="fives">
                                  <p>05</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b5"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="sixs">
                                  <p>06</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b6"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="sevens">
                                  <p>07</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b7"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="eights">
                                  <p>08</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b8"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="nines">
                                  <p>09</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b9"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="tens">
                                  <p>10</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b10"></div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                      <hr />
                      <div class="event-name b-greensea row">
                        <div class="col-12 text-center">
                          <h4>Aryan</h4>
                        </div>
                        <section>
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-lg-1">
                                <div class="ones">
                                  <p>01</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b1"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="twos">
                                  <p>02</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b2"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="threes">
                                  <p>03</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b3"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="fours">
                                  <p>04</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b4"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="fives">
                                  <p>05</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b5"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="sixs">
                                  <p>06</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b6"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="sevens">
                                  <p>07</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b7"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="eights">
                                  <p>08</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b8"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="nines">
                                  <p>09</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b9"></div>
                              </div>
                              <div class="col-lg-1">
                                <div class="tens">
                                  <p>10</p>
                                  <input class="in" type="text" />
                                </div>
                                <div class="b10"></div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <h6>Total Amount:- <span id="total_amount">00.00</span></h6>
                    <button type="button" class="btn btn-round btn-primary m-t-15">calculate</button>
                    <button type="button" class="btn btn-round btn-primary m-t-15 d-none" id="btn_submit_game">Submit</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
 <!-- / Main content -->

@endsection

@push('scripts')

<script>
  $(document).ready(function(){
    var total_amount = 0;
    var user_wallet_amount = parseFloat($('#user_wallet_amount').val());
    $('#calculate_total').click(function(){
      total_amount = 0;
      $('.game_number_amount').each(function(){
         var amount = parseFloat($(this).val());
         if (!isNaN(amount)) {
          total_amount += amount;
         }
      });
      $('#total_amount').text(total_amount)
      console.log(total_amount)
      if(total_amount>0){
        $('#btn_submit_game').removeClass('d-none')
      }else{
        $('#btn_submit_game').addClass('d-none')
      }
    });

    //trigger keyup onchange amount
    $('.game_number_amount').on('input change keyup',function(){
      total_amount = 0;
      $('.game_number_amount').each(function(){
         var amount = parseFloat($(this).val());
         if (!isNaN(amount)) {
          total_amount += amount;
         }
      });
      $('#total_amount').text(total_amount)
      console.log(total_amount)
      if(total_amount>0){
        $('#btn_submit_game').removeClass('d-none')
      }else{
        $('#btn_submit_game').addClass('d-none')
      }
    });

    $('#btn_submit_game').click(function(){
      // Check if the balance is sufficient
    if (user_wallet_amount >= total_amount) {
          $('#frm_play_game').submit();
        } else {
          alert('Insufficient balance. Please add Ballance in your wallets.');
        }
    });
  });

  function setAction(id){
    var url = $(id).attr('data-id');
    $('#frm_game_type').attr('action',url);
  }

  function showTwoInputPrompt() {
    const input1 = prompt("FROM:- ");
    const from = parseFloat(input1);
    
    if (isNaN(from)) {
        alert("Please enter a valid number for FROM.");
    } else if(from > 99) {
            alert("value should not be greater than 99");
      } else {
        const input2 = prompt("TO:- ");
        const to = parseFloat(input2);
        
        if (isNaN(to)) {
            alert("Please enter a valid number for TO.");
        } else if (to < from) {
            alert("TO should be greater than or equal to FROM.");
        }else if (to > 99) {
          alert("value should not be greater than 99");
        } else {
            const input3 = prompt("AMOUNT:- ");
            const amount = parseFloat(input3);
            
            if (isNaN(amount)) {
                alert("Please enter a valid number for AMOUNT.");
            } else {
                $(".game_number_amount").each(function(index) {
                    // Check if the index is within the specified range (from to to)
                    if (index >= (from-1) && index <= (to-1)) {
                        // Update the value of the input element
                        $(this).val(amount); // Index starts at 'from'
                    }
                });
                $('.game_number_amount').trigger('change')
                alert("Values updated successfully.");
            }
        }
    }
}

</script>
@endpush

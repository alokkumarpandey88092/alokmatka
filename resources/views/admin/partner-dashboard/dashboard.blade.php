<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Dashboard</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
    <style>
      .fa_dashbord_color{
        background-color:#5b68eb !important;
      }
      </style>
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      @php
      $role = Auth::user()->roles()->first();
      @endphp
      <h3>Welcome, {{ Auth::user()->name }}! ({{ $role ? $role->name : '' }})</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <!-- <span class="user-img text-white">
              <p>Profle</p>
            </span> -->
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.partner.sidebar')
    <!-- /Sidebar -->
   <!--Flash Message -->
   @include('layouts.partner.flash_msg')
    <!-- /Flash Message -->
<?php
use App\Helpers\Helper;
$getWinningNumber = new Helper();
?>
     <!-- Main content -->
 <div class="page-wrapper">

 <div class="row">
					<div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-user"></i>
									</span>
									<div class="dash-widget-info">
										<h3>{{ count(@$game) }}</h3>
										<a href="javascript:void(0)"><h6 class="text-muted">Total Game </h6></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-credit-card "></i>
									</span>
									<div class="dash-widget-info">
										<h3>{{ count(@$details) }}</h3>
										<h6 class="text-muted">Total Play</h6>
									</div>
								</div>
							</div>
						</div>
					</div>


          <div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-credit-card "></i>
									</span>
									<div class="dash-widget-info">
										<h3>0</h3>
										<h6 class="text-muted">Total Win</h6>
									</div>
								</div>
							</div>
						</div>
					</div>

          <div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-credit-card "></i>
									</span>
									<div class="dash-widget-info">
										<h3>0</h3>
										<h6 class="text-muted">Total Closing Balance</h6>
									</div>
								</div>
							</div>
						</div>
					</div>

          <div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-credit-card "></i>
									</span>
									<div class="dash-widget-info">
										<h3>0</h3>
										<h6 class="text-muted">Admin Commission</h6>
									</div>
								</div>
							</div>
						</div>
					</div>

          <div class="col-lg-4 col-md-6 col-12">
						<div class="card">
							<div class="card-body">
								<div class="dash-widget-header">
									<span class="dash-widget-icon bg-primary fa_dashbord_color">
										<i class="far fa-credit-card "></i>
									</span>
									<div class="dash-widget-info">
										<h3>0</h3>
										<h6 class="text-muted">Agent Commission</h6>
									</div>
								</div>
							</div>
						</div>
					</div>



</div>
</div>
</div>
</div>
<!-- / Main content -->

  </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Jodi Play</h2>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Crossing</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>From-to</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>ander-bahar haruf</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>

  <!-- Add Payment In Wallet Modal -->
<div class="modal fade in" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="addPaymentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addPaymentModalLabel">Add Payment to Wallet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Form inside the modal -->
                <form action="{{ route('addBallanceWallet') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="amount">Amount:</label>
                        <input type="text" class="form-control amount" id="amount" name="amount" placeholder="Enter amount" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Add Payment</button>
                </form>
            </div>
        </div>
    </div>
</div>

  <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script>
    //Handle Input table only accept numeric value
    $(document).on('keypress keyup input paste dragover drop','.amount',function(e){
      return e.charCode >= 48 && e.charCode <= 57;
    });
  </script>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<!-- Bootstrap Core JS -->
<!-- <script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script> -->

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
</body>

</html>

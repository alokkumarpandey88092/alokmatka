<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Create Game</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Game</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.partner.sidebar')
    <!-- /Sidebar -->

     <!--Flash Message -->
     @include('layouts.partner.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
      <div class="row">
					<div class="col-xl-8 offset-xl-2">
					
						<!-- Page Header -->
						<div class="page-header">
							<div class="row">
								<div class="col-sm-12">
									<h3 class="page-title">Create Game</h3>
								</div>
							</div>
						</div>
						<!-- /Page Header -->
						
						<div class="card">
							<div class="card-body">
							
								<!-- Form -->
								<form action="{{ route('partner.GameCreateStore') }}" method="post">
                  @csrf

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Game Image</label>
                        <input class="form-control" type="file" id="contest_image" name="contest_image" placeholder="Choose Contest Image">
                          @error('contest_image')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                          <br><img style="width:10%" id="contestImagePreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar_male.png')}}';">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                            <label>Game Name</label>
                            <input class="form-control" name="contest_name" type="text" Placeholder="Game Name" value="{{old('contest_name')}}" required>
                            @error('contest_name')
                                  <li style="color:#e71814">{{ $message }}</li>
                            @enderror
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Start Game</label>
                          <input type="time" name="start_time" class="form-control"  placeholder="Enter Start Time" value="{{old('start_time')}}" required>
                          @error('start_time')
                                <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>End Game</label>
                        <input type="time" name="end_time" class="form-control"  placeholder="Enter End Time" value="{{old('end_time')}}" required>
                        @error('end_time')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Jantari Winning Amounts(x)</label>
                        <select name="jantari_winning_amount" id="jantari_winning_amount" class="form-control" id="jantari_winning_amount" placeholder="Enter Value" value="{{old('jantari_winning_amount')}}" required> 
                           <option>Select value</option>
                          <option>80</option>
                           <option>81</option>
                            <option>82</option>
                            <option>83</option>
                            <option>84</option>
                            <option>85</option>
                           <option>86</option>
                            <option>87</option>
                            <option>88</option>
                            <option>89</option>
                            <option>90</option>
                           <option>91</option>
                            <option>92</option>
                            <option>93</option>
                            <option>94</option>
                            <option>95</option>
                           <option>96</option>
                            <option>97</option>
                            <option>98</option>
                            <option>99</option>
                            <option>100</option>
                      </select>
                        @error('jantari_winning_amount')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Bahar Winning Amounts(x)</label>
                        <select name="bahar_winning_amount" id="bahar_winning_amount" class="form-control" id="bahar_winning_amount" placeholder="Enter Value" value="{{old('bahar_winning_amount')}}" required>
                        <option>Select value</option>
                          <option>8.0</option>
                           <option>8.1</option>
                            <option>8.2</option>
                            <option>8.3</option>
                            <option>8.4</option>
                            <option>8.5</option>
                           <option>8.6</option>
                            <option>8.7</option>
                            <option>8.8</option>
                            <option>8.9</option>
                            <option>9.0</option>
                           <option>9.1</option>
                            <option>9.2</option>
                            <option>9.3</option>
                            <option>9.4</option>
                            <option>9.5</option>
                           <option>9.6</option>
                            <option>9.7</option>
                            <option>9.8</option>
                            <option>9.9</option>
                            <option>10.0</option>
                      </select>
                        @error('bahar_winning_amount')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Ander Winning Amounts(x)</label>
                        <select name="ander_winning_amount" id="ander_winning_amount" class="form-control" id="ander_winning_amount" placeholder="Enter Value" value="{{old('ander_winning_amount')}}" required>
                        <option>Select value</option>
                          <option value="8.0">8.0</option>
                           <option value="8.1">8.1</option>
                            <option value="8.2">8.2</option>
                            <option value="8.3">8.3</option>
                            <option value="8.4">8.4</option>
                            <option value="8.5">8.5</option>
                           <option value="8.6">8.6</option>
                            <option value="8.7">8.7</option>
                            <option value="8.8">8.8</option>
                            <option value="8.9">8.9</option>
                            <option value="9.0">9.0</option>
                           <option value="9.1">9.1</option>
                            <option value="9.2">9.2</option>
                            <option value="9.3">9.3</option>
                            <option value="9.4">9.4</option>
                            <option value="9.5">9.5</option>
                           <option value="9.6">9.6</option>
                            <option value="9.7">9.7</option>
                            <option value="9.8">9.8</option>
                            <option value="9.9">9.9</option>
                            <option value="10.0">10.0</option>

                      </select>
                        @error('ander_winning_amount')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Company Commission(%)</label>
                        <input name="company_commission" id="company_commission" class="form-control" id="company_commission" placeholder="Enter Value" value="{{old('company_commission')}}" required>
                        @error('company_commission')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Result Time</label>
                        <input type="time" name="result_time" class="form-control"  placeholder="Enter Result Time" value="{{old('result_time')}}" required>
                        @error('result_time')
                              <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea id="description" class="form-control" name="description" Placeholder=" Enter Description" required>{{old('description')}}</textarea>
                          @error('description')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                      </div>
                    </div>
                  </div>

									
									<div class="mt-4">
										<button class="btn btn-primary" type="submit">Submit</button>
										<button class="btn btn-primary" type="reset">Reset</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script>
  //alert()
      //TIMEPICKER
      $('#start_time, #end_time, #result_time').timepicker({
        'timeFormat': 'H:i', // Use 'H' for 24-hour format without AM/PM
        'step': 30,
        'scrollDefault': 'now'
      });

    // Event handler for start time change
      $('#start_time').on('changeTime', function() {
        var startTime = $('#start_time').val();
        var endTime = $('#end_time').val();
        var resultTime = $('#result_time').val();

        // Check if end time or result time is the same or less than start time
        if (endTime <= startTime || resultTime <= startTime) {
          // Set end time to a minute after start time
          var newEndTime = new Date($('#start_time').timepicker('getTime'));
          newEndTime.setMinutes(newEndTime.getMinutes() + 1);
          $('#end_time').timepicker('setTime', newEndTime);

          // Set result time to a minute after start time
          var newResultTime = new Date($('#start_time').timepicker('getTime'));
          newResultTime.setMinutes(newResultTime.getMinutes() + 1);
          $('#result_time').timepicker('setTime', newResultTime);
        }
      });

      // Event handler for end time change
      $('#end_time').on('changeTime', function() {
        var startTime = $('#start_time').val();
        var endTime = $('#end_time').val();
        var resultTime = $('#result_time').val();

          // Check if end time or result time is the same or less than start time
          if (endTime <= startTime || resultTime <= startTime) {
            // Set start time to a minute before end time
            var newStartTime = new Date($('#end_time').timepicker('getTime'));
            newStartTime.setMinutes(newStartTime.getMinutes() - 1);
            $('#start_time').timepicker('setTime', newStartTime);

            // Set result time to a minute after start time
            var newResultTime = new Date($('#start_time').timepicker('getTime'));
            newResultTime.setMinutes(newResultTime.getMinutes() + 1);
            $('#result_time').timepicker('setTime', newResultTime);
          }
        });

        // Event handler for result time change
        $('#result_time').on('changeTime', function() {
          var startTime = $('#start_time').val();
          var endTime = $('#end_time').val();
          var resultTime = $('#result_time').val();

          // Check if result time is the same or less than start time or end time
          if (resultTime <= startTime || resultTime <= endTime) {
            // Set result time to a minute after start time
            var newResultTime = new Date($('#start_time').timepicker('getTime'));
            newResultTime.setMinutes(newResultTime.getMinutes() + 1);
            $('#result_time').timepicker('setTime', newResultTime);
          }
        });
        // Handle Input: Allow only numeric values between 0 and 100 for percentage
        $(document).on('keypress keyup input paste dragover drop', '#company_commission', function(e) {
          var inputValue = $(this).val();

          // Remove leading zeros
          inputValue = inputValue.replace(/^0+/, '');

          // Check if the entered value is a valid number between 0 and 100
          if (isNaN(inputValue) || inputValue < 0 || inputValue > 100) {
            // If not valid, set the value to an empty string
            $(this).val('');
          }
        });

        //Handle Input table only accept numeric value
        $(document).on('keypress keyup input paste dragover drop','#jantari_winning_amount, #bahar_winning_amount, #ander_winning_amount',function(e){
          return e.charCode >= 48 && e.charCode <= 57;
        });


  $("#start_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', minDate);
});

$("#end_date").datepicker({
    format: 'dd M yyyy',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: 'linked',
    todayHighlight: true,
    startDate: new Date()
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', maxDate);
});


  $('#member_logo').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#memberLogoPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      $('#contest_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#contestImagePreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
</script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Manage Settings</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
#myDIV {
  width: 100%;
  text-align: center;
  margin-top: 20px;
}

	.partnergame-area {
    position: relative;
    background: #eff1fe;
     }
     .partnertable-header {
    padding: 20px 0px;
    position: absolute;
    top: 30px;
    width: 100%;
}
 .partnertable-header .gameon {
    background: #fff;
    height: 100px;
    border-radius: 10px;
}
.partnertable-header .gameon h1 {
    text-align: left;
    font-size: 20px;
    padding: 5px 15px;
    font-weight: 500;
}
.partnertable-header .comission-inner {
    background: #fff;
    height: 100px;
    border-radius: 10px;
  }
  .comission-title h2 {
    font-size: 20px;
    font-weight: 500;
    padding: 5px 15px 0px;
    margin: 0px;
}
.comission-title p,input {
    border: 1px solid #000;
    width: 40px;
    height: 30px;
    margin: 10px 0px 0px;
}
.comission-title .col {
    padding: 0px;
    text-align: center;
}
.partnerwinningtable-header {
    background: #fff;
    border-radius: 10px;
    padding: 10px 15px;
    text-align: center;
}
.partnerwinningtable-header h2 {
    font-size: 20px;
    font-weight: 500;
}
.partnerwinningtable-header ul {
    list-style: none;
    padding: 10px 0px;
    margin-bottom: 15px;
}
.partnerwinningtable-header ul li {
    display: inline-block;
    width: 40px;
    height: 30px;
    line-height: 30px;
    border: 1px solid #000;
}
.partnerwinningtable-header a {
    background:#5b68eb;
    color: #fff;
    text-decoration: none;
    padding: 8px 24px;
    border-radius: 8px;
}
.partnerwinningtable-header p {
    border: 1px solid #000;
    padding: 5px;
    margin: 0px;
}
.partnerwinningtable-header tr {
    border: 1px solid #fff;
}
.partnerwinningtable-header td {
    text-align: left;
}
.partnerwinningtable-header th {
    text-align: left;
}
.partnertotalplay-header .partnerplay-box {
    background: #fff;
    margin: 15px 0px;
    border-radius: 10px;
    text-align: center;
    padding: 10px;
}
.partnertotalplay-header .partnerplay-box h2 {
    font-size: 20px;
    font-weight: 500;
}
.partnertotalplay-header .partnerplay-box p {
  color: #000;
  margin: 0px;
}
.partnertotalplay-header .partnertotal-box {
    background: #fff;
    padding: 15px;
    border-radius: 10px;
}
.partnertotalplay-header .partnertotal-box h2 {
    font-size: 20px;
    font-weight: 500;
}
.partnertotalplay-header .partnertotal-box tr {
    border: 1px solid #fff;
}
.gameon {
    background: #fff;
    height: 100px;
    width: 25%;
    z-index: 9;
    position: relative;
    padding: 10px;
    text-align: center;
    border-radius: 10px;
    margin: 20px 0px;
}
.gameon h1 {
    font-size: 24px;
}
	</style>
</head>
           @php
                  $total=0;
                  $admin_comission=0;
                  $closing_amount=0;
                  @endphp
<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Settings</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.partner.sidebar')
    <!-- /Sidebar -->

     <!--Flash Message -->
     @include('layouts.partner.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">
        <div class="container">
            <div class="gameon">
                <h1>Game on/off</h1>
                <button onclick="myFunction()" id="toggleButton" type="button" class="btn btn-info">ON</button>
             </div>
            <div class="hide"  id="myDIV">
            <div class="partnertable-header">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    @php
                    //  $data['contests'] =DB::table('contests')->where('id',48)->first();

            //   dd($data);

                    @endphp

                    <div class="col-md-8" >
                        <div class="comission-inner">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="comission-title">
                                    <div class="row">
                                    <div class="col-8">
                                        <h2>Admin Commission</h2>
                                    </div>
                                    <div class="col">
                                        <h6>{{$game->company_commission ?? ''}}</h6>
                                     {{-- <input type="text" class="" name="admin_commission" id="admin_commission" value="{{$game->admin_commission ?? ''}}" /> --}}
                                    </div>
                                    <div class="col">
                                        <!-- <button type="button" class="btn btn-info">Update</button> -->
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-8">
                                        <h2>Jantari Winning Amounts(x)</h2>
                                    </div>
                                    <div class="col">
                                        <h6>{{$game->jantari_winning_amount ?? ''}}</h6>

                                    {{-- <input type="text" class="" name="jantari_commission" id="jantari_commission" /> --}}
                                    </div>
                                    <div class="col">
                                    <!-- <button type="button" class="btn btn-info">Update</button> -->
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comission-title">
                                    <div class="row">
                                    <div class="col-8">
                                        <h2>Ander Winning Amounts(x)</h2>
                                    </div>
                                    <div class="col">
                                        {{-- <input type="text" class="" name="andar_commission" id="andar_commission"/> --}}
                                        <h6>{{$game->ander_winning_amount ?? ''}}</h6>

                                    </div>
                                    <div class="col">
                                    <!-- <button type="button" class="btn btn-info">Update</button> -->
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-8">
                                        <h2>Bahar Winning Amounts(x)</h2>
                                    </div>
                                    <div class="col">
                                        {{-- <input type="text" class="" name="bahar_commission" id="bahar_commission" /> --}}
                                        <h6>{{$game->bahar_winning_amount ?? ''}}</h6>

                                    </div>
                                    <div class="col">
                                    <!-- <button type="button" class="btn btn-info">Update</button> -->
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="partnerwinningtable-header">
                <div class="row">
                    <div class="col-md-4">
                        <h2>Winning Number</h2>

                            <form action="{{route('partner.DataFetch')}}" method="get">
                            <ul>
                            <input type="text" name="winning_number_1" id="winning_number_1"  minlength="2" maxlength="2" required>
                            <!-- <input type="text" name="winning_number_2" id="winning_number_2" /> -->
                            </ul>
                            <button type="submit">Fatch Data</button>
                            </form>


                    </div>
                    <div class="col-md-8">
                    <table class="table">

        <thead>
            <tr>
                <th>Customer Name</th>
                <th>Mobile no</th>
                <th>Type</th>
                <th>Play no</th>
                <th>Play Amount</th>
                <th>Winning Amount</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($details) && count($details) > 0)

                @foreach($details as $detail)
                @php
                $data=json_decode($detail->user_choise,true);
                //$data['game_number'];
                //var_dump($data['0']['game_number']);
                //die();
                //$amount
              $total+=$data[0]['game_number_amount']  ;
              $admin_comission=$total/10;
              $closing_amount=$total-$admin_comission;

                @endphp

                    <tr>
                        <td>{{ $detail->name }}</td>
                        <td>
                 {{ str_repeat('x', strlen($detail->mobile) - 4) . substr($detail->mobile, -4) }}
                         </td>
                        <td>{{ $detail->game_type }}</td>
                        <td>
                @foreach($data as $item)
                   {{ $item['game_number'] }}<br>
                    @endforeach
                      </td>

                          <td>
                   @foreach($data as $item)
                  {{ $item['game_number_amount'] }}
                  <br>

                  @endforeach
                      </td>

                        <td>
                   @foreach($data as $item)
                          {{  (1)* $item['game_number_amount'] }}<br>
                   @endforeach
                       </td>


                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">No results found.</td>
                </tr>
            @endif
        </tbody>
    </table>



                        <div class="row">
                        <div class="col-8">
                            <p>admin comission ={{$admin_comission}}</p>
                        </div>
                        <div class="col-4">
                            <div class="Winning-btn">
                                <a href="#">Submit</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="partnertotalplay-header">
                <div class="row">
                    <div class="col-md-3">
                        <div class="partnerplay-box">
                        <h2>Total Play</h2>
                        <p>{{$total}}</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="partnerplay-box">
                        <h2>Total Win</h2>
                        <p>{{$closing_amount}}</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="partnerplay-box">
                        <h2>Admin Commision</h2>
                        <p>{{$admin_comission}}</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="partnerplay-box">
                        <h2>Closing Balance</h2>
                        <p>{{$closing_amount}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="partnertotalplay-header">
                <div class="row">
                    <div class="col-md-8">
                        <div class="partnertotal-box">
                        <h2>Grand Total Play</h2>
                        <table class="table caption-top">
                            <thead>
                                <tr>
                                    <th>Game Type</th>
                                    <th>Number</th>
                                    <th>Amount</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Jantari</td>
                                    <td>01</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>02</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>02</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>03</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>04</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>05</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>06</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>07</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>08</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>09</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <!-- <td>Jantari</td> -->
                                    <td>01</td>
                                    <td>10</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>1000</th>
                                    <th>120</th>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script>
$(document).ready(function(){
  // Handle Input: Allow only numeric values between 0 and 100 for percentage
        $(document).on('keypress keyup input paste dragover drop', '#admin_commission', function(e) {
        var inputValue = $(this).val();
          // Remove leading zeros
          inputValue = inputValue.replace(/^0+/, '');
          // Check if the entered value is a valid number between 0 and 100
          if (isNaN(inputValue) || inputValue < 0 || inputValue > 100) {
            // If not valid, set the value to an empty string
            $(this).val('');
          }
        });

        //Handle Input table only accept numeric value
        $(document).on('keypress keyup input paste dragover drop','#andar_commission, #bahar_commission, #jantari_commission',function(e){
          return e.charCode >= 48 && e.charCode <= 57;
        });
        Handle Input table only accept numeric value and also length should be 1
        $(document).on('keypress keyup input paste dragover drop', '#winning_number_1, #winning_number_2', function (e) {
            var enteredValue = this.value.trim();  // Trim any leading or trailing whitespaces
            // Check if the entered value is a single digit
            var isValidInput = /^[0-9]$/.test(enteredValue);

            // If the entered value is not valid or the length is not 1, clear the input
            if (!isValidInput || enteredValue.length !== 1) {
                this.value = '';
            }
        });

});
</script>
<script>
    // Get the button element
    var button = document.getElementById('toggleButton');

    // Set initial state
    var isToggled = false;

    // Add click event listener to toggle the button state
    button.addEventListener('click', function() {
        // Toggle the state
        isToggled = !isToggled;

        // Update the button text based on the state
        if (isToggled) {
            button.textContent = 'ON';
            button.classList.remove('btn-info');
            button.classList.add('btn-success');
        } else {
            button.textContent = 'Off';
            button.classList.remove('btn-success');
            button.classList.add('btn-info');
        }
    });
</script>

<script>
    function myFunction() {
      var x = document.getElementById("myDIV");
      if (x.style.display === "none") {
        x.style.display = "block";
      } else {
        x.style.display = "none";
      }
    }
    </script>


</body>
</html>

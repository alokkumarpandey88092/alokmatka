<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Choose Game</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Choose Your Game</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.partner.sidebar')
    <!-- /Sidebar -->

     <!--Flash Message -->
     @include('layouts.partner.flash_msg')
    <!-- /Flash Message -->
    <div class="page-wrapper">

      <div class="content container-fluid">
        
        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">
              <div class="col-md-6 col-lg-3 col-xl-3">
                <div class="card">
                  <div class="card-body">

                    <div class="pricing-card-price text-center">
                      <h3 class="heading2 price">Satta King</h3>
                    </div>
                    <div class="satta-img">
                      <img src="{{ asset('game/assets/img/satta1.jpg') }}" alt="">
                    </div>
                    <div class="baton text-center">
                      <a href="{{ route('GameList') }}" class="btn btn-info " data-bs-target="#staticBackdrop" d-flex
                        justify-content-center btn-block">play</a>
                    </div>

                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 col-xl-3">
                <div class="card">
                  <div class="card-body">

                    <div class="pricing-card-price text-center">
                      <h3 class="heading2 price">Satta King</h3>
                    </div>
                 <div class="satta-img">
                  <img src="{{ asset('game/assets/img/satta1.jpg') }}" alt="">
                 </div>
                    <div class="baton text-center">
                      <a href="{{ route('GameList') }}" class="btn btn-info " data-bs-target="#staticBackdrop" d-flex
                        justify-content-center btn-block">play</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 col-xl-3">
                <div class="card">
                  <div class="card-body">

                    <div class="pricing-card-price text-center">
                      <h3 class="heading2 price">Colour Box</h3>
                    </div>
                    <div class="colour-img">
                      <img src="{{ asset('game/assets/img/satta.jpg') }}" alt="">
                    </div>
                   
                    <div class="baton text-center">
                      <a href="{{ route('GameList') }}" class="btn btn-info " d-flex justify-content-center btn-block">play</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 col-xl-3">
                <div class="card">
                  <div class="card-body">

                    <div class="pricing-card-price text-center">
                      <h3 class="heading2 price">Colour Box</h3>
                    </div>
                    <div class="colour-img">
                      <img src="{{ asset('game/assets/img/satta.jpg') }}" alt="">
                    </div>
                  
                    <div class="baton text-center">
                      <a href="{{ route('GameList') }}" class="btn btn-info " d-flex justify-content-center btn-block">play</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>


        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h5 class="modal-title" id="staticBackdropLabel">Select Game</h5>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Jodi Play</h2>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Crossing</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>From-to</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="game">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>ander-bahar haruf</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script>
    var myModal = document.getElementById("myModal");
    var myInput = document.getElementById("myInput");

    myModal.addEventListener("shown.bs.modal", function () {
      myInput.focus();
    });
  </script>
  

<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
</body>

</html>
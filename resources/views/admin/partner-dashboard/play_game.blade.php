<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Play Game</title>

  <!-- Favicons -->
  <link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">

</head>

<body>
  <div class="main-wrapper">
    <!-- Header -->
    <div class="header">
      <div class="header-left">
      </div>
      <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <a href="javascript:void(0);" id="toggle_btn">
        <h3>{{@$data  ? $data->contest_name : ''}}</h3>
        <h6> {{ date(SIMPLE_DATE,strtotime(now()))}}</h6>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
        <h3>{{@$data  ? $data->contest_name : ''}}</h3>
      </a>

      <ul class="nav user-menu">
        <!--  -->
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <div class="timer">
              <i class="fas fa-clock" id="remainingTime"></i>
            </div>
            <input type="hidden" value="{{@$data ? $data->start_end_time : ''}}" id="game_start_time"/>
            <input type="hidden" value="{{@$data ? $data->end_time : ''}}" id="game_end_time"/>
            <div class="pro">
              <a href="javascript:void(0)"> <i class="fas fa-wallet"></i> (₹) {{@$user_wallet ? $user_wallet->total_amount : 0 }} </a>
            </div>

          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

     <!--Sidebar -->
     @include('layouts.partner.sidebar')
    <!-- /Sidebar -->

    <!--Flash Message -->
      @include('layouts.partner.flash_msg')
  <!-- /Flash Message -->

    <div class="page-wrapper">
      <div class="row repit">
      </div>
      <div class="row">
        <div class="col-xl-12">
          <div class="tabs-content">
            <div role="tabpanel" class="tab-pane mt-4 page-calendar active" id="schedule">
              <div class="col-xl-12 col-12 col-md-12 into">
                <div class="how">


                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop" d-flex
                    justify-content-center btn-block">Repeat Game</a>
                  <a href="" class="btn " data-bs-toggle="modal" data-bs-target="#staticBackdrop6" d-flex
                    justify-content-center btn-block">From Into</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop2" d-flex
                    justify-content-center btn-block">Open Play</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop3" d-flex
                    justify-content-center btn-block">Crossing Play</a>
                  <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#staticBackdrop4" d-flex
                    justify-content-center btn-block">Mesage Paste</a>
                <a href="" class="btn  " data-bs-toggle="modal" data-bs-target="#" d-flex
                    justify-content-center btn-block">Palti Play</a>
                </div>
              </div>
              <div class="row g-3">
                <div class="col-md-12 col-lg-6 from">
                  <div class="table-box">
                    <section>
                      <div class="container-fluid">
                          @php
                          $num = 1;
                          @endphp
                        <form method="post" action="{{ route('user.playGame.store') }}" id="frm_play_game">
                          @csrf
                          <input type="hidden" name="game_type" id="game_type" value="normal">
                          <input type="hidden" name="game_id" value="{{ @$id }}">
                          <input type="hidden" name="user_wallet_amount" id="user_wallet_amount" value="{{ @$user_wallet ? $user_wallet->total_amount : 0 }}">
                        <div class="a111">
                          <h3>Jantari</h3>
                          @for ($i = 1; $i <= 10; $i++)
                          <div class="row raw1">
                          @for ($j = 1; $j <= 10; $j++)
                            <div class="col-lg-1">
                              <div class="one">
                                <p><?php
                                  if ($num<10) {
                                    echo '0'.$num;
                                  }else if($num > 9 && $num < 100){
                                    echo $num;
                                  }elseif ($num == 100) {
                                    echo '00';
                                  }
                                  ?></p>
                                  <input type="hidden" name="game_number[]" value="<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>">
                                  <input type="text" data-value="<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>" id="game_number_amount_<?php if ($num<10) { echo '0'.$num; }else if($num > 9 && $num < 100){ echo $num; }elseif ($num == 100) { echo '00';} ?>" name="game_number_amount[]" class="in game_number_amount"/>
                              </div>
                              <div class="b1"></div>
                            </div>
                            @php
                              $num++;
                              @endphp
                            @endfor
                          </div>
                          @endfor
                          </form>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="col-md-12 col-lg-4 repit">
                  <div class="body mb-3">
                    <div class="event-name b-lightred row">
                      <div class="col-12 andar">
                        <h4>Andar Haruf</h4>
                      </div>
                      <section>
                        <div class="container-fluid">
                          <div class="row side">
                            <div class="col-lg-1">
                              <div class="ones">
                                <p>1</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b1"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="twos">
                                <p>2</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b2"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="threes">
                                <p>3</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b3"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="fours">
                                <p>4</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b4"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="fives">
                                <p>5</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b5"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="sixs">
                                <p>6</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b6"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="sevens">
                                <p>7</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b7"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="eights">
                                <p>8</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b8"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="nines">
                                <p>9</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b9"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="tens">
                                <p>0</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b10"></div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                    <div class="event-name b-greensea row">
                      <div class="col-12 bahar">
                        <h4>Bahar Haruf</h4>
                      </div>
                      <section>
                        <div class="container-fluid">
                          <div class="row side">
                            <div class="col-lg-1">
                              <div class="ones">
                                <p>1</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b1"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="twos">
                                <p>2</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b2"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="threes">
                                <p>3</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b3"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="fours">
                                <p>4</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b4"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="fives">
                                <p>5</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b5"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="sixs">
                                <p>6</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b6"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="sevens">
                                <p>7</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b7"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="eights">
                                <p>8</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b8"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="nines">
                                <p>9</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b9"></div>
                            </div>
                            <div class="col-lg-1">
                              <div class="tens">
                                <p>0</p>
                                <input class="inner" type="text" />
                              </div>
                              <div class="b10"></div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4 meet">
                      <!-- <a href="javascipt:void(0)" id="refreshButton" class="btn btn-primary  botn btn-lg disabled" tabindex="-1" role="button"
                        aria-disabled="true">Refresh</a> -->
                        <button type="button" class="" id="refreshButton">Refresh</button>
                    </div>
                    <div class="col-lg-4 total">
                      <p>Total: <span id="total_amount">0</span> </p>
                    </div>
                    <div class="col-lg-4 nice">
                      <!-- <a href="#" class="btn btn-primary botn btn-lg disabled" data-bs-toggle="modal"
                        data-bs-target="#exampleModal" tabindex="-1" role="button" aria-disabled="true">Calculate</a> -->
                        <button type="button" class="" id="btn_submit_game">Submit</button>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- pop up  -->
    <!-- Button trigger modal -->


    <!-- Modal -->
    <!-- Modal -->
    <!-- Modal -->

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  " style="width:100% ">
        <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-5 paste">

              <h4>Today History</h4>
              <table style="width:100% ">
              </table>
              <p></p>

            </div>
            <div class="col-lg-5 repeat">
              <table style="width:100% ">

              </table>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0)" onclick="getAutoFillPreviousGameHistory();" data-bs-dismiss="modal"> Submit Game </a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
  <!-- one popup -->
  <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop6" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabe6">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 form">
              <div class="row">
                <div class="col-lg-5">
                  <input type="text " class="left" placeholder="From">
                </div>
                <div class="col-lg-5">
                  <input type="text" class="right" placeholder="To">
                </div>
              </div>
              <i class="fas "></i>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <input type="text" class="amount" placeholder="Amount">
                  </div>

                </div>
              </div>
              <div class="row">
                <div class="col-lg-10 go">
                  <a href="javascript:void(0)" onclick="GoByFromInTo();">Go Submit</a>
                </div>
              </div>
            </div>
            <div class="col-lg-6 repeatt">
              <table style="width:100% ">
              </table>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0)" data-bs-dismiss="modal" onclick="ShowAfterFromToSelection();"> Submit Game </a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div> <!--one popup -->
  <!-- Third popup  -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style="width:100%">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel2" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel2">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 form">
              <div class="row">
                <div class="col-lg-12">
                  <input type="text " class="leftt" placeholder="From">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <input type="text" class="amountt" placeholder="Amount">
                  </div>

                </div>
              </div>
              <div class="row mt-5">
                <div class="col-lg-10 go">
                  <a href="javascript:void(0)" onclick="GoByOpenPlay();">Go Submit</a>
                </div>
              </div>

            </div>
            <div class="col-lg-6 repeatt">
              <table style="width:100% ">

              </table>
              <div class="row repeat-sec">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0);" data-bs-dismiss="modal" onclick="ShowAfterOpenPlaySelection();"> Submit Game </a>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


  <!-- second popup end -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop3" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel3">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 hows">
             <div class="row">
              <div class="col-lg-12 cap">
                <div class="row side1">
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="kuyui">
                    </div>
                    <div class="b1"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="6a99m">
                    </div>
                    <div class="b2"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="8msvhf">
                    </div>
                    <div class="b3"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="pybf5y">
                    </div>
                    <div class="b4"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="fir0l">
                    </div>
                    <div class="b5"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="1esl0r">
                    </div>
                    <div class="b6"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="xoq9os">
                    </div>
                    <div class="b7"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="kcaobm">
                    </div>
                    <div class="b8"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="jp5fgn">
                    </div>
                    <div class="b9"></div>
                  </div>
                  <div class="col-lg-1">
                    <div class="ones">

                      <input class="inner2" type="text" fdprocessedid="j7c3s">
                    </div>
                    <div class="b10"></div>
                  </div>
                </div>
              </div>
             </div>
             <div class="row">
              <div class="col-lg-12">
                <input type="text" class="ama" placeholder="Amount">
              </div>
             </div>
             <div class="row">
              <div class="col-lg-6">
                <div class="jodie">
                  <a href="javascript:void(0)" onclick="generateJodi();">With Jodie</a>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="without">
                  <a href="javascript:void(0)" onclick="generateWithoutJodi();">Without Jodie</a>
                </div>
              </div>
             </div>
            </div>
            <div class="col-lg-6 repeatts">
              <table style="width:100% ">
              </table>
              <div class="row repeat-secc">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="price">
                        <i class="fas fa-rupee-sign">00.00</i>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="today-btn">
                        <a href="javascript:void(0)" data-bs-dismiss="modal" onclick="ShowAfterCrossingPlaySelection();">Submit Game</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


  <!-- third popup end -->
  <!-- four popup start -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  " style="width:100% ">
      <div class="modal-content style=" width:100% ">
      <div class=" modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade" id="staticBackdrop4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel4" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header justify-content-center">
          <h2 class="modal-title" id="staticBackdropLabel4">{{@$data  ? $data->contest_name : ''}}</h2>

        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6 metal-row">
              <div class="row">
                <div class="col-lg-12">
                  <input type="text" class="metal" placeholder="Enter Value">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 ex">
                  <h6>example1</h6>
                  <h6>example2</h6>
                  <h6>example3</h6>
                  <h6>example4</h6>
                  <h6>example5</h6>
                </div>
              </div>

            </div>
            <div class="col-lg-6 ">
              <div class="col-lg-6 repeattt">
                <table style="width:100% ">

                  <tr>
                    <td>11</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>12</td>
                    <td>=</td>
                    <td>10</td>
                  <tr>
                    <td>13</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>14</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <td>15</td>
                    <td>=</td>
                    <td>10</td>
                  </tr>

                </table>
                <div class="row repeat-secc">
                  <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="price">
                          <i class="fas fa-rupee-sign">   90</i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="today-btn">
                          <a href=""> Submit Game </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>




  <!-- pop up end -->

  <!-- jQuery -->
  <script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
   <!-- Include SweetAlert CDN -->
   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script>
  //$(document).ready(function () {
        var isGameOver = false;
        // Assuming start_time and end_time are coming from the database
        var startTime = document.getElementById("game_start_time").value;
        var endTime = document.getElementById("game_end_time").value;

         // Update remaining time every second
        var intervalId = setInterval(updateRemainingTime, 1000);

        function updateRemainingTime() {
            var now = new Date();
            var startDate = new Date(now.toDateString() + " " + startTime);
            var endDate = new Date(now.toDateString() + " " + endTime);

            // Calculate remaining time in milliseconds
            var remainingTime = endDate - now;
            //console.log(remainingTime)

            if (remainingTime > 0) {
              isGameOver = true;
                // Convert remaining time to hours, minutes, and seconds
                var hours = Math.floor(remainingTime / (60 * 60 * 1000));
                var minutes = Math.floor((remainingTime % (60 * 60 * 1000)) / (60 * 1000));
                var seconds = Math.floor((remainingTime % (60 * 1000)) / 1000);
                // Format the remaining time
                var formattedTime =
                    padZero(hours) + "h " + padZero(minutes) + "m " + padZero(seconds) + "s";

                // Display remaining time
                document.getElementById("remainingTime").innerHTML = formattedTime
            } else {
                document.getElementById("remainingTime").innerHTML = "Event ended";
                isGameOver = false;
                clearInterval(intervalId);
                 // Show SweetAlert
                 Swal.fire({
                   title: "Game Over",
                   text: "Your game has over!",
                   icon: "info",
                   confirmButtonText: "OK",
                   allowOutsideClick: false,
                   allowEscapeKey: false,
                   allowEnterKey: false,
                   showCloseButton: false
               }).then((result) => {
                   // If the user clicks "OK", redirect to another route
                   if (result.isConfirmed) {
                       window.location.href = "{{ route('GameList') }}"; // Replace with your desired route
                       hasRedirected = true; // Set the flag to true after redirection
                   }
               });
            }
        }

        // Function to pad a number with zero if it's less than 10
        function padZero(number) {
            return (number < 10 ? "0" : "") + number;
        }

        // Initial update
        updateRemainingTime();

      // Call when Page has load for showing data in modal
      getPreviousGameHistory();
    function getPreviousGameHistory(){
          // Make an AJAX request when the page loads
        $.ajax({
          url:"{{ url('user/previous-game') }}",
          type: 'GET',
          dataType: 'json',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Use the CSRF token in headers
          },
          success: function (data) {
              var userChoices = JSON.parse(data.user_choise);
              // Initialize a variable to store the total amount
              var totalAmount = 0;
              var parentElement = $('#staticBackdrop');
              // Iterate through the userChoices array using $.each
              $.each(userChoices, function(index, choice) {
                  // Append rows to the first table (index 0)
                  $('table', parentElement).eq(0).append('<tr><td>' + choice.game_number + '</td><td> = </td><td>' + choice.game_number_amount + '</td></tr>');

                  // Append rows to the second table (index 1)
                  $('table', parentElement).eq(1).append('<tr><td>' + choice.game_number + '</td><td> = </td><td>' + choice.game_number_amount + '</td></tr>');
                  // Update the total amount
                    totalAmount += parseFloat(choice.game_number_amount);
              });
              // Update the content of the price div with the total amount
              $('.price i', parentElement).text('   ' + totalAmount.toFixed(2));

          },
          error: function (error) {
              console.error('Error:', error);
          }
      });
    }


    //refresh all value
    $('#refreshButton').on('click', function () {
        $('.game_number_amount').each(function () {
            $(this).val('');
        });
        $('#total_amount').text('00.00');
    });

    //Handle Input table only accept numeric value
    $(document).on('keypress keyup input paste dragover drop','.game_number_amount, .left, .right, .amount, .leftt, .amountt, .ama',function(e){
      return e.charCode >= 48 && e.charCode <= 57;
    });

    //Handle Input table only accept numeric value and also length should be 1
    $(document).on('input','.inner2',function(e){
        //return e.charCode >= 48 && e.charCode <= 57 && this.value.length<1;
        var enteredValue = this.value;

          // Check if the entered value is a single digit
          var isValidInput = /^[0-9]$/.test(enteredValue);

          // Check if the entered value is unique among other input values
          var isUnique = $('.inner2').filter(function () {
            return this.value === enteredValue;
          }).length === 1;

          // If the entered value is valid and unique, allow it, otherwise prevent the input
          if (!isValidInput || !isUnique) {
            this.value = ''; // Clear the input value if it's not valid
            e.preventDefault();
            return false;
          }
    });


    function getAutoFillPreviousGameHistory(){
          // Make an AJAX request when the page loads
        $.ajax({
          url:"{{ url('user/previous-game') }}",
          type: 'GET',
          dataType: 'json',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Use the CSRF token in headers
          },
          success: function (data) {
              var userChoices = JSON.parse(data.user_choise);
              var totalAmount = 0;
              var num = 1;
              // Iterate through the userChoices array using $.each
              $.each(userChoices, function(index, choice) {
                // Update the total amount
                 totalAmount += parseFloat(choice.game_number_amount);
                  // Assuming your input fields have IDs like 'game_number_amount_1', 'game_number_amount_2', etc.
                  //var paddedNum = ("00" + num).slice(-2);
                  var paddedNum = ("00" + parseInt(choice.game_number)).slice(-2);
                  var inputField = $('#game_number_amount_' + paddedNum);

                  // Check if the input field exists before setting its value
                  if (inputField.length) {
                      inputField.val(choice.game_number_amount);
                  }

                  num++;
              });
              // Update the content of the price div with the total amount
              $('#total_amount').text(totalAmount.toFixed(2));
              $('#game_type').val('game_repeat');

          },
          error: function (error) {
              console.error('Error:', error);
          }
      });
    }

    //GoByFromInTo
    function GoByFromInTo(){
      var fromValue =$('#staticBackdrop6 .left').val();
      var toValue = $('#staticBackdrop6 .right').val();
      var amountValue =$('#staticBackdrop6 .amount').val();
      var total_amount_for_show = 0;
      // Check if the values are in the range 01 to 99
      if (/^[0-9]{2}$/.test(fromValue) && /^[0-9]{2}$/.test(toValue)) {
        // Check if "from" is not greater than "to"
        if (parseInt(fromValue, 10) <= parseInt(toValue, 10) || (toValue === "00" && parseInt(fromValue, 10) < 100)) {
          $("#staticBackdrop6 table").empty();
              total_amount_for_show = 0;
              for (var i = parseInt(fromValue, 10); i <= parseInt(toValue, 10); i++) {
                var paddedNumber = ("0" + i).slice(-2);
                $("#staticBackdrop6 table").append(`<tr><td>${paddedNumber}</td><td>=</td><td>${amountValue}</td></tr>`);
                total_amount_for_show += parseInt(amountValue, 10);
              }
              $("#staticBackdrop6 .price i").text(total_amount_for_show);
          //alert("Values are within the range.");
        } else {
          alert("'From' should not be greater than 'To'.");
        }
      } else {
        alert("Values should be in the range of 01 to 99.");
      }
    }

    //ShowAfterFromToSelection
    function ShowAfterFromToSelection(){
      $('#refreshButton').trigger('click');
      var fromValue =$('#staticBackdrop6 .left').val();
      var toValue = $('#staticBackdrop6 .right').val();
      var amountValue =$('#staticBackdrop6 .amount').val();
      var total_amount_for_show = 0;
          for (var i = parseInt(fromValue, 10); i <= parseInt(toValue, 10); i++) {
            var paddedNumber = ("0" + i).slice(-2);
            total_amount_for_show += parseInt(amountValue, 10);
            var inputField = $('#game_number_amount_' + paddedNumber);
              // Check if the input field exists before setting its value
              if (inputField.length) {
                  inputField.val(parseInt(amountValue, 10));
              }
          }
            // Update the content of the price div with the total amount
            $('#total_amount').text(total_amount_for_show.toFixed(2));
            $('#game_type').val('game_from_to');
    }

    //GoByOpenPlay
    function GoByOpenPlay(){
      $("#staticBackdrop2 table").empty();
      var fromValue =$('#staticBackdrop2 .leftt').val();
      var amountValue =$('#staticBackdrop2 .amountt').val();
      var total_amount_for_show = 0;
        var numberString = fromValue.toString();
        var pairs = [];
        // Loop through the string in steps of 2
        for (var i = 0; i < numberString.length; i += 2) {
          // Take pairs of two digits and add to the array
          var pair = numberString.substr(i, 2);
          // Check if the pair has two digits
          //if (pair.length === 2) {
          if (pair.length === 2 && pairs.indexOf(pair) === -1) {
            pairs.push(pair);
            $("#staticBackdrop2 table").append(`<tr><td>${pair}</td><td>=</td><td>${amountValue}</td></tr>`);
            total_amount_for_show += parseInt(amountValue, 10);
          }
        }
        $("#staticBackdrop2 .price i").text(total_amount_for_show);
    }

    //ShowAfterFromToSelection
    function ShowAfterOpenPlaySelection(){
      $('#refreshButton').trigger('click');
      var total_amount_for_show = 0;
      $('#staticBackdrop2 table tr').each(function() {
      var firstTdText = $(this).find('td:first').text();
      var thirdTdText = $(this).find('td:eq(2)').text();
      console.log(firstTdText,thirdTdText)
      if (firstTdText.trim() !== '') {
        var inputField = $('#game_number_amount_' + firstTdText);
          if (inputField.length) {
              inputField.val(parseInt(thirdTdText, 10));
              total_amount_for_show += parseInt(thirdTdText, 10);
          }
      }
    });
      $('#total_amount').text(total_amount_for_show.toFixed(2));
      $('#game_type').val('game_open_play');
    }

    //generateWithoutJodi
    function generateWithoutJodi() {
        var number = "";
        $('.inner2').each(function() {
        var inputValue = $(this).val();
        // Remove spaces from the input value
        var cleanedValue = inputValue.replace(/\s/g, '');

        // Concatenate cleaned value to the result string
          number += cleanedValue;
      });
      var total_amount_for_show = 0;
      var amountValue =$('#staticBackdrop3 .ama').val();
        if(amountValue==''){
          alert('Please enter amount')
          return false;
        }
      $("#staticBackdrop3 table").empty();
        var result = [];
        var digits = number.toString().split('');

        for (var i = 0; i < digits.length; i++) {
          for (var j = 0; j < digits.length; j++) {
            //if (i !== j) {
              result.push(digits[i] + digits[j]);
              $("#staticBackdrop3 table").append(`<tr><td>${digits[i] + digits[j]}</td><td>=</td><td>${amountValue}</td></tr>`);
              total_amount_for_show += parseInt(amountValue, 10);
           //}
          }
        }
        $("#staticBackdrop3 .price i").text(total_amount_for_show);
        $("#game_type").val('game_without_jodi');
     }

    //generateJodi
    function generateJodi() {
        var number = "";
        $('.inner2').each(function() {
          var inputValue = $(this).val();
          // Remove spaces from the input value
          var cleanedValue = inputValue.replace(/\s/g, '');

          // Concatenate cleaned value to the result string
          number += cleanedValue;
        });

        var total_amount_for_show = 0;
        var amountValue = $('#staticBackdrop3 .ama').val();
        if (amountValue === '') {
          alert('Please enter amount');
          return false;
        }

        $("#staticBackdrop3 table").empty();
        var result = [];

        for (var i = 0; i < number.length; i++) {
          // Add the jodi for the current digit
          result.push(number[i] + number[i]);
          $("#staticBackdrop3 table").append(`<tr><td>${number[i] + number[i]}</td><td>=</td><td>${amountValue}</td></tr>`);
          total_amount_for_show += parseInt(amountValue, 10);
        }
        $("#staticBackdrop3 .price i").text(total_amount_for_show);
        $("#game_type").val('game_with_jodi');
      }

      //ShowAfterCrossingPlaySelection
    function ShowAfterCrossingPlaySelection(){
      $('#refreshButton').trigger('click');
      var total_amount_for_show = 0;
      $('#staticBackdrop3 table tr').each(function() {
      var firstTdText = $(this).find('td:first').text();
      var thirdTdText = $(this).find('td:eq(2)').text();
      //console.log(firstTdText,thirdTdText)
      if (firstTdText.trim() !== '') {
        var inputField = $('#game_number_amount_' + firstTdText);
          if (inputField.length) {
              inputField.val(parseInt(thirdTdText, 10));
              total_amount_for_show += parseInt(thirdTdText, 10);
          }
      }
    });
      $('#total_amount').text(total_amount_for_show.toFixed(2));
    }

    $(document).ready(function(){
    var total_amount = 0;
    var user_wallet_amount = parseFloat($('#user_wallet_amount').val());

    //trigger keyup onchange amount
    $('.game_number_amount').on('keypress keyup input paste',function(){
      total_amount = 0;
      $('.game_number_amount').each(function(){
         var amount = parseFloat($(this).val());
         if (!isNaN(amount)) {
          total_amount += amount;
         }
      });
      $('#total_amount').text(total_amount.toFixed(2));
    });

    //start trigger keyup event onload page
    $('.game_number_amount').trigger('keypress');
    //end trigger keyup event onload page

    $('#btn_submit_game').click(function(){
      $('.game_number_amount').trigger('keypress');
      // Check if the balance is sufficient
      if (parseInt($('#total_amount').text()) == 0) {
         alert("Please Select Minimum one number and amount can'nt be 0");
         return false;
        }
    if (user_wallet_amount >= total_amount) {
          $('#frm_play_game').submit();
        } else {
          alert('Insufficient balance. Please add Ballance in your wallets.');
        }
    });
  });
  </script>

<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>

</body>

</html>

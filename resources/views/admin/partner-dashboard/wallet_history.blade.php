<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />
  <title>Game History</title>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<!-- <link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}"> -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin1.css') }}">
	<!-- Datatables CSS -->
    <link rel="stylesheet" href="{{ asset('game/assets/plugins/datatables/datatables.min.css') }}">
</head>

<body>
  <div class="main-wrapper">
    <div class="header">
      <div class="header-left">
      </div>
       <a href="javascript:void(0);" id="toggle_btn">
         <i class="fas fa-align-left"></i>
      </a>
      <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
         <i class="fas fa-align-left"></i>
      </a>
      <h3>Wallet History</h3>

      <ul class="nav user-menu">
        <li class="nav-item dropdown noti-dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
          </a>
        </li>
        <li class="nav-item dropdown">
          <a href="javascript:void(0)" class="dropdown-toggle user-link nav-link" data-bs-toggle="dropdown">
            <h4>Profile</h4>
          </a>
        </li>

      </ul>
    </div>
    <!-- /Header -->
    <!-- /Header -->
    <!-- /Header -->

    <!--Sidebar -->
    @include('layouts.partner.sidebar')
    <!-- /Sidebar -->
     <!--Flash Message -->
     @include('layouts.partner.flash_msg')
    <!-- /Flash Message -->

    <div class="page-wrapper">

      <div class="content container-fluid">
        
        <div class="row pricing-box">
          <div class="col-xl-12">
            <div class="row">

            <div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-hover table-center mb-0 datatable">
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>Transaction Id</th>
												<th>Amount</th>
												<th>Transaction Type</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($wallet_history as $k => $v)
											<tr>
												<td>{{++$k}}</td>
												<td>{{ $v->created_at ? date(SIMPLE_DATE,strtotime($v->created_at)) : ''}}</td>
												<td>{{ $v->transaction_id ? $v->transaction_id : ''}}</td>
												<td> &#8377; {{ $v->wallet_amount ? $v->wallet_amount : ''}}</td>
												<td>{{ $v->type ? $v->type : ''}}</td>
                                                <td>{{ $v->pay_status ? $v->pay_status : ''}}</td>
											</tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

            </div>

          </div>


        </div>

      </div>
    </div>
  </div>

    <!-- jQuery -->
<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>
  
<!-- Datatables JS -->
<script src="{{ asset('game/assets/plugins/datatables/datatables.min.js') }}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('game/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('game/assets/js/admin.js') }}"></script>
</body>

</html>
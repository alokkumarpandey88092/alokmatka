@extends('layouts.app_new')
@section('title','Matka || Game Management')
@section('header_title','Game')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}"/>
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title">Contest</h3> -->
                @can('role-create')
                <a href="{{ route('contest.create') }}" type="button" class="btn btn-info float-right">Create New Game</a>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_contest_list" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Game Name</th>
                    <th>Game Play Time</th>
                    <th>Activity</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->contest_name ? $v->contest_name : ''}}</td>
                    <td>
                    <label class="badge badge-success">{{$v->start_end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->start_end_time)) : ''}}</label> - 
                    <label class="badge badge-success">{{$v->end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->end_time)) : ''}}</label>
                    </td>
                    <td>
                      <button data-id="{{ route('user.playGame', $v->id) }}" onclick="setAction(this);" type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Play</button>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select Game</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" id="frm_game_type" data-parsley-validate>
      <div class="modal-body">
              <select class="select2" name="game_type" data-placeholder="Select a game" style="width: 100%;" required id="game_type">
                    <option value="">Select Game</option>
                    <option value="jodi_play">JODI PLAY</option>
                    <option value="crossing" disabled>CROSSING</option>
                    <option value="from_to">FROM-TO</option>
                    <option value="ander_bahar_haruf" disabled>ANDER-BAHAR HARUF</option>
               </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn_game_type">Go</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/daterangepicker.js') }}"></script>
<!-- Include Parsley.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
$(document).ready(function() {
  $('.select2').select2();
  $(function () {
    $("#tbl_contest_list").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  });
  function setAction(id){
    var url = $(id).attr('data-id');
    $('#frm_game_type').attr('action',url);
  }

  // Event handler for form submission
  $('#btn_game_type').on('click', function () {
        if ($('#frm_game_type').parsley().isValid()) {
          var url = $('#frm_game_type').attr('action');
          var option = $('#game_type').val();
          window.location.href = url+"/"+option;
        }
    });
    
</script>
@endpush

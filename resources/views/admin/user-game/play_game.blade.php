@extends('layouts.app_new')
@section('title','Matka || Play Game')
@section('header_title','Play Game')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}"/>
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-md-4">
                      <h1 class="card-title"><b>Wallet Amount:-</b> </h1> &nbsp;&nbsp;
                      <a href="javascript:void(0)" type="button" class="btn btn-success btn-lg">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-currency-rupee" viewBox="0 0 16 16">
                          <path d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4v1.06Z"/>
                        </svg>
                      {{@$user_wallet ? number_format($user_wallet->total_amount,2) : 00.00}} 
                    </a>
                     </div>
                      <div class="col-md-4">
                      <h1 class="card-title"><b>Game Name:- </b> {{@$data ? @$data->contest_name : ''}} </h1>
                      </div>
                      <div class="col-md-4"></div>
                  </div>
                </div>
                
             
              <!-- /.card-header -->
                <!-- /.card-header -->
                  <div class="card-body">
                      <div class="row">
                      @for ($i = 1; $i <= 100; $i++)
                       <div class="col-1 col-sm-1">
                        <input type="checkbox" value="{{$i}}" class="game_number" name="game_number[]">&nbsp;{{ $i }}
                      </div>
                      @endfor
                    </div>

                  </div>
            </div>

                <div class="card card-primary">
                    <form method="post" action="{{ route('user.playGame.store') }}" data-parsley-validate id="frm_play_game">
                    @csrf
                    <input type="hidden" name="game_id" value="{{ @$id }}">
                    <input type="hidden" name="user_wallet_amount" id="user_wallet_amount" value="{{ @$user_wallet ? $user_wallet->total_amount : 0 }}">
                    <div class="card-body"> 
                      <div id="newinput" class="row">
                      </div>
                    </div>
                    <div class="card-footer" id="btn_submit">
                    </div>
                </form>
                </div>

          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/daterangepicker.js') }}"></script>
<!-- Include Parsley.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
$(document).ready(function() {
    $(document).on('change','.game_number', function(){
        $('#frm_play_game').parsley();
        var number = $(this).val();
        var newRowAdd = `<div id="row_game_number_${number}" class="col-3 col-sm-3">
                            <div class="input-group m-3">
                                <div class="input-group-prepend">
                                <button class="btn btn-success" type="button">
                                    <i class="bi bi-award-fill"></i>Choice No:- ${number}</button> 
                                </div>
                                <input type="hidden" name="game_number[]" value="${number}">
                                <input type="text" id="game_number_amount" name="game_number_amount[]" class="form-control game_number_amount" data-parsley-type="digits" data-parsley-required Placeholder="Enter Amount" required>
                            </div> 
                        </div>`;
        if ($(this).is(':checked')) {
            $('#newinput').append(newRowAdd);
        } else {
            $(`#row_game_number_${number}`).remove();
        }
        var checkedCount = $('input.game_number:checked').length;
        if(checkedCount > 0){
            $('#btn_submit').html(`<button type="button" class="btn btn-primary" id="btn_game_play_submit">Submit</button>`);
        }else{
            $('#btn_submit').empty();
        }      
    });

    // Event handler for form submission
   $('#frm_play_game').on('submit', function () {
        if ($(this).parsley().isValid()) {
          document.getElementById('spinner-overlay').style.display = 'block';
        }
    });


    //make logic for check if user has available ballance or not
    var amount = 0;
    $(document).on('click','#btn_game_play_submit', function(){
      amount = 0;
      var user_wallet_amount = parseFloat($('#user_wallet_amount').val());
      $('.game_number_amount').each(function(){
        var number = parseFloat(this.value);
        if (!isNaN(number)) {
          amount += number;
        }
      });
      // Check if the balance is sufficient
        if (user_wallet_amount >= amount) {
          // Submit the form
          $('#frm_play_game').submit(); // Replace 'yourFormId' with the actual ID of your form
        } else {
          // Display an error message or take appropriate action
          alert('Insufficient balance. Please add Ballance in your wallets.');
        }
              
    });

  });
</script>
@endpush

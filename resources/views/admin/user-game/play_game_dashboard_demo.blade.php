@extends('layouts.app_new')
@section('title','Matka || Play Game')
@section('header_title','Play Game')
@push('css')
<link rel="stylesheet" href="{{ asset('bs5/dist/assets/css/fullcalendar.css') }}">
<link rel="stylesheet" href="{{ asset('bs5/dist/assets/plugins/morrisjs/morris.css') }}" />

<!-- Custom Css -->
<link rel="stylesheet" href="{{ asset('bs5/dist/assets/css/main.css') }}">
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
      <div class="card">
                    <div class="body bg-dark profile-header">
                        <div class="row">
                            <div class="col-lg-10 col-md-12 position-relative">
                                <!-- <img src="assets/images/profile_av.jpg" class="user_pic rounded img-raised" alt="User"> -->
                                <div class="detail">
                                    <div class="u_name ms-3">
                                        <h4 class="mb-0 mt-0"><strong>Ghaziabad</strong></h4>
                                        <span>Thursday, 28 jan 2021</span>
                                    </div>
                                    <div id="m_area_chart"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 user_earnings text-center">
                                <h6>Total Earnings</h6>
                                <h4 class="mt-0">$<small class="number count-to" data-from="0" data-to="2124" data-speed="1500" data-fresh-interval="1000">2124</small></h4>
                                <!-- <input type="text" class="knob" value="39" data-width="80" data-height="80" data-thickness="0.1" data-bgcolor="#485058" data-fgColor="#f97c53"> -->
                                <!-- <span class="d-block">Average 39% <i class="zmdi zmdi-caret-up text-success"></i></span> -->
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs profile_tab mb-3 p-0 text-start">
                        <li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#schedule" "="">Repit Game</a></li>
                        <li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#schedule">Crossing Play</a></li>
                        <li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#schedule" "="">From Into</a></li>
                        <li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#schedule" "="">Message Paste</a></li>
                    </ul>
                </div>
      </div>
</section>

<!-- Default Size -->
<div class="modal fade" id="addevent" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Add Event</h4>
            </div>
            <div class="modal-body">
                <div class="form-group mb-2">
                    <div class="form-line">
                        <input type="number" class="form-control" placeholder="Event Date">
                    </div>
                </div>
                <div class="form-group mb-2">
                    <div class="form-line">
                        <input type="text" class="form-control" placeholder="Event Title">
                    </div>
                </div>
                <div class="form-group mb-2">
                    <div class="form-line">
                        <textarea class="form-control no-resize" placeholder="Event Description..."></textarea>
                    </div>
                </div>       
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-round waves-effect">Add</button>
                <button type="button" class="btn btn-simple btn-round waves-effect" data-bs-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script src="{{ asset('bs5/dist/assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{ asset('bs5/dist/assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js --> 

<script src="{{ asset('bs5/dist/assets/bundles/knob.bundle.js') }}"></script> <!-- Jquery Knob Plugin Js -->
<script src="{{ asset('bs5/dist/assets/bundles/morrisscripts.bundle.js') }}"></script> <!-- Morris Plugin Js --> 
<script src="{{ asset('bs5/dist/assets/bundles/fullcalendarscripts.bundle.js') }}"></script><!--/ calender javascripts -->

<script src="{{ asset('bs5/dist/assets/bundles/mainscripts.bundle.js') }}"></script><!-- Custom Js -->
<script src="../js/pages/calendar/calendar.js"></script>
<script src="../js/pages/profile.js"></script>
@endpush

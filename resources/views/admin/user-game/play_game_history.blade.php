@extends('layouts.app_new')
@section('title','Matka || Game History')
@section('header_title','Game History')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}"/>
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title">Contest</h3> -->
                @can('role-create')
                <a href="{{ route('contest.create') }}" type="button" class="btn btn-info float-right">Create New Game</a>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_contest_list" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Game Name</th>
                    <th>Game Play Time</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->user_name ? $v->user_name : ''}}</td>
                    <td>{{$v->game_name ? $v->game_name : ''}}</td>
                    <td>
                    <label class="badge badge-success">{{$v->start_end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->start_end_time)) : ''}}</label> - 
                    <label class="badge badge-success">{{$v->end_time ? date(TIME_FORMAT_AM_PM,strtotime($v->end_time)) : ''}}</label>
                    </td>
                    <td>
                      <a href="javascript:void(0)" type="button" class="btn btn-success">Completed</a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/daterangepicker.js') }}"></script>
<script>
$(document).ready(function() {
  $(function () {
    $("#tbl_contest_list").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  });
</script>
@endpush

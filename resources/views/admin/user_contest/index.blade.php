@extends('layouts.app_new')
@section('title','Be Max || User Contest Management')
@section('header_title','User Contest')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}"/>
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-4">
                    <div id="datepicker" class="form-control" style="background: #fff; cursor: pointer; border: 1px solid #ccc;">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                      </div>
                      <div class="col-md-4"></div>
                      <div class="col-md-4"></div>
                  </div>
                </div>
                <!-- <h3 class="card-title">User Contest</h3> -->
                <!-- @can('role-create')
                <a href="{{ route('contest.create') }}" type="button" class="btn btn-info float-right">Create New User Contest</a>
                @endcan -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_contest_list" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>User Answer</th>
                    <th>Contest Name</th>
                    <th>Contest Type</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($user_contest as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->user_name ? $v->user_name : ''}}</td>
                    <td>{{$v->answer ? $v->answer : ''}}</td>
                    <td>{{$v->contest_name ? $v->contest_name : ''}}</td>
                    <td>
                    @switch($v->contest_type)
                        @case('1')
                            Single Choice
                            @break
                        @case('2')
                            Arrangement Choice
                            @break
                        @default      
                    @endswitch
                    </td>
                    <td>{{$v->start_date ? $v->start_date : ''}}</td>
                    <td>{{$v->end_date ? $v->end_date : ''}}</td>
                    <td>{{$v->start_end_time ? $v->start_end_time : ''}}</td>
                    <td>{{$v->end_time ? $v->end_time : ''}}</td>
                    <!-- <td>
                      @if($v->status == 1)
                      <a href="{{ route('contest.aproved', $v->id) }}" onclick="return confirm('Are You Sure Want to do Pending ?')" type="button" class="btn btn-success">Approved</a>
                      @else
                      <a href="{{ route('contest.aproved', $v->id) }}" onclick="return confirm('Are You Sure Want to do Approved ?')" type="button" class="btn btn-warning">Pending</a>
                      @endif
                    </td>
                    <td> 
                    @can('role-edit')
                      <a href="{{ route('contest.edit',$v->id) }}" type="button" class="btn btn-primary float-left" style="display:inline">Edit</a>
                    @endcan
                    @can('role-delete')
                    <form action="{{ route('contest.destroy', $v->id) }}" method="POST" style="display:inline;">
                      @csrf
                      @method('DELETE')
                      <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger">Delete</button>
                  </form>
                  <button type="submit"  class="btn btn-danger" style="display:inline">Delete</button>
                  @endcan
                    </td> -->
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/daterangepicker.js') }}"></script>
<script>
  $(document).ready(function() {
    //cb(moment().startOf('month'), moment().endOf('month'));
    $('#datepicker').daterangepicker({
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month till Date': [moment().startOf('month'), moment()],
            'Year till Date': [moment().startOf('year'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'day').startOf('year')]

        }
    }, cb);


// Attach the event listener for when the apply button is clicked
$('#datepicker').on('apply.daterangepicker', function (ev, picker) {
  document.getElementById('spinner-overlay').style.display = 'block';
    var fromDate = moment(picker.startDate).format('YYYY-MM-DD');
    var toDate = moment(picker.endDate).format('YYYY-MM-DD')
    const url = `{{route('user_contest.index') }}?fromDate=${fromDate}&toDate=${toDate}`;
    window.location.href = url;
  });

function getDatesFromUrl() {
        const urlParams = new URLSearchParams(window.location.search);
        const fromDate = urlParams.get('fromDate');
        const toDate = urlParams.get('toDate');
        
        return {
            start: fromDate ? moment(fromDate) : moment().startOf('month'),
            end: toDate ? moment(toDate) : moment()
        };
    }

    // Initial call to set the dates from URL parameters
    const initialDates = getDatesFromUrl();
    //console.log(initialDates.start.format('YYYY-MM-DD'))
    cb(initialDates.start, initialDates.end);

function cb(start, end) {
  $('#datepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}



  $(function () {
    $("#tbl_contest_list").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

});
</script>
@endpush

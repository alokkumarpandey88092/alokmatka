@extends('layouts.app_new')
@section('title','Be Max || Create Contest')
@section('header_title','Create Contest')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('contest.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_contest_save" method="post" action="{{ route('contest.store') }}" enctype="multipart/form-data">
                @csrf
                <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Contest Type</label>
                        <select class="select2" name="contest_type" data-placeholder="Select Contest Type" style="width: 100%;" required>
                          <option value="">Nothing Selected</option>
                          <option value="1">Single Choice Type</option>
                          <option value="2">Arrangment Type</option>
                        </select>
                        @error('contest_name')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>

                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Contest Name</label>
                        <input type="text" name="contest_name" class="form-control" id="contest_name" placeholder="Enter Contest Name" value="{{old('contest_name')}}" required>
                        @error('contest_name')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Contest Time</label> 
                        <input type="text" name="contest_time" class="form-control" id="contest_time" placeholder="Enter Contest Time" value="{{old('contest_time')}}" required>
                        @error('contest_time')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Start Date</label> 
                        <input type="text" name="start_date" class="form-control" id="start_date" placeholder="Enter Start Date" value="{{old('start_date')}}" required readonly>
                        @error('start_date')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputEmail1">End Date</label>
                            <input type="text" name="end_date" class="form-control" id="end_date" placeholder="Enter End Date" value="{{old('end_date')}}" required readonly>
                        @error('end_date')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputMobile">Start Time</label>
                        <input type="text" name="start_time" class="form-control" id="start_time" placeholder="Enter Start Time" value="{{old('start_time')}}" required >
                        @error('start_time')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputPassword1">End Time</label>
                        <input type="text" name="end_time" class="form-control" id="end_time" placeholder="Enter End Time" value="{{old('end_time')}}" required >
                          @error('end_time')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div> 
                        <!-- /.form-group -->
                      </div>
                    </div>
                    <!-- /.row -->

                      <div class="row">
                        <div class="col-12 col-sm-6">
                          <label for="formFile">Pool Price</label>
                          <input type="number" name="pool_price" id="pool_price" class="form-control" value="{{old('pool_price')}}" placeholder="Pool Price" step="0.01" pattern="\d+(\.\d{2})?" required>
                          @error('pool_price')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>

                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Joining Fee</label>
                          <input class="form-control" type="number" id="joining_fee" name="joining_fee" value="{{old('joining_fee')}}" placeholder="Joining Fee" step="0.01" pattern="\d+(\.\d{2})?" required>
                          @error('joining_fee')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Seat</label>
                          <input class="form-control" type="number" id="seats" name="seats" value="{{old('seats')}}" placeholder="Seat" required min="0">
                          @error('seats')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>

                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Description</label>
                          <textarea id="description" class="form-control" name="description" Placeholder=" Enter Description" required>{{old('description')}}</textarea>
                          @error('description')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Member Logo</label>
                          <input class="form-control" type="file" id="member_logo" name="member_logo" placeholder="Choose Logo">
                          @error('member_logo')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror 
                          <br><img style="height:100px;width:200px" id="memberLogoPreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar_male.png')}}';">
                        </div>

                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Contest Image</label>
                          <input class="form-control" type="file" id="contest_image" name="contest_image" placeholder="Choose Contest Image">
                          @error('contest_image')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                          <br><img style="height:100px;width:200px" id="contestImagePreview" src="" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar_male.png')}}';">
                        </div>  
                    </div>

                  </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>

<!-- Page specific script -->
<script>
  //alert()
      //TIMEPICKER
        $('#start_time, #end_time').timepicker({
        'timeFormat': 'H:i', // Use 'H' for 24-hour format without AM/PM
        'step': 30,
        'scrollDefault': 'now'
      });
  $("#start_date,#end_date").datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            immediateUpdates: true,
            todayBtn: 'linked',
            todayHighlight: true,
            //startDate: new Date(),
        });
  $('#member_logo').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#memberLogoPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
      $('#contest_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#contestImagePreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_contest_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

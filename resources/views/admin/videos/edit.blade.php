@extends('layouts.app_new')
@section('title','Videos Alarm || Edit Videos')
@section('header_title','Edit Videos')
@push('css')

@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('videos.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="frm_videos_save" method="post" action="{{ route('videos.update',$data->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <select class="select2" name="category" data-placeholder="Select a Category" style="width: 100%;" required>
                        <option value="">Select a Category</option>
                          @foreach($videos_category as $k => $v)
                            <option {{(@$data->category == $v->id) ? 'selected' : ''}} value="{{$v->id ? $v->id : ''}}">{{$v->category_name ? $v->category_name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Sub Category</label> 
                          <select class="select2" multiple="multiple"  name="subcategory[]" data-placeholder="Select a SubCategory" style="width: 100%;" required>
                          @foreach($videos_subcategory as $k => $v)
                            <option value="{{$v->id ? $v->id : ''}}"
                            @foreach(@$data->videoSubcategory as $key => $val)
                            @if($val->subcategory_id == $v->id) selected @endif
                            @endforeach
                            >{{$v->subcate_name ? $v->subcate_name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label>Genres</label> 
                          <select class="select2" multiple="multiple"  name="genres[]" data-placeholder="Select a Genres" style="width: 100%;" required>
                          @foreach($videos_genre as $k => $v)
                            <option value="{{$v->id ? $v->id : ''}}"
                            @foreach(@$data->videoGenre as $key => $val)
                            @if($val->genre_id == $v->id) selected @endif
                            @endforeach
                            >{{$v->genre_name ? $v->genre_name : ''}}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputEmail1">Video Title</label>
                            <input type="text" name="title" class="form-control" id="exampleInputTitle" placeholder="Enter Title" value="{{@$data->title ? @$data->title : old('title')}}" required>
                        @error('title')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="exampleInputMobile">Video Link</label>
                        <input type="url" name="link" class="form-control" id="exampleInputLink" placeholder="Enter link" value="{{ @$data->link ? @$data->link : old('link')}}" required>
                        @error('link')
                          <li style="color:#e71814">{{ $message }}</li>
                        @enderror
                        </div>
                        <!-- /.form-group -->
                      </div>

                      <!-- /.col -->
                      <div class="col-12 col-sm-6"> 
                        <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea id="description" class="form-control" name="description" Placeholder=" Enter Description">{{ @$data->description ?  @$data->description : old('description')}}</textarea>
                          @error('description')
                          <li style="color:#e71814">{{ $message }}</li>
                          @enderror
                        </div> 
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-12 col-sm-6">
                          <label for="formFile" class="form-label">Thumbnail URL</label>
                          <input class="form-control" type="text" id="thumb_image" name="thumb_image" value="{{@$data->video_thumb}}">
                        </div>
                        <div class="col-12 col-sm-6"> 
                            <div class="form-group">
                              <div class="input-group">
                              <img style="height:100px" id="imgPreview" src="{{asset('assets/videos_thumb')}}/{{@$data->video_thumb}}" alt="profile photo" class="" onerror="this.onerror=null;this.src='{{asset('members_image/avatar-male.png')}}';">
                              </div>
                            </div>
                        </div>
                    </div>

                  </div>
                
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page specific script -->
<script>
  $('#thumb_image').change(function(){
        const file = this.files[0];
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            //console.log(event.target.result);
            $('#imgPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#frm_videos_save').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

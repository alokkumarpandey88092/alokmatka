@extends('layouts.app_new')
@section('title','Be Max || Post Management')
@section('header_title','Post')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Post</h3>
                @can('role-create')
                <a href="{{ route('videos.create') }}" type="button" class="btn btn-info float-right">Create New Videos</a>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_videos_list" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th>Description</th>
                    <th>Approval</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->category_name ? $v->category_name : ''}}</td>
                    <td>{{$v->title ? $v->title : ''}}</td>
                    <td>{{$v->link ? $v->link : ''}}</td>
                    <td>{{$v->description ?  \Illuminate\Support\Str::limit($v->description, 70, $end='...') : ''}}</td>
                    <td>
                      @if($v->status == 1)
                      <a href="{{ route('videos.aproved', $v->id) }}" onclick="return confirm('Are You Sure Want to do Pending ?')" type="button" class="btn btn-success">Approved</a>
                      @else
                      <a href="{{ route('videos.aproved', $v->id) }}" onclick="return confirm('Are You Sure Want to do Approved ?')" type="button" class="btn btn-warning">Pending</a>
                      @endif
                    </td>
                    <td> 
                    @can('role-edit')
                      <a href="{{ route('videos.edit',$v->id) }}" type="button" class="btn btn-primary float-left" style="display:inline">Edit</a>
                      <!-- <a href="javascript:void(0)" type="button" class="btn btn-primary">Edit</a> -->
                    @endcan
                      <!-- <a href="{{ route('users.show',$v->id) }}" type="button" class="btn btn-info">Show</a> -->
                    @can('role-delete')
                    <form action="{{ route('videos.destroy', $v->id) }}" method="POST" style="display:inline;">
                      @csrf
                      @method('DELETE')
                      <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger">Delete</button>
                  </form>
                  <!-- <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger" style="display:inline">Delete</button> -->
                  @endcan
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_videos_list").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

@extends('layouts.app_new')
@section('title','Be Max || User Wallet Transactions')
@section('header_title','Wallet Transactions')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}"/>
@endpush
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                  <!-- <h3 class="card-title">User Wallet's Transactions</h3><br>
                  <a href="{{ route('wallets.index') }}" type="button" class="btn btn-info float-left">Back</a> -->
                  @if($allow_permission)
                  <div class="row">
                    <div class="col-md-4">
                      <div id="datepicker" class="form-control" style="background: #fff; cursor: pointer; border: 1px solid #ccc;">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                      </div>
                      <div class="col-md-4"></div>
                      <div class="col-md-4"></div>
                  </div>
                </div>
                @endif
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_wallet" class="table table-bordered table-striped ">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Transactions's Id</th>
                    <th>Wallet Amount (&#8377;)</th>
                    <th>Credit/Debit</th>
                    <th>Transaction Date</th>
                  </tr>
                  </thead>
                  <tbody>
                @foreach($data as $k => $v)
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->name ? $v->name : ''}}</td>
                    <td>{{$v->transaction_id ? $v->transaction_id : ''}}</td>
                    <td>{{$v->wallet_amount ? number_format($v->wallet_amount,2) : 00}}</td>
                    <td>{{$v->type ? ucfirst($v->type) : '-'}}</td>
                    <td>{{$v->transaction_date ? date(SIMPLE_DATE,strtotime($v->transaction_date)) : '-'}}</td>
                  </tr>
                    @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/daterangepicker.js') }}"></script>
<script>
$(document).ready(function() {
    //cb(moment().startOf('month'), moment().endOf('month'));
    $('#datepicker').daterangepicker({
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month till Date': [moment().startOf('month'), moment()],
            'Year till Date': [moment().startOf('year'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'day').startOf('year')]

        }
    }, cb);


// Attach the event listener for when the apply button is clicked
$('#datepicker').on('apply.daterangepicker', function (ev, picker) {
  document.getElementById('spinner-overlay').style.display = 'block';
    var fromDate = moment(picker.startDate).format('YYYY-MM-DD');
    var toDate = moment(picker.endDate).format('YYYY-MM-DD');
    //const walletId  = 73;
    const currentURL = window.location.pathname;
    const walletId = currentURL.split('/').pop();
    const url = `{{ route('wallets.show', ['wallet' => ':walletId']) }}`
                .replace(':walletId', walletId)
                + `?fromDate=${fromDate}&toDate=${toDate}`;
    window.location.href = url;
  });

function getDatesFromUrl() {
        const urlParams = new URLSearchParams(window.location.search);
        const fromDate = urlParams.get('fromDate');
        const toDate = urlParams.get('toDate');
        
        return {
            start: fromDate ? moment(fromDate) : moment().startOf('month'),
            end: toDate ? moment(toDate) : moment()
        };
    }

    // Initial call to set the dates from URL parameters
    const initialDates = getDatesFromUrl();
    //console.log(initialDates.start.format('YYYY-MM-DD'))
    cb(initialDates.start, initialDates.end);

function cb(start, end) {
  $('#datepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

  $(function () {
    $("#tbl_wallet").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  });
</script>
@endpush

<!-- jQuery -->
<script src="{{ asset('ration/assets/js/jquery-3.6.0.min.js') }}"></script>

<!-- Bootstrap Core JS -->
<script src="{{ asset('ration/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('ration/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Slimscroll JS -->
<script src="{{ asset('ration/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('ration/assets/js/select2.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('ration/assets/js/admin.js') }}"></script>

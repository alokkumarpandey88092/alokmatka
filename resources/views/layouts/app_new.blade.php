<!DOCTYPE html>
<html lang="en" class="sidebar-large">
  @include('layouts.head')
  @stack('css')

  <body class="hold-transition sidebar-mini layout-fixed">
      <div id="spinner-overlay">
      <div class="spinner"></div>
    </div>
     <div class="wrapper">
       @include('layouts.header')
       @switch(true)
            @case(Auth::user()->hasRole('Admin'))
                @include('layouts.sidebar')
                @break
            @case(Auth::user()->hasRole('Partner'))
              @include('layouts.sidebar.partner_sidebar')
                @break
            @case(Auth::user()->hasRole('Agent'))
              @include('layouts.sidebar.agent_sidebar')
                @break
            @case(Auth::user()->hasRole('User'))
              @include('layouts.sidebar.user_sidebar')
                @break
            @endswitch
       @yield('content')
        @include('layouts.footer')
       @stack('scripts')
     </div>
  </body>

</html>
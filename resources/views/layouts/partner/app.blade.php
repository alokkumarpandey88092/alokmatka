<!DOCTYPE html>
<html lang="en" class="sidebar-large">
  @include('layouts.user_game.head')
  @stack('css')

  <body class="hold-transition sidebar-mini layout-fixed">
    
  <div class="main-wrapper">
       <!-- Header -->
       <!-- @include('layouts.user_game.header') -->
       <!-- /Header -->
       <!-- Sidebar -->
       <!-- @include('layouts.user_game.sidebar') -->
       <!-- /Sidebar -->
       @yield('content')
        @include('layouts.user_game.footer')
       @stack('scripts')
     </div>
  </body>

</html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('ration/assets/img/favicon.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('ration/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('ration/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('ration/assets/plugins/fontawesome/css/all.min.css') }}">
    
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('ration/assets/css/animate.min.css') }}">

    <!-- Main CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('ration/assets/css/admin.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('ration/assets/css/admin1.css') }}">

</head>
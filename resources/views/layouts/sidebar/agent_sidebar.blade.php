<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ Auth::user()->name }}</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="{{asset('assets/img/logo.svg')}}" class=" elevation-2" alt="User Image"> -->
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}
          @if(!empty(Auth::user()->getRoleNames()))
                @foreach(Auth::user()->getRoleNames() as $val)
                  ({{ $val }})
                @endforeach
            @endif
          </a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
          <?php
          $current_route = Route::currentRouteName();
          $profiles_route = ['profiles.index', 'profiles.create', 'profiles.edit'];
          $questions_answers_route = ['questions.index', 'questions.create', 'questions.edit','answers.index', 'answers.create', 'answers.edit'];
          $call_log_route = ['call-logs.index','call-logs.create','call-logs.edit'];
          $users_route = ['users.index', 'users.create', 'users.edit', 'roles.index','roles.create','roles.edit'];
          $roles_route = ['roles.index','roles.create','roles.edit'];
          $analyst_route = ['analysts.index', 'analysts.create', 'analysts.edit','questions.index', 'questions.create', 'questions.edit','banners.index','banners.create', 'banners.edit'];
          $questions_route = ['questions.index','questions.create', 'questions.edit'];
          $banners_route = ['banners.index','banners.create', 'banners.edit'];
          $wallet_route = ['wallets.index','wallets.show', 'user_kyc.index', 'user_kyc.create', 'user_kyc.edit', 'user_kyc.show'];
          $agent_route = ['agents.index'];
          $videos_route = ['videos.index', 'videos.create', 'videos.edit'];
          $post_route = ['post.index', 'post.create', 'post.edit'];
          $contest_route = ['contest.index', 'contest.create', 'contest.edit','contest.answer'];
          $user_contest_route = ['user_contest.index', 'user_contest.create', 'user_contest.edit'];
          $banner_route = ['banner.index', 'banner.create', 'banner.edit'];
          $winner_route = ['leaderboard', 'winnerlist'];
          ?>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library --> 
         
          <li class="nav-item menu-open">
            <a href="{{ route('home') }}" class="nav-link <?php echo in_array($current_route, ['home']) ? "active": ""; ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
          </li>
          
          <li class="nav-item ">
            <a href="{{ route('profiles.index') }}" class="nav-link <?php echo in_array($current_route, $profiles_route) ? "active": ""; ?>">
              <i class="nav-icon fas fa-clipboard-check"></i>
              <p>
                Profile
              </p>
            </a>  
          </li>

          <li class="nav-item <?php echo in_array($current_route, $contest_route) ? 'menu-is-opening menu-open': ''; ?>">
            <a href="#" class="nav-link <?php echo in_array($current_route, $contest_route) ? "active": ""; ?>">
              <i class="nav-icon fas fa-poll-h"></i>
              <p>
                Game
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: <?php echo in_array($current_route, $contest_route) ? 'block': ''; ?>;">
              <li class="nav-item">
                <a href="{{ route('contest.index') }}" class="nav-link <?php echo in_array($current_route, ['contest.index']) ? "active": ""; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage Game</p>
                </a>  
              </li>
              <!-- <li class="nav-item">
                <a href="{{ route('contest.answer') }}" class="nav-link <?php echo in_array($current_route, ['contest.answer']) ? "active": ""; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contest Answer</p>
                </a>  
              </li> -->
            </ul>
          </li>

          <!-- <li class="nav-item <?php echo in_array($current_route, $questions_answers_route) ? 'menu-is-opening menu-open': ''; ?>">
            <a href="#" class="nav-link <?php echo in_array($current_route, $questions_answers_route) ? "active": ""; ?>">
              <i class="nav-icon fas fa-poll-h"></i>
              <p>
                Question-Answers
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: <?php echo in_array($current_route, $questions_answers_route) ? 'block': ''; ?>;">
              <li class="nav-item">
                <a href="{{ route('questions.index') }}" class="nav-link <?php echo in_array($current_route, ['questions.index']) ? "active": ""; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Questions</p>
                </a>  
              </li>
              <li class="nav-item">
                <a href="{{ route('answers.index') }}" class="nav-link <?php echo in_array($current_route, ['answers.index']) ? "active": ""; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Answers</p>
                </a>
              </li>
            </ul>
          </li> -->

          <!-- <li class="nav-item ">
            <a href="{{ route('call-logs.index') }}" class="nav-link <?php echo in_array($current_route, $call_log_route) ? "active": ""; ?>">
              <i class="nav-icon fas fa-clipboard-check"></i>
              <p>
                Call-Log
              </p>
            </a>  
          </li> -->
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@if(session('success'))
            <script>
                Swal.fire({
                     title: '{{ session('success') }}',
                    //text: 'You clicked the button!',
                    icon: 'success'
                });
            </script>
        @endif
        
        @if(session('danger'))
            <script>
                Swal.fire({
                     title: '{{ session('danger') }}',
                    //text: 'Something went wrong!',
                    icon: 'error'
                });
            </script>
        @endif

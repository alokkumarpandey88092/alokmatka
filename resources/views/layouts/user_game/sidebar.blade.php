<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
 <!-- Sidebar Menu -->
		<?php
          $current_route = Route::currentRouteName();
          ?>
<div class="sidebar" id="sidebar">
        
        <div class="sidebar-inner slimscroll">
          <div id="sidebar-menu" class="sidebar-menu">
            <ul>
              <li>
                <a href="{{ route('user-game.index') }}"
                  ><i class="fas fa-columns"></i> <span>Dashboard</span></a
                >
              </li>

              <li>
                <a href="{{ route('GameTypeView') }}"
                  ><i class="fas fa-columns"></i> <span>Game</span></a
                >
              </li>

              <li>
                <a href="{{ route('GameHistory') }}"
                  ><i class="fas fa-history"></i> <span>History</span>
                  <span></span
                ></a>
              </li>
              <li class="submenu">
                <a href=""
                  ><i class="fas fa-wallet"></i> <span>wallet</span>
                  <span class="menu-arrow"></span
                ></a>
                <ul style="display: none">
                  <li><a href="{{ route('bank-accounts.index') }}">Bank Details</a></li>
                  <li><a href="{{ route('WalletHistory') }}">Wallet History</a></li>
                </ul>
              </li>
              <li>
                <a  href="javascript:void(0)" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="far fa-window-close"></i> <span>Logout</span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
           				</form>
              </li>
            </ul>
          </div>
        </div>
      </div>
		<!-- /Sidebar -->
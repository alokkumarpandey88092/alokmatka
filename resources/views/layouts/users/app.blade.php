<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title>Dashbaord</title>
    @include('layouts.users.css')
    @stack('css')
</head>
<body>
<div class="main-wrapper">
	
		<!-- Header -->
		@include('layouts.users.header')
		<!-- /Header -->
		
		@include('layouts.users.sidebar')
		<!-- /Sidebar -->
        @yield('content')
	</div>
    @include('layouts.users.js')
    @stack('scripts')
</body>
</html>
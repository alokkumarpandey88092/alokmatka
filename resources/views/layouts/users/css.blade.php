<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('game/assets/img/favicon.png') }}">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/bootstrap/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('game/assets/plugins/fontawesome/css/all.min.css') }}">
 
<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/animate.min.css') }}">

<!-- Main CSS -->
<link rel="stylesheet" href="{{ asset('game/assets/css/admin.css') }}">
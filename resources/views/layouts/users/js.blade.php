	<!-- jQuery -->
	<script src="{{ asset('game/assets/js/jquery-3.6.0.min.js') }}"></script>

	<!-- Bootstrap Core JS -->
	<script src="{{ asset('game/assets/js/popper.min.js') }}"></script>
	<script src="{{ asset('game/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

	<!-- Slimscroll JS -->
	<script src="{{ asset('game/assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

	<!-- Custom JS -->
	<script src="{{ asset('game/assets/js/admin.js') }}"></script>
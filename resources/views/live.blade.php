<!DOCTYPE html>
<html>
<head>
<script src="https://cdn.agora.io/sdk/release/AgoraRTCSDK-3.6.11.js"></script>

    <meta charset="UTF-8">
    <title>Live Streaming</title>
    <style>
      *{
    font-family: sans-serif;
}
h1,h4{
    text-align: center;
}
#me{
    position: relative;
    width: 50%;
    margin: 0 auto;
    display: block;
}
#me video{
    position: relative !important;
}
#remote-container{
    display: flex;
}
#remote-container video{
    height: auto;
    position: relative !important;
}
    </style>
</head>
<body>
    <h1>
        Live Streaming<br><small style="font-size: 14pt;">Powered by Agora.io</small>
    </h1>
    <h4>Local video</h4>
    <div id="me"></div>
    <h4>Remote video</h4>
    <div id="remote-container">
    </div>
    <button class="play">Play</button>
    <script src="/script.js"></script>
</body>
</html>
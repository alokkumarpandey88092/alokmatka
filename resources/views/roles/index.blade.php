@extends('layouts.app_new')
@section('title','Matka || Role Management')
@section('header_title','Role Management')
@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
          @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Roles</h3>
                @can('role-create')
                <a href="{{ route('roles.create') }}" type="button" class="btn btn-info float-right">Create New Role</a>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_rolemanagement" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($roles as $k => $v)  
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$v->name ? $v->name : ''}}</td>
                    <td>
                    @can('role-edit')
                    <a href="{{ route('roles.edit',$v->id) }}" type="button" class="btn btn-primary">Edit</a>
                    @endcan
                    <!-- @can('role-delete')
                      <a href="{{ route('roles.destroy',$v->id) }}" type="button" class="btn btn-danger">Delete</a>
                    @endcan -->
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
  $(function () {
    $("#tbl_rolemanagement").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    //   "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      "buttons": ["csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

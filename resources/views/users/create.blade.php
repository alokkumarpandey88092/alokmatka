@extends('layouts.app_new')
@section('title','Matka || Create User')
@section('header_title','Create User')
@push('css')

@endpush
@section('content')
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            @if ( Session::has('success'))
            <div class="alert alert-success" role="alert" id="alert_msg">
                {{ Session::get('success') }}
            </div> 
            @endif
             @if ( Session::has('danger'))
            <div class="alert alert-danger" role="alert" id="alert_msg">
                {{ Session::get('danger') }}
            </div> 
            @endif
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Create <small>User</small></h3> -->
                <a href="{{ route('users.index') }}" type="button" class="btn btn-info">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="quickForm" method="post" action="{{ route('users.store') }}">
                @csrf
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Enter Name">
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    @error('email')
                          <span style="color:#e71814">{{ $message }}</span>
                    @enderror
                  </div> -->
                  <div class="form-group">
                    <label for="exampleInputMobile">Mobile</label>
                    <input type="text" name="mobile" class="form-control mobile" id="exampleInputMobile" placeholder="Enter Mobile" required>
                    @error('mobile')
                          <span style="color:#e71814">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    @error('password')
                          <span style="color:#e71814">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Confirm Password</label>
                    <input type="password" name="confirm-password" class="form-control" id="exampleInputConfirmPassword1" placeholder="Password" required>
                    @error('confirm-password')
                          <span style="color:#e71814">{{ $message }}</span>
                    @enderror
                  </div><div class="form-group">
                  <label>Role</label> 
                  <select class="select2" multiple="multiple" name="roles[]" data-placeholder="Select a role" style="width: 100%;">
                  @foreach($roles as $k => $v)
                    <option {{$v->name == 'User' ? 'selected' : ''}} value="{{$v->name ? $v->name : ''}}">{{$v->name ? $v->name : ''}}</option>
                  @endforeach
                  </select>
                  </div>
                  
                  <!-- <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@push('scripts')
<!-- jquery-validation -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page specific script -->
<script>
    $(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    //Handle Input table only accept numeric value
    $(document).on('keypress keyup input paste dragover drop','.mobile',function(e){
          return e.charCode >= 48 && e.charCode <= 57 && this.value.length<10;
      });
    });

$(function () {
//   $.validator.setDefaults({
//     submitHandler: function () {
//       alert( "Form successful submitted!" );
//     }
//   });
  $('#quickForm').validate({
    rules: {
      name: {
        required: true,
      },
      mobile: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        name: {
        required: "Please enter name",
      },
      mobile: {
        required: "Please enter Mobile",
        minlength: "Your mobile must be at least 10 digit",
        maxlength: "Your mobile must be at equal 10 digit"
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endpush

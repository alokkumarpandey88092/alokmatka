<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\PaymentGatewayController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('user-registration', [ApiController::class, 'register']);
Route::post('user-login', [ApiController::class, 'login']);
Route::post('otp-varification', [ApiController::class, 'verifyOtp']);
Route::post('analyst-registartion', [ApiController::class, 'analystRegistration']);
Route::post('analyst-login', [ApiController::class, 'analystLogin']);
Route::get('payment-gateway', [PaymentGatewayController::class, 'InitiatePayment']);
Route::get('prequestionnaire', [ApiController::class, 'preQuestionnaire']);
Route::post('resetPassword', [ApiController::class, 'resetPassword']);
Route::post('busyStatus', [ApiController::class, 'busyStatus']);
Route::post('checkCallBalance', [ApiController::class, 'checkCallBalance']);
Route::get('liveAnalyst', [ApiController::class, 'liveAnalyst']);
Route::get('get-banner', [ApiController::class, 'banner']);
Route::get('sendAppLink', [ApiController::class, 'sendAppLink']);
Route::get('privacy-policy', [ApiController::class, 'privacy_policy']);
Route::get('term-condition', [ApiController::class, 'termCondition']);
Route::get('index-fund', [ApiController::class, 'indexfund']);
Route::get('contest-list', [ApiController::class, 'contest']);
Route::get('contest-data', [ApiController::class, 'contestData']);
Route::get('post-list', [ApiController::class, 'post']);



// auth passport  middleware
Route::middleware('auth:api')->group(function () {
    Route::post('update-profile', [ApiController::class, 'updateProfile']);
    Route::get('analyst-list', [ApiController::class, 'analystList']);
    Route::post('user-detail', [ApiController::class, 'userDetails']);
    Route::post('addwallet-money', [ApiController::class, 'addWallet']);
    Route::post('user-wallet', [ApiController::class, 'userWallet']);
    Route::get('user-wallet-history', [ApiController::class, 'userWalletTransaction']);
    Route::get('user-call-log', [ApiController::class, 'userCallLog']);
    Route::post('agora-token', [ApiController::class, 'Agoratoken']);
    Route::post('send_notification', [ApiController::class, 'send_notification']);
    Route::post('update-device-token', [ApiController::class, 'updateDeviceToken']);
    Route::post('user-answere', [ApiController::class, 'userAnswere']);
    Route::post('videoAndChatCallProcess', [ApiController::class, 'videoAndChatCallProcess']);
    Route::post('answerAnalyst', [ApiController::class, 'answerAnalyst']);
    Route::post('makeLive', [ApiController::class, 'makeLive']);
    Route::post('endLive', [ApiController::class, 'endLive']);
    Route::post('userjoinlive', [ApiController::class, 'userJoinLive']);
    Route::post('checkLiveAccess', [ApiController::class, 'checkLiveAccess']);
    Route::post('join-contest', [ApiController::class, 'joinContest']);
    Route::get('user-contest-list', [ApiController::class, 'userContesList']);
    Route::get('check-kyc', [ApiController::class, 'checkKyc']);
    Route::post('user-kyc', [ApiController::class, 'userKyc']);
    Route::post('user-result', [ApiController::class, 'userResult']);
    Route::post('contest-winner', [ApiController::class, 'ContestWinner']);
    Route::post('contest-user-answer', [ApiController::class, 'userContestANS']);
    Route::get('leader-board', [ApiController::class, 'leaderBoard']);

    //new api


});
Route::get('genre-list', [ApiController::class, 'genreList']);
Route::get('category-list', [ApiController::class, 'categoryList']);
Route::get('subcategory-list', [ApiController::class, 'subcategoryList']);
Route::get('video-list', [ApiController::class, 'videoList']);
Route::get('genre-video-list', [ApiController::class, 'genreVideo']);
Route::get('subcategory-video-list', [ApiController::class, 'subcategoryVideo']);
Route::get('movie-video-list', [ApiController::class, 'movieList']);
Route::get('videos-list', [ApiController::class, 'videosList']);
Route::get('tvshowsList-list', [ApiController::class, 'tvshowsList']);
Route::get('banner-list', [ApiController::class, 'BannerList']);
Route::get('payment-initiate', [ApiController::class, 'matkaPaymentGateway']);

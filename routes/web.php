<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\{UserController,BankAccountController,wallethistoryController};
use App\Http\Controllers\Admin\{AnalystController, AccountController, WalletController, AgentController, QuestionsController, AnswersController, CallLogController, AnalystProfileController, AnalystBannerController, VideosController, BannerController, ContestController, UserContestController, UserKycController, PostController,  UserGameController, PartnerController };

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/terms', function () {
    return view('terms');
});



Auth::routes();

Route::get('/logcheck', [App\Http\Controllers\HomeController::class, 'logcheck']);
Route::post('/admin-login', [App\Http\Controllers\HomeController::class, 'logincheck'])->name('admin-login');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/otp-verification', [App\Http\Controllers\Auth\LoginController::class, 'checkOtp'])->name('checkOtp');
Route::post('/otp-verify', [App\Http\Controllers\Auth\LoginController::class, 'verifyOtp'])->name('verify-otp');
Route::get('/gms', [App\Http\Controllers\HomeController::class, 'testing']);
Route::get('/live', function () {
    return view('live');
});

Route::post('register',[UserController::class, 'register'])->name('user.register');

   //Start User Game play Route Here
   Route::group(['middleware' => ['auth', 'checkRole:User']], function () {
    Route::resource('user-game', UserGameController::class);
    Route::get('game/choose-game',[UserGameController::class, 'GameTypeView'])->name('GameTypeView');
    Route::get('game/game-history',[UserGameController::class, 'GameHistory'])->name('GameHistory');
    Route::get('game/wallet-history',[UserGameController::class, 'WalletHistory'])->name('WalletHistory');
    Route::post('game/addBallanceWallet',[UserGameController::class, 'addBallanceWallet'])->name('addBallanceWallet');
    Route::get('game/list',[UserGameController::class, 'GameList'])->name('GameList');
    Route::any('user/{id?}/play-game/{game_type?}',[UserGameController::class, 'playGameView'])->name('user.playGame');
    Route::post('user/play-gameStore',[UserGameController::class, 'playGameStore'])->name('user.playGame.store');
    Route::get('user/play-game/history',[UserGameController::class, 'playGameHistory'])->name('user.playGame.history');
    Route::get('user/play-game/dashboard/{id?}',[UserGameController::class, 'playGameDashboard'])->name('user.playGame.dashboard');

    Route::get('user/previous-game',[UserGameController::class, 'previousGame'])->name('user.previousGame');
});
//End User Game play Route Here

  //Start Partner Dashboard Route Here
  Route::group(['middleware' => ['auth', 'checkRole:Partner'], 'prefix' => 'partner'], function () {
    Route::get('dashboard', [PartnerController::class,'index'])->name('partner.dashboard');
    Route::get('game/create-game',[PartnerController::class, 'GameCreateView'])->name('partner.GameCreateView');
     Route::post('game/create-game',[PartnerController::class, 'GameCreateStore'])->name('partner.GameCreateStore');
    Route::get('game/game-history',[PartnerController::class, 'GameHistory'])->name('partner.GameHistory');
    Route::get('game/{id?}/settings',[PartnerController::class, 'GameSettings'])->name('partner.GameSettings');
    Route::get('/game',[PartnerController::class, 'DataFetch'])->name('partner.DataFetch');

});
//End Partner Dashboard Route Here


 //Start Agent Dashboard Route Here
 Route::group(['middleware' => ['auth', 'checkRole:Agent'], 'prefix' => 'agent'], function () {
     Route::get('dashboard', [AgentController::class,'index'])->name('agent.dashboard');
     Route::get('game/create-user',[AgentController::class, 'UserCreateView'])->name('agent.UserCreateView');
     Route::post('game/create-user',[AgentController::class, 'UserCreateStore'])->name('agent.UserCreateStore');
     Route::get('game/edit-user/{user_id?}',[AgentController::class, 'UserEditView'])->name('agent.UserEditView');
     Route::post('game/edit-user/{user_id?}',[AgentController::class, 'UserEditStore'])->name('agent.UserEditStore');
     Route::get('game/user-history',[AgentController::class, 'UserHistory'])->name('agent.UserHistory');
     Route::post('game/delete-user/{user_id?}',[AgentController::class, 'UserDestroy'])->name('agent.UserDestroy');
});
//End Agent Dashboard Route Here

Route::group(['middleware' => ['auth', 'checkRole:Admin']], function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('analysts', AnalystController::class);
    Route::resource('videos', VideosController::class);
    Route::resource('contest', ContestController::class);
    Route::get('contest/uploda-result/{id?}', [ContestController::class, 'ContestUploadResult'])->name('contest.upload.result');
    Route::post('contest/uploda-result/{id?}', [ContestController::class, 'ContestUploadResultStore'])->name('contest.upload.result.store');
    Route::get('contest/aproved/{id?}', [ContestController::class, 'approveContest'])->name('contest.aproved');
    Route::get('prize/distribution', [ContestController::class, 'prizeDistribution'])->name('contest.prizedistribution');
    Route::get('prize/{id?}', [ContestController::class, 'prizeDistributionView'])->name('contest.prize');

    Route::get('contest-answer', [ContestController::class, 'contestAnswer'])->name('contest.answer');
    Route::get('contest-answer-edit/{id?}', [ContestController::class, 'editAnswer'])->name('contest.editanswer');
    Route::post('contest-answer-save/{id?}', [ContestController::class, 'saveContestAns'])->name('contest.editanswersave');
    Route::resource('user_contest', UserContestController::class);
    Route::resource('wallets', WalletController::class);
    Route::resource('user_kyc', UserKycController::class);
    Route::resource('post', PostController::class);
    Route::get('wallets/{wallet}', [WalletController::class, 'show'])->name('wallets.show');

    Route::get('videos/aproved/{id?}', [VideosController::class, 'approveVideos'])->name('videos.aproved');
    Route::get('analysts/aproved/{id?}', [AnalystController::class, 'approveAnalyst'])->name('analysts.aproved');
    Route::get('analyst/account', [AccountController::class, 'index'])->name('account.index');
    Route::get('notification', [ContestController::class, 'notificationView'])->name('notification.create');
    Route::get('notification-list', [ContestController::class, 'notificationList'])->name('notification.index');
    Route::get('leader-board', [ContestController::class, 'leaderBoardList'])->name('leaderboard');
    Route::get('winners', [ContestController::class, 'winnerList'])->name('winnerlist');
    //Start User Management Route Here
    //Route::resource('products', ProductController::class);
    Route::resource('questions', QuestionsController::class);
    Route::resource('answers', AnswersController::class);
    Route::resource('call-logs', CallLogController::class);
    Route::resource('profiles', AnalystProfileController::class);
    Route::resource('banners', AnalystBannerController::class);
    Route::resource('banner', BannerController::class);

});

//Start some common group route here
Route::group(['middleware' => ['auth', 'checkRole:User,Partner,Agent']], function () {
    Route::resource('bank-accounts', BankAccountController::class);
});


//End some common group route here

Route::get('matka/test', [VideosController::class, 'matkaTest']);
Route::get('base64ToString', [UserController::class, 'decodeBase64']);


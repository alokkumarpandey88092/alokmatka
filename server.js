const express = require('express');
const app = express();
const port = 8080;

// Define the webhook route and handler
app.get('https://konnectprodstream4.knowlarity.com:8200/update-stream/0d419bbd-2b71-434a-a601-422cb87d12f2/konnect?source=router', (req, res) => {
  // Handle the incoming webhook request
  console.log('Webhook received:', req.body);

  // Send a response back to the webhook sender
  res.status(200).send('Webhook received successfully');
});

// Start the server
app.listen(port, () => {
  console.log(`Webhook server started on port ${port}`);
});





// const server = require('http').createServer();
// const io = require('socket.io')(server);

// io.on('connection', (socket) => {
//   console.log('Client connected:', socket.id);

//   // Handle WebSocket events
//   socket.on('message', (data) => {
//     console.log('Received message:', data);

//     // Call the REST API and process the data
//     // ...

//     // Send the response back to the client
//     socket.emit('response', responseData);
//   });

//   socket.on('disconnect', () => {
//     console.log('Client disconnected:', socket.id);
//   });
// });

// const port = 3000; // Specify the port number
// server.listen(port, () => {
//   console.log(`WebSocket server running on port ${port}`);
// });

// const net = require('net');
// const http = require('http');

// // TCP server setup
// const server = net.createServer();

// server.on('connection', (socket) => {
//   console.log('A device connected.');
//   socket.setTimeout(300000); 

//   socket.on('data', (data) => {
//     const event = data.toString().trim();
//     console.log('Received event:', event);

//     // Process and handle the event as per your requirements
//     // ...

//     // Make an HTTP request to notify the event
//     notifyEvent(event);
//   });

//   socket.on('close', () => {
//     console.log('Device disconnected.');
//   });

//   socket.on('error', (err) => {
//     console.log('Socket error:', err);
//   });

  
// });

// const port = 8080;
// server.listen(port, () => {
//   console.log(`Server listening on port ${port}`);
// });

// // HTTP request setup
// const deviceIP = '122.176.118.118';
// const devicePort = 9156;
// encodedToken = '0d419bbd-2b71-434a-a601-422cb87d12f2';
// x_api_key = 'p7nCSznkUy3hsCr7cXlzu4t7Kz1zNZcR3QwUy2aI';
// channel = 'Basic';
// customer_number = '+919389752279';

// function notifyEvent(event) {
//   const url =  `https://konnectprodstream4.knowlarity.com:8200/update-stream/0d419bbd-2b71-434a-a601-422cb87d12f2/konnect?source=router`; 
//   //`http://${deviceIP}:${devicePort}/device.cgi/tcpevents?action=getevent&event=${encodeURIComponent(event)}`;

//   const options = {
//     method: 'GET',
//     // headers: {
     
//     //   "x-api-key" : `p7nCSznkUy3hsCr7cXlzu4t7Kz1zNZcR3QwUy2aI`,
//     //   "Authorization": `0d419bbd-2b71-434a-a601-422cb87d12f2`,
//     //   "content-type" : `application/json`,
//     //   "cache-control": "no-cache",
//     //   "channel": `Basic`,
//     //   "customer_number":`+919389752279`
//     // }
//   };

//   const request = http.request(url, options, (response) => {
//     let responseData = '';

//     response.on('data', (chunk) => {
//       responseData += chunk;
//     });

//     response.on('end', () => {
//       console.log('Notification response:', responseData);
//       // Process the response data as per your requirements
//       // ...
//     });
//   });

//   request.on('error', (error) => {
//     console.error('Request error:', error);
//   });

//   request.end();
// }